import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { Provider } from 'react-redux';

import store from './src/public/redux/store';
import CheckAuth from './src/auth/screens/CheckAuth';
import config from './src/public/config/config.json';

const color = config.color;

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <SafeAreaView
          style={{ flex: 1, backgroundColor: color.backgroundContainer }}
        >
          <CheckAuth />
        </SafeAreaView>
      </Provider>
    );
  }
}

export default App;
