import { Fetch, getUser } from '../public/services';
import config from '../public/config/config.json';
import { getNotification } from '../notification/action';
// import CheckAuth from '../auth/screens/CheckAuth';
import OneSignal from 'react-native-onesignal';
// import DeviceInfo from 'react-native-device-info';

export const getNewsFeed = () => async dispatch => {
  dispatch({ type: 'GET_NEWS_FEED_PENDING' });

  OneSignal.getPermissionSubscriptionState((status) => {
    console.log('andriesnya',status);
});

  const data = {
    type: 'get_news_feed',
    limit: 20,
    android_m_device_id:  'this.userId'
  };
  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/posts?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(`/api/posts?access_token=${config.guest}`, data);
  }

  if (response.api_status === 200) {
    dispatch({ type: 'GET_NEWS_FEED_SUCCESS', payload: response.data });
  }
};

export const updateNewsFeed = (postId, ads) => async dispatch => {
  dispatch({ type: 'UPDATE_NEWS_FEED_PENDING' });
  const data = {
    type: 'get_news_feed',
    limit: 20,
    after_post_id: postId
  };
  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/posts?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(`/api/posts?access_token=${config.guest}`, data);
  }

  if (response.api_status === 200) {
    // if (ads) {
      await dispatch(getAds());
    // }
    dispatch({ type: 'UPDATE_NEWS_FEED_SUCCESS', payload: response.data });
  }
};

export const handleLike = postId => async dispatch => {
  dispatch({ type: 'HANDLE_LIKE_PENDING', payload: postId });
  const data = {
    post_id: postId,
    action: 'like'
  };
  const user = await getUser();
  const response = await Fetch(
    `/api/post-actions?access_token=${user.access_token}`,
    data
  );

  if (response.api_status === 200) {
    dispatch({ type: 'HANDLE_LIKE_SUCCESS', payload: response.data });
  }
};

export const shareToTimeline = postId => async dispatch => {
  dispatch({ type: 'SHARE_TIMELINE_PENDING' });

  const user = await getUser();
  const data = {
    type: 'share_post_on_timeline',
    id: postId,
    user_id: user.user_id
  };

  const response = await Fetch(
    `/api/posts?access_token=${user.access_token}`,
    data
  );

  if (response.api_status === 200) {
    await dispatch({ type: 'SHARE_TIMELINE_SUCCESS', payload: response.data });
    dispatch(getNewsFeed());
    dispatch(getNotification());
  }
};

export const setViewable = item => async dispatch => {
  dispatch({ type: 'SET_VIEWABLE', payload: item.viewableItems });
};

export const deletePost = postId => async dispatch => {
  dispatch({ type: 'DELETE_POST_PENDING' });

  const user = await getUser();
  const data = {
    post_id: postId,
    action: 'delete'
  };

  const response = await Fetch(
    `/api/post-actions?access_token=${user.access_token}`,
    data
  );

  if (response.api_status === 200) {
    dispatch({ type: 'DELETE_POST_SUCCESS', payload: postId });
  }
};

export const getAds = () => async dispatch => {
  dispatch({ type: 'GET_ADS_PENDING' });
  const response = await Fetch('/app_api.php?application=phone&type=get_ads');
  if (response.api_status === '200' && response.ads != null) {
    dispatch({
      type: 'GET_ADS_SUCCESS',
      payload: { type: 'ads', data: response }
    });
  }
};
