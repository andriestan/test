const initialState = {
  isLoading: true,
  isLoadingUpdate: false,
  isPagination: true,
  news: [],
  viewable: [],
  isLoadingDelete: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_NEWS_FEED_PENDING':
      return { ...state, isLoading: true, news: [] };
    case 'GET_NEWS_FEED_SUCCESS':
      return {
        ...state,
        isLoading: false,
        news: action.payload,
        isPagination: !(action.payload.length < 20)
      };
    case 'UPDATE_NEWS_FEED_PENDING':
      return { ...state, isLoadingUpdate: true };
    case 'UPDATE_NEWS_FEED_SUCCESS':
      return {
        ...state,
        isLoadingUpdate: false,
        news: state.news.concat(action.payload),
        isPagination: !(action.payload.length < 20)
      };
    // eslint-disable-next-line no-case-declarations
    case 'HANDLE_LIKE_PENDING':
      const index = state.news.findIndex(
        field => field.post_id === action.payload
      );
      const news = state.news;
      news[index].is_liked = !news[index].is_liked;
      if (news[index].is_liked) {
        news[index].post_likes = Number(news[index].post_likes) + 1;
      } else {
        news[index].post_likes = Number(news[index].post_likes) - 1;
      }
      return { ...state, news };
    // eslint-disable-next-line no-case-declarations
    case 'CREATE_COMMENT_SUCCESS':
      const indexComment = state.news.findIndex(
        field => field.post_id === action.payload.post_id
      );
      const newsComment = state.news;
      newsComment[indexComment].post_comments =
        Number(newsComment[indexComment].post_comments) + 1;
      return { ...state, news: newsComment };
    case 'SET_VIEWABLE':
      return { ...state, viewable: action.payload };
    case 'DELETE_POST_PENDING':
      return { ...state, isLoadingDelete: true };
    case 'DELETE_POST_SUCCESS':
      return {
        ...state,
        isLoadingDelete: false,
        news: state.news.filter(data => data.post_id !== action.payload)
      };
    // eslint-disable-next-line no-case-declarations
    case 'DELETE_COMMENT_SUCCESS':
      const indexMinComment = state.news.findIndex(
        field => field.post_id === action.payload.postId
      );
      const newsMinComment = state.news;
      newsMinComment[indexMinComment].post_comments =
        Number(newsMinComment[indexMinComment].post_comments) - 1;
      return { ...state, news: newsMinComment };
    case 'GET_ADS_SUCCESS':
      return {
        ...state,
        news: state.news.concat(action.payload)
      };
    default:
      return state;
  }
};
