import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import { PostShimmer } from '../../public/components';
import config from '../../public/config/config.json';

const color = config.color;

class Shimmer extends Component {
  constructor() {
    super();
    this.state = {
      data: [1, 2, 3]
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.content,
            {
              flexDirection: 'row',
              marginTop: 0,
              marginBottom: 10,
              paddingHorizontal: 20,
              paddingVertical: 10,
              alignItems: 'center'
            }
          ]}
        >
          <View style={styles.wrapImage} />
          <View
            style={{
              borderColor: color.backgroundContainer,
              borderWidth: 1,
              borderRadius: 20,
              flex: 1,
              marginLeft: 10,
              paddingVertical: 10,
              paddingHorizontal: 15
            }}
          >
            <View style={{ backgroundColor: color.backgroundContainer, width: 100, height: 15 }} />
          </View>
        </View>
        {this.state.data.map(item => (
          <PostShimmer key={item.toString()} />
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContainer
  },
  content: {
    backgroundColor: color.backgroundContent,
    marginVertical: 5
  },
  wrapImage: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.backgroundContainer
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2
  },
  wrapLikeCommentShare: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderTopColor: color.backgroundContainer,
    borderTopWidth: 1,
    paddingVertical: 10,
    marginTop: 5
  }
});

export default Shimmer;
