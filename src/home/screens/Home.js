import React, { Component, Fragment } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  RefreshControl,
  TouchableHighlight,
  Text,
  Image,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';

import {
  getNewsFeed,
  updateNewsFeed,
  handleLike,
  shareToTimeline,
  setViewable
} from '../action';
import { handleLogout } from '../../auth/action';
import { getProfile } from '../../profile/action';
import { getNotification } from '../../notification/action';
import config from '../../public/config/config.json';
import { Loading, Post, PostShare, PostAds } from '../../public/components';
import Shimmer from '../components/Shimmer';

const color = config.color;
const viewabilityConfig = {
  minimumViewTime: 0,
  viewAreaCoveragePercentThreshold: 1,
  waitForInteraction: false
};

class Home extends Component {
  constructor() {
    super();
    this.state = {
      refreshing: false
    };
  }

  componentDidMount() {
    if (this.props.auth.isLogin) {
      this.props.getProfile(
        this.props.handleLogout,
        this.props.navigation,
        this.props.getNewsFeed
      );
      this.props.getNotification();
    }
    this.props.getNewsFeed();
    this.props.navigation.setParams({
      scrollToTop: this.scrollToTop
    });
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    this.props.getNewsFeed();
    if (this.props.auth.isLogin) {
      this.props.getProfile(
        this.props.handleLogout,
        this.props.navigation,
        this.props.getNewsFeed
      );
      this.props.getNotification();
    }
    this.setState({ refreshing: false });
  };

  onEndReached = () => {
    if (!this.props.home.isLoadingUpdate) {
      this.props.updateNewsFeed(
        this.props.home.news[this.props.home.news.length - 1].id,
        this.props.home.news.length % 40 === 0
      );
    }
  };

  onViewableItemsChanged = props => {
    this.props.setViewable(props);
  };

  scrollToTop = () => {
    this.flatList.scrollToIndex({ index: 0 });
  };

  renderItem = ({ item, index }) => (
    <Fragment>
      {index === 0 ? (
        <TouchableHighlight
          style={[
            styles.content,
            {
              flexDirection: 'row',
              marginTop: 0,
              marginBottom: 10,
              paddingHorizontal: 20,
              paddingVertical: 10,
              alignItems: 'center'
            }
          ]}
          underlayColor={color.buttonEffect}
          // eslint-disable-next-line no-confusing-arrow
          onPress={() =>
            this.props.auth.isLogin
              ? this.props.navigation.navigate('Post', { title: 'Create Post' })
              : this.props.navigation.navigate('Login', { title: 'Login' })
          }
        >
          <Fragment>
            <View style={styles.wrapImage}>
              <Text style={{ fontSize: 12, color: color.whitelv2 }}>Image</Text>
              {this.props.auth.isLogin ? (
                <Image
                  source={{ uri: this.props.profile.userData.avatar }}
                  style={styles.image}
                />
              ) : (
                <Image
                  source={require('../../public/assets/icon/ic-whatsup.png')}
                  style={styles.image}
                />
              )}
            </View>
            <View
              style={{
                borderColor: color.whitelv2,
                borderWidth: 1,
                borderRadius: 20,
                flex: 1,
                marginLeft: 10,
                paddingVertical: 10,
                paddingHorizontal: 15
              }}
            >
              <Text style={{ color: color.whitelv2 }}>
                Share your Experience
              </Text>
            </View>
          </Fragment>
        </TouchableHighlight>
      ) : null}
      {// eslint-disable-next-line no-nested-ternary
      item.type === 'ads' ? (
        <PostAds item={item.data} />
      ) : item.shared_info === null ? (
        <Post
          userData={this.props.profile.userData}
          item={item}
          navigation={this.props.navigation}
          isLogin={this.props.auth.isLogin}
          isMe={this.props.profile.userData.user_id === item.publisher.user_id}
          handleLike={() => this.props.handleLike(item.post_id)}
          handleShareTimeline={this.props.shareToTimeline}
        />
      ) : (
        <PostShare
          data={item}
          userData={this.props.profile.userData}
          item={item.shared_info}
          navigation={this.props.navigation}
          isLogin={this.props.auth.isLogin}
          isMe={this.props.profile.userData.user_id === item.publisher.user_id}
          isMeShare={
            this.props.profile.userData.user_id ===
            item.shared_info.publisher.user_id
          }
          handleLike={() => this.props.handleLike(item.post_id)}
          handleShareTimeline={this.props.shareToTimeline}
        />
      )}
    </Fragment>
  );

  render() {
    return !this.props.home.isLoading ? (
      <View style={styles.container}>
        <FlatList
          ref={list => {
            this.flatList = list;
          }}
          onViewableItemsChanged={this.onViewableItemsChanged}
          viewabilityConfig={viewabilityConfig}
          data={this.props.home.news}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={this.props.home.isPagination ? this.onEndReached : null}
          ListFooterComponent={
            this.props.home.isPagination ? (
              <Loading
                style={{ height: 75, marginBottom: 5 }}
                color={color.white}
              />
            ) : (
              <View
                style={{
                  height: 75,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text style={{ color: color.whitelv2 }}>No more post</Text>
              </View>
            )
          }
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }
          renderItem={this.renderItem}
        />
      </View>
    ) : (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
          />
        }
        scrollEnabled={false}
      >
        <Shimmer />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContainer
  },
  content: {
    backgroundColor: color.backgroundContent,
    marginVertical: 5
  },
  wrapImage: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: color.whitelv2,
    borderWidth: 1
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2
  }
});

const mapStateToProps = state => ({
  home: state.home,
  auth: state.auth,
  profile: state.profile
});

const mapDispatchToProps = {
  getNewsFeed,
  updateNewsFeed,
  getProfile,
  handleLogout,
  handleLike,
  getNotification,
  shareToTimeline,
  setViewable
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
