const initialState = {
  data: {
    title: '',
    loading: true
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'HANDLE_WEBVIEW':
      return { ...state, data: action.payload };
    default:
      return state;
  }
};
