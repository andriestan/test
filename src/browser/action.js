export const handleWebView = data => dispatch => {
  dispatch({ type: 'HANDLE_WEBVIEW', payload: data });
};
