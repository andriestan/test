import React, { Component } from 'react';
import { ScrollView, StyleSheet, Dimensions, RefreshControl } from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { WebView } from '../../public/components';
import { handleWebView } from '../action';

const color = config.color;

class Browser extends Component {
  constructor() {
    super();
    this.state = {
      refreshing: false
    };
  }

  componentWillUnmount() {
    this.props.handleWebView({ title: '', loading: true });
  }

  onRefresh = async () => {
    await this.setState({ refreshing: true });
    this.setState({ refreshing: false });
  };

  render() {
    return (
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
        }
      >
        {!this.state.refreshing ? (
          <WebView
            source={{
              uri: this.props.navigation.getParam('url')
            }}
            style={{ width: Dimensions.get('window').width }}
            onLoadProgress={({ nativeEvent }) => {
              this.loadingProgress = nativeEvent.progress;
            }}
            onNavigationStateChange={this.props.handleWebView}
          />
        ) : null}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white
  }
});

const mapStateToProps = state => ({
  browser: state.browser
});

const mapDispatchToProps = {
  handleWebView
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Browser);
