import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableHighlight,
  Text,
  TouchableOpacity,
  Linking,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';

const color = config.color;

const Header = ({ navigation, browser }) => (
  <View style={styles.container}>
    <TouchableHighlight
      style={{ width: '15%', alignItems: 'center', height: '100%', justifyContent: 'center' }}
      underlayColor={color.backgroundlv2}
      onPress={() => navigation.goBack()}
    >
      <Image
        source={require('../../public/assets/icon/ic-back.png')}
        style={{ width: '35%', height: '35%' }}
      />
    </TouchableHighlight>
    <View style={{ width: '70%', justifyContent: 'center', height: '100%' }}>
      {!browser.data.loading ? (
        <Text style={{ color: '#fff', fontSize: 20 }}>
          {browser.data.title.substr(0, 25)}
          {browser.data.title.length > 25 ? '...' : ''}
        </Text>
      ) : (
        <View style={{ width: '100%', alignItems: 'flex-start' }}>
          <ActivityIndicator color={color.white} />
        </View>
      )}
      <Text style={{ color: color.whitelv2, fontSize: 10 }}>
        {navigation.getParam('url').substr(0, 45)}
        {navigation.getParam('url').length > 45 ? '...' : ''}
      </Text>
    </View>
    <TouchableOpacity
      style={{ width: '15%', justifyContent: 'center', height: '100%' }}
      onPress={() => Linking.openURL(navigation.getParam('url'))}
    >
      <Text style={{ color: '#fff' }}>Open in</Text>
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.background,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center'
  }
});

const mapStateToProps = state => ({
  browser: state.browser
});

export default connect(mapStateToProps)(Header);
