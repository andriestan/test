import React, { Component, Fragment } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Image,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import FilePickerManager from 'react-native-file-picker';
import VideoPlayer from 'react-native-video';

import config from '../../public/config/config.json';
import { inputPost, choosePhoto, chooseSuggest, chooseVideo } from '../action';
import { Loading } from '../../public/components';

const color = config.color;

class Post extends Component {
  constructor() {
    super();
    this.state = {
      addPost: false
    };
  }

  handleChoosePhoto() {
    const options = {
      noData: true
    };
    ImagePicker.launchImageLibrary(options, response => {
      if (response.uri) {
        this.props.choosePhoto(response);
      }
    });
  }

  handleChooseVideo() {
    FilePickerManager.showFilePicker(null, response => {
      if (response.didCancel) {
        console.log('User cancelled file picker');
      } else if (response.error) {
        console.log('FilePickerManager Error: ', response.error);
      } else {
        this.props.chooseVideo(response);
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ paddingHorizontal: 15, flex: 1 }}>
          <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
            <View style={styles.wrapPhotoProfile}>
              <Text style={{ fontSize: 10 }}>Image</Text>
              <Image
                source={{ uri: this.props.profile.userData.avatar }}
                style={styles.photoProfile}
              />
            </View>
            <View
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingTop: 3,
                justifyContent: 'center'
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  marginBottom: 3,
                  color: color.white
                }}
              >
                {this.props.profile.userData.name}
              </Text>
              <Text style={{ fontSize: 12, color: color.white }}>
                {this.props.profile.userData.username}
              </Text>
            </View>
          </View>
          <TextInput
            placeholder={
              'Share your best achievement here.\nmention @oxa\n\n#oxagame #oxa'
            }
            placeholderTextColor={color.whitelv2}
            style={{
              color: color.white,
              padding: 0,
              flex: 1,
              textAlignVertical: 'top',
              fontSize: 22
            }}
            multiline
            autoFocus
            value={this.props.post.textPost}
            onChangeText={text =>
              this.props.inputPost(text, this.props.post.isSuggest)
            }
          />
          {this.props.post.isSuggest ? (
            <View
              style={[
                {
                  flex: 2,
                  backgroundColor: color.backgroundContent,
                  marginBottom: 10
                },
                !this.props.post.suggests.length > 0
                  ? { justifyContent: 'center', alignItems: 'center' }
                  : null
              ]}
              elevation={5}
            >
              {// eslint-disable-next-line no-nested-ternary
              !this.props.post.isLoadingSuggest ? (
                this.props.post.suggests.length > 0 ? (
                  <FlatList
                    data={this.props.post.suggests}
                    keyExtractor={(item, index) => index.toString()}
                    keyboardShouldPersistTaps="handled"
                    renderItem={({ item }) => (
                      <TouchableOpacity
                        style={{
                          flexDirection: 'row',
                          paddingHorizontal: 20,
                          paddingVertical: 10
                        }}
                        onPress={() =>
                          this.props.chooseSuggest(
                            this.props.post.textPost.substring(
                              0,
                              this.props.post.textPost.lastIndexOf('@') + 1
                            ),
                            item.username
                          )
                        }
                      >
                        <View style={styles.wrapPhotoProfileSugges}>
                          <Text style={{ fontSize: 10 }}>Image</Text>
                          <Image
                            source={{ uri: item.avatar }}
                            style={styles.photoProfileSugges}
                          />
                        </View>
                        <View
                          style={{
                            flex: 1,
                            paddingLeft: 15,
                            justifyContent: 'center'
                          }}
                        >
                          <Text
                            style={{
                              fontSize: 16,
                              fontWeight: 'bold',
                              marginBottom: 3,
                              color: color.white
                            }}
                          >
                            {item.username}
                          </Text>
                          <Text style={{ fontSize: 12, color: color.white }}>
                            {item.name}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    )}
                  />
                ) : (
                  <Text style={{ color: color.whitelv2 }}>No user</Text>
                )
              ) : (
                <Loading color={color.white} style={{ flex: 1 }} />
              )}
            </View>
          ) : null}
        </View>
        {this.props.post.photo !== '' ? (
          <View style={{ padding: 15 }}>
            <View style={{ width: 100, height: 100, borderRadius: 10 }}>
              <Image
                source={{ uri: this.props.post.photo.uri }}
                style={{
                  width: '100%',
                  height: '100%',
                  borderRadius: 10,
                  borderWidth: 1,
                  borderColor: color.backgroundContainer
                }}
              />
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  top: -5,
                  right: -5,
                  backgroundColor: color.red,
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: 20,
                  height: 20,
                  borderRadius: 20 / 2
                }}
                onPress={() => this.props.choosePhoto('')}
              >
                <Text
                  style={{
                    color: color.white,
                    fontSize: 10,
                    fontWeight: 'bold'
                  }}
                >
                  X
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : null}
        {this.props.post.video !== '' ? (
          <View style={{ padding: 15 }}>
            <View style={{ width: 100, height: 100 }}>
              <VideoPlayer
                style={{
                  width: '100%',
                  height: '100%',
                  borderWidth: 1,
                  borderColor: color.backgroundContainer
                }}
                source={{ uri: this.props.post.video.uri }}
                resizeMode="cover"
                muted
                repeat
              />
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  top: -8,
                  right: -8,
                  backgroundColor: color.red,
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: 20,
                  height: 20,
                  borderRadius: 20 / 2
                }}
                onPress={() => this.props.chooseVideo('')}
              >
                <Text
                  style={{
                    color: color.white,
                    fontSize: 10,
                    fontWeight: 'bold'
                  }}
                >
                  X
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : null}
        <View style={{ backgroundColor: color.backgroundContainer }}>
          <TouchableHighlight
            style={{ padding: 10, alignItems: 'center' }}
            underlayColor={color.buttonEffect}
            onPress={() => this.setState({ addPost: !this.state.addPost })}
          >
            <Text style={{ color: color.whitelv2 }}>
              {!this.state.addPost ? 'Add Post' : 'Close'}
            </Text>
          </TouchableHighlight>
          {this.state.addPost ? (
            <Fragment>
              <TouchableHighlight
                style={{ padding: 10 }}
                underlayColor={color.buttonEffect}
                onPress={() => this.handleChoosePhoto()}
              >
                <Text style={{ color: color.whitelv2 }}>Choose Photo</Text>
              </TouchableHighlight>
              <TouchableHighlight
                style={{ padding: 10 }}
                underlayColor={color.buttonEffect}
                onPress={() => this.handleChooseVideo()}
              >
                <Text style={{ color: color.whitelv2 }}>Choose Video</Text>
              </TouchableHighlight>
            </Fragment>
          ) : null}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContent
  },
  wrapPhotoProfile: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    borderColor: color.backgroundContainer,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoProfile: {
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2,
    position: 'absolute'
  },
  wrapPhotoProfileSugges: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    borderColor: color.backgroundContainer,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoProfileSugges: {
    width: '100%',
    height: '100%',
    borderRadius: 40 / 2,
    position: 'absolute'
  }
});

const mapStateToProps = state => ({
  profile: state.profile,
  post: state.post
});

const mapDispatchToProps = {
  inputPost,
  choosePhoto,
  chooseSuggest,
  chooseVideo
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Post);
