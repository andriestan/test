const initialState = {
  isLoading: false,
  textPost: '',
  photo: '',
  video: '',
  isSuggest: false,
  isLoadingSuggest: false,
  suggests: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'INPUT_POST':
      return {
        ...state,
        textPost: action.payload,
        isSuggest:
          !action.payload
            .substring(action.payload.lastIndexOf('@'), action.payload.length)
            .includes(' ') &&
          action.payload.includes('@') &&
          (action.payload[action.payload.lastIndexOf('@') - 1] === ' ' ||
            action.payload.lastIndexOf('@') === 0)
      };
    case 'CHOOSE_PHOTO':
      return { ...state, photo: action.payload, video: '' };
    case 'CHOOSE_VIDEO':
      return { ...state, video: action.payload, photo: '' };
    case 'HANDLE_POST_PENDING':
      return { ...state, isLoading: true };
    case 'HANDLE_POST_SUCCESS':
      return {
        ...state,
        isLoading: false,
        textPost: '',
        photo: ''
      };
    case 'HANDLE_POST_ERROR':
      return {
        ...state,
        isLoading: false
      };
    case 'HANDLE_SUGGEST_RESET':
      return {
        ...state,
        isLoadingSuggest: false,
        suggests: []
      };
    case 'HANDLE_SUGGEST_PENDING':
      return { ...state, isLoadingSuggest: true };
    case 'HANDLE_SUGGEST_SUCCESS':
      return {
        ...state,
        isLoadingSuggest: false,
        suggests: action.payload
      };
    default:
      return state;
  }
};
