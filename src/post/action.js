import { Platform } from 'react-native';

import { Fetch, getUser } from '../public/services';
import { getNewsFeed } from '../home/action';

export const handleSuggest = searchKey => async dispatch => {
  dispatch({ type: 'HANDLE_SUGGEST_PENDING', payload: searchKey });
  const data = {
    search_key: searchKey
  };

  const user = await getUser();
  const response = await Fetch(
    `/api/search?access_token=${user.access_token}`,
    data
  );

  if (searchKey === '') {
    dispatch({
      type: 'HANDLE_SUGGEST_RESET'
    });
  } else if (response.api_status === 200) {
    dispatch({
      type: 'HANDLE_SUGGEST_SUCCESS',
      payload: response.users
    });
  }
};

export const inputPost = (data, isSuggest) => async dispatch => {
  if (isSuggest) {
    dispatch(
      handleSuggest(data.substring(data.lastIndexOf('@') + 1, data.length))
    );
  }
  dispatch({ type: 'INPUT_POST', payload: data });
};

export const chooseSuggest = (data, user) => async dispatch => {
  dispatch(inputPost(`${data}${user} `, false));
};

export const choosePhoto = data => async dispatch => {
  dispatch({ type: 'CHOOSE_PHOTO', payload: data });
};

export const chooseVideo = data => async dispatch => {
  dispatch({ type: 'CHOOSE_VIDEO', payload: data });
};

export const handlePost = (input, file, navigation) => async dispatch => {
  dispatch({ type: 'HANDLE_POST_PENDING' });
  const user = await getUser();
  const data = {
    user_id: user.user_id,
    s: user.access_token,
    postText: input
  };

  if (file !== '') {
    data.postFile = await {
      name: file.fileName,
      type: file.type,
      uri:
        Platform.OS === 'android' ? file.uri : file.uri.replace('file://', '')
    };
  }

  const response = await Fetch(
    '/app_api.php?application=phone&type=new_post',
    data
  );
  // eslint-disable-next-line eqeqeq
  if (response.api_status == 200) {
    dispatch({
      type: 'HANDLE_POST_SUCCESS'
    });
    navigation.goBack();
    dispatch(getNewsFeed());
  }
};
