import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableHighlight,
  Text,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { handlePost } from '../action';

const color = config.color;

// eslint-disable-next-line no-shadow
const Header = ({ navigation, handlePost, post }) => (
  <View style={styles.container}>
    <TouchableHighlight
      style={{
        width: '15%',
        alignItems: 'center',
        height: '100%',
        justifyContent: 'center'
      }}
      underlayColor={color.backgroundlv2}
      onPress={() => navigation.goBack()}
      disabled={post.isLoading}
    >
      <Image
        source={require('../../public/assets/icon/ic-back.png')}
        style={{ width: '35%', height: '35%' }}
      />
    </TouchableHighlight>
    <View style={{ width: '70%', justifyContent: 'center', height: '100%' }}>
      <Text style={{ color: '#fff', fontSize: 20 }}>
        {navigation.getParam('title')}
      </Text>
    </View>
    <TouchableOpacity
      style={{
        width: '15%',
        alignItems: 'center',
        height: '100%',
        justifyContent: 'center'
      }}
      onPress={() =>
        handlePost(
          post.textPost,
          post.photo !== '' ? post.photo : post.video,
          navigation
        )
      }
      disabled={
        (post.textPost === '' && post.photo === '' && post.video === '') ||
        post.isLoading ||
        false
      }
    >
      {!post.isLoading ? (
        <Text
          style={{
            color:
              post.textPost === '' && post.photo === '' && post.video === ''
                ? color.whitelv2
                : color.white
          }}
        >
          Post
        </Text>
      ) : (
        <ActivityIndicator color={color.white} />
      )}
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.background,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center'
  }
});

const mapStateToProps = state => ({
  post: state.post
});

const mapDispatchToProps = {
  handlePost
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);
