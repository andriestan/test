import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  Dimensions,
  View,
  TouchableHighlight,
  ScrollView,
  RefreshControl
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { WebView, Loading } from '../../public/components';
import { getUser } from '../../public/services';

const color = config.color;

class Community extends Component {
  constructor() {
    super();
    this.state = {
      access_token: '',
      refreshing: true
    };
  }

  async componentDidMount() {
    if (this.props.auth.isLogin) {
      const user = await getUser();
      await this.setState({ access_token: user.access_token });
      this.setState({ refreshing: false });
    }
  }

  onRefresh = async () => {
    await this.setState({ refreshing: true });
    this.setState({ refreshing: false });
  };

  render() {
    return this.props.auth.isLogin ? (
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
          />
        }
      >
        {!this.state.refreshing ? (
          <WebView
            source={{
              uri: `${
                config.host
              }/api/set-browser-cookie-setting?access_token=${
                this.state.access_token
              }`,
              method: 'POST',
              body: `server_key=${config.server_key}&url=get_community`
            }}
            style={{ width: Dimensions.get('window').width }}
          />
        ) : (
          <Loading color={color.white} style={{ flex: 1 }} />
        )}
      </ScrollView>
    ) : (
      <View
        style={[
          styles.container,
          { justifyContent: 'center', alignItems: 'center' }
        ]}
      >
        <TouchableHighlight
          style={{
            paddingVertical: 10,
            width: '70%',
            alignItems: 'center',
            backgroundColor: color.blue,
            borderRadius: 10
          }}
          underlayColor={color.buttonEffect}
          onPress={() =>
            this.props.navigation.navigate('Login', { title: 'Login' })
          }
        >
          <Text style={{ color: color.white }}>Login / Register</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContent
  }
});

const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile
});

export default connect(mapStateToProps)(Community);
