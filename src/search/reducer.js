const initialState = {
  users: [],
  pages: [],
  groups: [],
  hashtag: [],
  isLoading: false,
  searchKey: '',
  isPagination: true,
  isLoadingUpdate: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'HANDLE_SEARCH_RESET':
      return {
        ...state,
        isLoading: false,
        users: [],
        pages: [],
        groups: [],
        hashtag: []
      };
    case 'HANDLE_SEARCH_PENDING':
      return { ...state, isLoading: true, searchKey: action.payload };
    case 'HANDLE_SEARCH_SUCCESS':
      return {
        ...state,
        isLoading: false,
        users: action.payload.users,
        pages: action.payload.pages,
        groups: action.payload.groups,
        hashtag: action.payload.hashtag,
        isPagination: !(action.payload.hashtag.length < 20)
      };
    case 'HANDLE_SEARCH_ERROR':
      return {
        ...state,
        isLoading: false
      };
    // eslint-disable-next-line no-case-declarations
    case 'HANDLE_LIKE_HASHTAG_PENDING':
      const index = state.hashtag.findIndex(
        field => field.post_id === action.payload
      );
      const hashtag = state.hashtag;
      hashtag[index].is_liked = !hashtag[index].is_liked;
      if (hashtag[index].is_liked) {
        hashtag[index].post_likes = Number(hashtag[index].post_likes) + 1;
      } else {
        hashtag[index].post_likes = Number(hashtag[index].post_likes) - 1;
      }
      return { ...state, hashtag };
    case 'HANDLE_SEARCH_HASHTAG_PENDING':
      return { ...state, isLoadingUpdate: true };
    case 'HANDLE_SEARCH_HASHTAG_SUCCESS':
      return {
        ...state,
        isLoadingUpdate: false,
        isPagination: !(action.payload.length < 20),
        hashtag: state.hashtag.concat(action.payload)
      };
    default:
      return state;
  }
};
