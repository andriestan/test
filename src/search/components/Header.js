import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableHighlight, TextInput } from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { handleSearch } from '../action';

const color = config.color;

class Header extends Component {
  componentDidMount() {
    setTimeout(() => this.textInput.focus(), 500);
  }

  render() {
    // eslint-disable-next-line no-shadow
    const { navigation, handleSearch, search } = this.props;
    return (
      <View style={styles.container}>
        <TouchableHighlight
          style={{ width: '15%', alignItems: 'center', height: '100%', justifyContent: 'center' }}
          underlayColor={color.backgroundlv2}
          onPress={() => navigation.goBack()}
        >
          <Image
            source={require('../../public/assets/icon/ic-back.png')}
            style={{ width: '35%', height: '35%' }}
          />
        </TouchableHighlight>
        <View style={{ width: '80%', justifyContent: 'flex-end', height: '100%' }}>
          <View style={styles.wrapInput}>
            <View style={{ width: 20, height: 15 }}>
              <Image
                source={require('../../public/assets/icon/search.png')}
                style={{ width: '80%', height: '100%' }}
              />
            </View>
            <View style={{ flex: 1 }}>
              <TextInput
                ref={input => {
                  this.textInput = input;
                }}
                placeholder="Search and discover"
                placeholderTextColor="#9496a3"
                value={search.searchKey}
                onChangeText={text => handleSearch(text)}
                style={{ padding: 0, color: '#fff' }}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.background,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center'
  },
  wrapInput: {
    flexDirection: 'row',
    borderBottomColor: '#9496a3',
    borderBottomWidth: 1,
    paddingHorizontal: 10,
    marginBottom: 11,
    alignItems: 'center',
    paddingBottom: 2
  }
});

const mapStateToProps = state => ({
  search: state.search
});

const mapDispatchToProps = {
  handleSearch
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);
