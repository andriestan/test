import { Fetch, getUser } from '../public/services';
import config from '../public/config/config.json';

export const handleSearch = searchKey => async dispatch => {
  dispatch({ type: 'HANDLE_SEARCH_PENDING', payload: searchKey });
  const data = {
    search_key: searchKey
  };

  const dataHashtag = {
    hash: searchKey,
    type: 'hashtag',
    limit: 20
  };

  const user = await getUser();
  let response;
  // eslint-disable-next-line no-unused-vars
  let hashtagResponse;
  if (user) {
    response = await Fetch(
      `/api/search?access_token=${user.access_token}`,
      data
    );
    if (searchKey[0] === '#') {
      hashtagResponse = await Fetch(
        `/api/posts?access_token=${user.access_token}`,
        dataHashtag
      );
    }
  } else {
    response = await Fetch(`/api/search?access_token=${config.guest}`, data);
    if (searchKey[0] === '#') {
      hashtagResponse = await Fetch(
        `/api/posts?access_token=${config.guest}`,
        dataHashtag
      );
    }
  }

  if (searchKey === '') {
    dispatch({
      type: 'HANDLE_SEARCH_RESET'
    });
  } else if (response.api_status === 200) {
    if (hashtagResponse) {
      if (hashtagResponse.api_status === 200) {
        dispatch({
          type: 'HANDLE_SEARCH_SUCCESS',
          payload: {
            users: response.users,
            pages: response.pages,
            groups: response.groups,
            hashtag: hashtagResponse ? hashtagResponse.data : []
          }
        });
      }
    } else {
      dispatch({
        type: 'HANDLE_SEARCH_SUCCESS',
        payload: {
          users: response.users,
          pages: response.pages,
          groups: response.groups,
          hashtag: hashtagResponse ? hashtagResponse.data : []
        }
      });
    }
  } else {
    dispatch({
      type: 'HANDLE_SEARCH_ERROR',
      payload: response.errors.error_text
    });
  }
};

export const handleSearcReset = () => dispatch => {
  dispatch({
    type: 'HANDLE_SEARCH_RESET'
  });
};

export const updateSearchHashtag = (searchKey, postId) => async dispatch => {
  dispatch({ type: 'HANDLE_SEARCH_HASHTAG_PENDING' });

  const data = {
    hash: searchKey,
    type: 'hashtag',
    limit: 20,
    after_post_id: postId
  };

  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/posts?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(`/api/posts?access_token=${config.guest}`, data);
  }

  if (searchKey === '') {
    dispatch({
      type: 'HANDLE_SEARCH_RESET'
    });
  } else if (response.api_status === 200) {
    dispatch({
      type: 'HANDLE_SEARCH_HASHTAG_SUCCESS',
      payload: response.data
    });
  }
};

export const handleLikeHashtag = postId => async dispatch => {
  dispatch({ type: 'HANDLE_LIKE_HASHTAG_PENDING', payload: postId });
  dispatch({ type: 'HANDLE_LIKE_PENDING', payload: postId });
  const data = {
    post_id: postId,
    action: 'like'
  };
  const user = await getUser();
  await Fetch(`/api/post-actions?access_token=${user.access_token}`, data);
};
