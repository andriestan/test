import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { Loading, Post } from '../../public/components';
import {
  handleSearch,
  handleSearcReset,
  updateSearchHashtag,
  handleLikeHashtag
} from '../action';
import { shareToTimeline } from '../../home/action';

const color = config.color;

class Search extends Component {
  componentDidMount() {
    if (this.props.navigation.getParam('hashtag')) {
      this.props.handleSearch(this.props.navigation.getParam('hashtag'));
    }
  }

  componentWillUnmount() {
    if (this.props.navigation.getParam('hashtag')) {
      this.props.handleSearch('');
      this.props.handleSearcReset();
    }
  }

  onEndReached = () => {
    if (!this.props.search.isLoadingUpdate) {
      this.props.updateSearchHashtag(
        this.props.search.searchKey,
        this.props.search.hashtag[this.props.search.hashtag.length - 1].post_id
      );
    }
  };

  renderItem = ({ item }) => (
    <Post
      userData={this.props.profile.userData}
      item={item}
      navigation={this.props.navigation}
      isLogin={this.props.auth.isLogin}
      isMe={this.props.profile.userData.user_id === item.publisher.user_id}
      handleLike={() => this.props.handleLikeHashtag(item.post_id)}
      handleShareTimeline={this.props.shareToTimeline}
    />
  );

  render() {
    // eslint-disable-next-line no-nested-ternary
    return !this.props.search.isLoading ? (
      this.props.search.hashtag.length !== 0 ? (
        <View style={styles.container}>
          <FlatList
            data={this.props.search.hashtag}
            keyExtractor={(item, index) => index.toString()}
            keyboardShouldPersistTaps="handled"
            onEndReached={
              this.props.search.isPagination ? this.onEndReached : null
            }
            ListFooterComponent={
              this.props.search.isPagination ? (
                <Loading
                  style={{ height: 75, marginBottom: 5 }}
                  color={color.white}
                />
              ) : (
                <View
                  style={{
                    height: 75,
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  <Text style={{ color: color.whitelv2 }}>No more post</Text>
                </View>
              )
            }
            extraData={this.props}
            renderItem={this.renderItem}
          />
        </View>
      ) : (
        <View
          style={[
            styles.container,
            { justifyContent: 'center', alignItems: 'center' }
          ]}
        >
          <Text style={{ color: color.whitelv2 }}>Empty</Text>
        </View>
      )
    ) : (
      <Loading
        color={color.white}
        style={{ flex: 1, backgroundColor: color.backgroundContent }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContainer,
    marginTop: -5
  },
  wrapPhotoProfile: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    borderColor: color.backgroundContainer,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoProfile: {
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2,
    position: 'absolute'
  }
});

const mapStateToProps = state => ({
  search: state.search,
  auth: state.auth,
  profile: state.profile
});

const mapDispatchToProps = {
  handleSearch,
  handleLikeHashtag,
  shareToTimeline,
  handleSearcReset,
  updateSearchHashtag
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
