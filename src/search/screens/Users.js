import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { Loading } from '../../public/components';

const color = config.color;

class Search extends Component {
  renderItem = ({ item }) => (
    <TouchableOpacity
      style={{ flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 10 }}
      onPress={() =>
        this.props.navigation.navigate('Profile', {
          title: item.username,
          userData: item
        })
      }
    >
      <View style={styles.wrapPhotoProfile}>
        <Text style={{ fontSize: 10 }}>Image</Text>
        <Image source={{ uri: item.avatar }} style={styles.photoProfile} />
      </View>
      <View style={{ flex: 1, paddingLeft: 15, paddingTop: 3, justifyContent: 'center' }}>
        <Text
          style={{
            fontSize: 16,
            fontWeight: 'bold',
            marginBottom: 3,
            color: color.white
          }}
        >
          {item.username}
        </Text>
        <Text style={{ fontSize: 12, color: color.white }}>{item.name}</Text>
      </View>
    </TouchableOpacity>
  );

  render() {
    // eslint-disable-next-line no-nested-ternary
    return !this.props.isLoading ? (
      this.props.users.length !== 0 ? (
        <View style={styles.container}>
          <FlatList
            data={this.props.users}
            keyExtractor={(item, index) => index.toString()}
            keyboardShouldPersistTaps="handled"
            renderItem={this.renderItem}
          />
        </View>
      ) : (
        <View style={[styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
          <Text style={{ color: color.whitelv2 }}>Empty</Text>
        </View>
      )
    ) : (
      <Loading color={color.white} style={{ flex: 1, backgroundColor: color.backgroundContent }} />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContent
  },
  wrapPhotoProfile: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    borderColor: color.backgroundContainer,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoProfile: {
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2,
    position: 'absolute'
  }
});

const mapStateToProps = ({ search }) => search;

export default connect(mapStateToProps)(Search);
