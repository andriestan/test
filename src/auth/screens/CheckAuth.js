import React, { Component } from 'react';
import { connect } from 'react-redux';
import OneSignal from 'react-native-onesignal';

import App from '../../public/navigation';
import { handleCheckAuth } from '../action';
import { getUser } from '../../public/services';
import config from '../../public/config/config.json';

class CheckAuth extends Component {
  constructor(properties) {
    super(properties);
    OneSignal.init(config.oneSignal);
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds.bind(this));
    // this.onIds = this.onIds.bind(this);
    OneSignal.configure(); // triggers the ids event
  }

  componentDidMount() {
    this.checkUser();
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log('Notification received: ', notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
    // console.log('andries: ', device.userId);
  }

  checkUser = async () => {
    const user = await getUser();
    if (user) {
      this.props.handleCheckAuth(true);
    } else {
      this.props.handleCheckAuth(false);
    }
  };

  render() {
    return !this.props.isLoadingCheck ? <App /> : null;
  }
}

const mapStateToProps = ({ auth }) => auth;

export default connect(
  mapStateToProps,
  { handleCheckAuth }
)(CheckAuth);
