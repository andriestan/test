import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  Image,
  Text,
  TouchableHighlight,
  Keyboard,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { handleInputRegister, handleRegister } from '../action';
import { getNewsFeed } from '../../home/action';
import { getProfile } from '../../profile/action';

const color = config.color;

class Register extends Component {
  constructor() {
    super();
    this.state = {
      keyboard: false,
      password: true,
      confirmPassword: true
    };
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.handleKeyboardOn.bind(this)
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.handleKeyboardOff.bind(this)
    );
  }

  componentWillUnmount() {
    if (
      this.props.inputRegister.username !== '' ||
      this.props.inputRegister.email !== '' ||
      this.props.inputRegister.password !== '' ||
      this.props.inputRegister.confirmPassword !== ''
    ) {
      this.props.handleInputRegister({ username: '' });
      this.props.handleInputRegister({ email: '' });
      this.props.handleInputRegister({ password: '' });
      this.props.handleInputRegister({ confirmPassword: '' });
    }
  }

  handlePassword(password) {
    this.setState({
      password
    });
  }

  handleConfirmPassword(confirmPassword) {
    this.setState({
      confirmPassword
    });
  }

  handleKeyboardOn() {
    this.setState({ keyboard: true });
  }

  handleKeyboardOff() {
    this.setState({ keyboard: false });
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView keyboardShouldPersistTaps="handled">
          {!this.state.keyboard ? (
            <View style={styles.wrapLogo}>
              <Image
                source={require('../../public/assets/img/logo.png')}
                style={{ width: 160, height: 49 }}
              />
            </View>
          ) : null}
          <View style={{ alignItems: 'center', marginVertical: 20 }}>
            <View
              style={{
                width: '70%'
              }}
            >
              <View style={styles.wrapInput}>
                <TextInput
                  style={{ padding: 0, color: color.white }}
                  placeholderTextColor={color.whitelv2}
                  placeholder="Username"
                  autoCapitalize="none"
                  returnKeyType="next"

                  onSubmitEditing={() => {
                    this.email.focus();
                  }}
                  onChangeText={text => this.props.handleInputRegister({ username: text })}
                />
              </View>
              <View style={styles.wrapInput}>
                <TextInput
                  ref={input => {
                    this.email = input;
                  }}
                  style={{ padding: 0, color: color.white }}
                  placeholderTextColor={color.whitelv2}
                  placeholder="Email"
                  autoCapitalize="none"
                  keyboardType="email-address"
                  returnKeyType="next"

                  onSubmitEditing={() => {
                    this.password.focus();
                  }}
                  onChangeText={text => this.props.handleInputRegister({ email: text })}
                />
              </View>
              <View style={[styles.wrapInput, { flexDirection: 'row', alignItems: 'center' }]}>
                <TextInput
                  ref={input => {
                    this.password = input;
                  }}
                  style={{ padding: 0, color: color.white, flex: 1, marginRight: 5 }}
                  placeholderTextColor={color.whitelv2}
                  keyboardType={this.state.password ? 'default' : 'visible-password'}
                  placeholder="Password"
                  secureTextEntry
                  returnKeyType="next"

                  onChangeText={text => this.props.handleInputRegister({ password: text })}
                  onSubmitEditing={() => {
                    this.confirmPassword.focus();
                  }}
                />
                <TouchableOpacity onPress={() => this.handlePassword(!this.state.password)}>
                  <Text style={{ color: color.white }}>
                    {this.state.password ? 'Show' : 'Hide'}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={[styles.wrapInput, { flexDirection: 'row', alignItems: 'center' }]}>
                <TextInput
                  ref={input => {
                    this.confirmPassword = input;
                  }}
                  style={{ padding: 0, color: color.white, flex: 1, marginRight: 5 }}
                  placeholderTextColor={color.whitelv2}
                  keyboardType={this.state.confirmPassword ? 'default' : 'visible-password'}
                  placeholder="Confirm Password"
                  secureTextEntry
                  
                  onChangeText={text => this.props.handleInputRegister({ confirm_password: text })}
                  onSubmitEditing={() =>
                    this.props.handleRegister(
                      this.props.inputRegister,
                      this.props.navigation,
                      this.props.getNewsFeed,
                      this.props.getProfile
                    )
                  }
                />
                <TouchableOpacity
                  onPress={() => this.handleConfirmPassword(!this.state.confirmPassword)}
                >
                  <Text style={{ color: color.white }}>
                    {this.state.confirmPassword ? 'Show' : 'Hide'}
                  </Text>
                </TouchableOpacity>
              </View>
              <TouchableHighlight
                style={{
                  backgroundColor: color.red,
                  paddingVertical: 10,
                  borderRadius: 10,
                  alignItems: 'center',
                  marginTop: 10
                }}
                underlayColor={color.backgroundContainer}
                onPress={() =>
                  this.props.handleRegister(
                    this.props.inputRegister,
                    this.props.navigation,
                    this.props.getNewsFeed,
                    this.props.getProfile
                  )
                }
                disabled={this.props.isLoading}
              >
                {!this.props.isLoading ? (
                  <Text style={{ color: color.white }}>Create New Account</Text>
                ) : (
                  <ActivityIndicator color={color.white} />
                )}
              </TouchableHighlight>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.backgroundContent,
    flex: 1
  },
  wrapLogo: {
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
    height: 100,
    backgroundColor: color.background
  },
  wrapInput: {
    borderBottomColor: color.backgroundContainer,
    borderBottomWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginVertical: 10
  }
});

const mapStateToProps = ({ auth }) => auth;

const mapDispatchToProps = {
  handleInputRegister,
  handleRegister,
  getNewsFeed,
  getProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Register);
