import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  Image,
  Text,
  TouchableHighlight,
  Keyboard,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { handleInput, handleLogin } from '../action';
import { getNewsFeed } from '../../home/action';
import { getProfile } from '../../profile/action';

const color = config.color;

class Login extends Component {
  constructor() {
    super();
    this.state = {
      keyboard: false,
      password: true
    };
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.handleKeyboardOn.bind(this)
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.handleKeyboardOff.bind(this)
    );
  }

  componentWillUnmount() {
    if (this.props.input.username !== '' || this.props.input.password !== '') {
      this.props.handleInput({ username: '' });
      this.props.handleInput({ password: '' });
    }
  }

  handlePassword(password) {
    this.setState({
      password
    });
  }

  handleKeyboardOn() {
    this.setState({ keyboard: true });
  }

  handleKeyboardOff() {
    this.setState({ keyboard: false });
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView keyboardShouldPersistTaps="handled">
          <View style={[styles.wrapLogo, this.state.keyboard ? { height: 100 } : null]}>
            <Image
              source={require('../../public/assets/img/logo.png')}
              style={{ width: 160, height: 49 }}
            />
          </View>
          <View style={{ alignItems: 'center', marginVertical: 20 }}>
            <View
              style={{
                width: '70%',
                borderBottomColor: color.backgroundContainer,
                borderBottomWidth: 1
              }}
            >
              <View style={styles.wrapInput}>
                <TextInput
                  placeholderTextColor={color.whitelv2}
                  style={{ padding: 0, color: color.white }}
                  placeholder="Username"
                  autoCapitalize="none"
                  returnKeyType="next"
                  onSubmitEditing={() => {
                    this.password.focus();
                  }}
                  onChangeText={text => this.props.handleInput({ username: text })}
                />
              </View>
              <View style={[styles.wrapInput, { flexDirection: 'row', alignItems: 'center' }]}>
                <TextInput
                  placeholderTextColor={color.whitelv2}
                  ref={input => {
                    this.password = input;
                  }}
                  style={{ padding: 0, color: color.white, flex: 1, marginRight: 5 }}
                  keyboardType={this.state.password ? 'default' : 'visible-password'}
                  placeholder="Password"
                  secureTextEntry
                  onChangeText={text => this.props.handleInput({ password: text })}
                  onSubmitEditing={() =>
                    this.props.handleLogin(
                      this.props.input,
                      this.props.navigation,
                      this.props.getNewsFeed,
                      this.props.getProfile
                    )
                  }
                />
                <TouchableOpacity onPress={() => this.handlePassword(!this.state.password)}>
                  <Text style={{ color: color.white }}>
                    {this.state.password ? 'Show' : 'Hide'}
                  </Text>
                </TouchableOpacity>
              </View>
              <TouchableHighlight
                style={{
                  backgroundColor: color.blue,
                  paddingVertical: 10,
                  borderRadius: 10,
                  alignItems: 'center',
                  marginTop: 10
                }}
                underlayColor={color.backgroundlv2}
                onPress={() =>
                  this.props.handleLogin(
                    this.props.input,
                    this.props.navigation,
                    this.props.getNewsFeed,
                    this.props.getProfile
                  )
                }
                disabled={this.props.isLoading}
              >
                {!this.props.isLoading ? (
                  <Text style={{ color: color.white }}>Log in</Text>
                ) : (
                  <ActivityIndicator color={color.white} />
                )}
              </TouchableHighlight>
              <TouchableOpacity
                style={{ alignItems: 'center', marginVertical: 25 }}
                onPress={() =>
                  this.props.navigation.navigate('ForgotPassword', { title: 'Forgot Password?' })
                }
              >
                <Text style={{ fontWeight: 'bold', color: color.white }}>Forgot Password?</Text>
              </TouchableOpacity>
            </View>
            <View style={{ alignItems: 'center', width: '100%', marginVertical: 20 }}>
              <TouchableHighlight
                style={{
                  backgroundColor: color.red,
                  paddingVertical: 10,
                  borderRadius: 10,
                  alignItems: 'center',
                  marginTop: 10,
                  width: '65%'
                }}
                underlayColor={color.backgroundContainer}
                onPress={() => this.props.navigation.navigate('Register', { title: 'Register' })}
              >
                <Text style={{ color: '#fff' }}>Register</Text>
              </TouchableHighlight>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.backgroundContent,
    flex: 1
  },
  wrapLogo: {
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
    height: 200,
    backgroundColor: color.background
  },
  wrapInput: {
    borderBottomColor: color.backgroundContainer,
    borderBottomWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginVertical: 10
  }
});

const mapStateToProps = ({ auth }) => auth;

const mapDispatchToProps = {
  handleInput,
  handleLogin,
  getNewsFeed,
  getProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
