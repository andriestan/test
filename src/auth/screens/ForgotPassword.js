import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  Image,
  Text,
  TouchableHighlight,
  Keyboard,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { handleInputResetPassword, handleResetPassword } from '../action';

const color = config.color;

class ForgotPassword extends Component {
  constructor() {
    super();
    this.state = {
      keyboard: false
    };
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.handleKeyboardOn.bind(this)
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.handleKeyboardOff.bind(this)
    );
  }

  componentWillUnmount() {
    if (this.props.inputResetPassword.email !== '') {
      this.props.handleInputResetPassword({ email: '' });
    }
  }

  handleKeyboardOn() {
    this.setState({ keyboard: true });
  }

  handleKeyboardOff() {
    this.setState({ keyboard: false });
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView keyboardShouldPersistTaps="handled">
          <View style={[styles.wrapLogo, this.state.keyboard ? { height: 100 } : null]}>
            <Image
              source={require('../../public/assets/img/logo.png')}
              style={{ width: 160, height: 49 }}
            />
          </View>
          <View style={{ alignItems: 'center', marginVertical: 20 }}>
            <View
              style={{
                width: '70%'
              }}
            >
              <View style={styles.wrapInput}>
                <TextInput
                  style={{ padding: 0, color: color.white }}
                  placeholderTextColor={color.whitelv2}
                  placeholder="Email"
                  autoCapitalize="none"
                  keyboardType="email-address"
                  value={this.props.inputResetPassword.email}
                  onChangeText={text => this.props.handleInputResetPassword({ email: text })}
                />
              </View>
              <TouchableHighlight
                style={{
                  backgroundColor: color.blue,
                  paddingVertical: 10,
                  borderRadius: 10,
                  alignItems: 'center',
                  marginTop: 10
                }}
                underlayColor={color.backgroundlv2}
                onPress={() => this.props.handleResetPassword(this.props.inputResetPassword)}
                disabled={this.props.isLoading}
              >
                {!this.props.isLoading ? (
                  <Text style={{ color: color.white }}>Reset Password</Text>
                ) : (
                  <ActivityIndicator color={color.white} />
                )}
              </TouchableHighlight>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.backgroundContent,
    flex: 1
  },
  wrapLogo: {
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
    height: 200,
    backgroundColor: color.background
  },
  wrapInput: {
    borderBottomColor: color.backgroundContainer,
    borderBottomWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginVertical: 10
  }
});

const mapStateToProps = ({ auth }) => auth;

const mapDispatchToProps = {
  handleInputResetPassword,
  handleResetPassword
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPassword);
