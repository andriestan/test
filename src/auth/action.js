import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { Fetch, getUser } from '../public/services';
import { getNotification } from '../notification/action';

export const handleCheckAuth = check => async dispatch => {
  dispatch({ type: 'HANDLE_CHECK_AUTH', payload: check });
};

export const handleInput = data => dispatch => {
  dispatch({
    type: 'HANDLE_INPUT',
    payload: data
  });
};

export const handleInputRegister = data => dispatch => {
  dispatch({
    type: 'HANDLE_INPUT_REGISTER',
    payload: data
  });
};

export const handleInputResetPassword = data => dispatch => {
  dispatch({
    type: 'HANDLE_INPUT_RESET_PASSWORD',
    payload: data
  });
};

export const handleLogin = (data, navigation, getNews, getProfile) => async dispatch => {
  dispatch({ type: 'HANDLE_LOGIN_PENDING' });
  const response = await Fetch('/api/auth', data);
  if (response.api_status === 200) {
    await AsyncStorage.setItem('user', JSON.stringify(response));
    dispatch({ type: 'HANDLE_LOGIN_SUCCESS' });
    navigation.goBack();
    getNews();
    getProfile();
    dispatch(getNotification());
  } else {
    dispatch({
      type: 'HANDLE_LOGIN_ERROR'
    });
    Alert.alert(response.errors.error_text);
  }
};

export const handleRegister = (data, navigation, getNews, getProfile) => async dispatch => {
  dispatch({ type: 'HANDLE_REGISTER_PENDING' });
  const response = await Fetch('/api/create-account', data);
  if (response.api_status === 200) {
    await AsyncStorage.setItem('user', JSON.stringify(response));
    dispatch({ type: 'HANDLE_REGISTER_SUCCESS' });
    navigation.navigate('Home');
    getNews();
    getProfile();
  } else {
    dispatch({
      type: 'HANDLE_REGISTER_ERROR'
    });
    Alert.alert(response.errors.error_text);
  }
};

export const handleResetPassword = data => async dispatch => {
  if (data.email === '') {
    Alert.alert('Email required!');
  } else {
    dispatch({ type: 'HANDLE_RESET_PASSWORD_PENDING' });
    const response = await Fetch('/api/send-reset-password-email', data);
    if (response.api_status === 200) {
      dispatch({ type: 'HANDLE_RESET_PASSWORD_SUCCESS' });
      Alert.alert('Success! Check your email please!');
    } else {
      dispatch({
        type: 'HANDLE_RESET_PASSWORD_ERROR'
      });
      Alert.alert(response.errors.error_text);
    }
  }
};

export const handleLogout = (navigation, getNews) => async dispatch => {
  dispatch({ type: 'HANDLE_LOGOUT_PENDING' });
  const user = await getUser();
  await Fetch(`/api/delete-access-token?access_token=${user.access_token}`);
  await AsyncStorage.removeItem('user');
  dispatch({ type: 'HANDLE_LOGOUT_SUCCESS' });
  navigation.closeDrawer();
  navigation.navigate('Home');
  getNews();
};
