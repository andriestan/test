const initialState = {
  isLoading: false,
  isLoadingCheck: true,
  isLogin: false,
  input: {
    username: '',
    password: ''
  },
  inputRegister: {
    username: '',
    email: '',
    password: '',
    confirm_password: ''
  },
  inputResetPassword: {
    email: ''
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'HANDLE_CHECK_AUTH':
      return { ...state, isLogin: action.payload, isLoadingCheck: false };
    case 'HANDLE_INPUT':
      return {
        ...state,
        input: {
          ...state.input,
          [Object.keys(action.payload)]: Object.values(action.payload).toString()
        }
      };
    case 'HANDLE_INPUT_REGISTER':
      return {
        ...state,
        inputRegister: {
          ...state.inputRegister,
          [Object.keys(action.payload)]: Object.values(action.payload).toString()
        }
      };
    case 'HANDLE_INPUT_RESET_PASSWORD':
      return {
        ...state,
        inputResetPassword: {
          ...state.inputResetPassword,
          [Object.keys(action.payload)]: Object.values(action.payload).toString()
        }
      };
    case 'HANDLE_LOGIN_PENDING':
      return { ...state, isLoading: true };
    case 'HANDLE_LOGIN_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isLogin: true,
        input: { username: '', password: '' }
      };
    case 'HANDLE_LOGIN_ERROR':
      return {
        ...state,
        isLoading: false
      };
    case 'HANDLE_REGISTER_PENDING':
      return { ...state, isLoading: true };
    case 'HANDLE_REGISTER_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isLogin: true,
        inputRegister: {
          username: '',
          email: '',
          password: '',
          confirm_password: ''
        }
      };
    case 'HANDLE_REGISTER_ERROR':
      return {
        ...state,
        isLoading: false
      };
    case 'HANDLE_RESET_PASSWORD_PENDING':
      return { ...state, isLoading: true };
    case 'HANDLE_RESET_PASSWORD_SUCCESS':
      return {
        ...state,
        isLoading: false,
        inputResetPassword: { email: '' }
      };
    case 'HANDLE_RESET_PASSWORD_ERROR':
      return {
        ...state,
        isLoading: false,
        inputResetPassword: { email: '' }
      };
    case 'HANDLE_LOGOUT_PENDING':
      return { ...state, isLoading: true };
    case 'HANDLE_LOGOUT_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isLogin: false
      };
    default:
      return state;
  }
};
