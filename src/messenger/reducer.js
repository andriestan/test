const initialState = {
  isLoading: true,
  isLoadingMessanger: true,
  messages: [],
  messanger: [],
  isLoadingSend: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_MESSENGER_PENDING':
      return { ...state, isLoadingMessanger: true };
    case 'GET_MESSENGER_SUCCESS':
      return { ...state, isLoadingMessanger: false, messanger: action.payload };
    case 'GET_MESSAGES_PENDING':
      return { ...state, isLoading: true };
    case 'GET_MESSAGES_SUCCESS':
      return { ...state, isLoading: false, messages: action.payload };
    case 'SEND_MESSAGE_PENDING':
      return { ...state, isLoadingSend: true };
    // eslint-disable-next-line no-case-declarations
    case 'SEND_MESSAGE_SUCCESS':
      const messages = state.messages;
      messages.unshift(action.payload);
      return { ...state, isLoadingSend: false, messages };
    default:
      return state;
  }
};
