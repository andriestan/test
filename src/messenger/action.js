import { Fetch, getUser } from '../public/services';

export const getMessenger = () => async dispatch => {
  dispatch({ type: 'GET_MESSENGER_PENDING' });

  const user = await getUser();
  const data = {
    user_id: user.user_id,
    s: user.access_token
  };

  const response = await Fetch('/app_api.php?application=phone&type=get_messages', data);
  if (response.api_status === '200') {
    dispatch({
      type: 'GET_MESSENGER_SUCCESS',
      payload: response.messages
    });
  }
};

export const getMessages = recipientId => async dispatch => {
  dispatch({ type: 'GET_MESSAGES_PENDING' });

  const user = await getUser();
  const data = {
    user_id: user.user_id,
    recipient_id: recipientId,
    s: user.access_token
  };

  const response = await Fetch('/app_api.php?application=phone&type=get_user_messages', data);
  if (response.api_status === '200') {
    dispatch({
      type: 'GET_MESSAGES_SUCCESS',
      payload: response.messages
    });
  }
};

export const sendMessage = (userId, text) => async dispatch => {
  dispatch({ type: 'SEND_MESSAGE_PENDING' });

  const user = await getUser();
  const data = {
    user_id: userId,
    message_hash_id: 1,
    text
  };

  const response = await Fetch(`/api/send-message?access_token=${user.access_token}`, data);
  if (response.api_status === 200) {
    dispatch({
      type: 'SEND_MESSAGE_SUCCESS',
      payload: response.message_data[0]
    });
  }
};
