import React, { Component, Fragment } from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { getFollowing } from '../../profile/action';
import Shimmer from '../components/ShimmerMessenger';

const color = config.color;

class People extends Component {
  componentDidMount() {
    this.props.getFollowing();
  }

  renderItem = ({ item, index }) => (
    <Fragment>
      {index === 0 ? (
        <Fragment>
          <View style={{ paddingHorizontal: 20, marginTop: 15 }}>
            <TouchableHighlight
              style={{
                alignItems: 'center',
                paddingVertical: 10,
                paddingHorizontal: 20,
                backgroundColor: color.buttonEffect,
                flexDirection: 'row'
              }}
              underlayColor={color.backgroundContent}
              onPress={() => this.props.navigation.navigate('Search', { title: 'Search' })}
            >
              <Fragment>
                <Image
                  style={{ width: 16, height: 16, marginRight: 10 }}
                  source={require('../../public/assets/icon/search.png')}
                />
                <Text style={{ color: color.whitelv2 }}>Search users...</Text>
              </Fragment>
            </TouchableHighlight>
          </View>
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 20,
              marginTop: 20,
              marginBottom: 5
            }}
          >
            <Text style={{ fontWeight: 'bold', color: color.whitelv2, fontSize: 16 }}>
              Suggested
            </Text>
          </View>
        </Fragment>
      ) : null}
      <TouchableHighlight
        style={{ flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 5 }}
        onPress={() =>
          this.props.navigation.navigate('Chat', {
            title: item.username,
            recipientId: item.user_id
          })
        }
        underlayColor={color.buttonEffect}
      >
        <Fragment>
          <View style={styles.wrapPhotoProfile}>
            <Text style={{ fontSize: 8, color: color.whitelv2 }}>Image</Text>
            <Image source={{ uri: item.avatar }} style={styles.photoProfile} />
          </View>
          <View
            style={{
              flex: 1,
              paddingLeft: 10,
              justifyContent: 'center'
            }}
          >
            <Text
              style={{
                fontSize: 16,
                marginBottom: 3,
                color: color.white
              }}
            >
              {item.username}
            </Text>
          </View>
        </Fragment>
      </TouchableHighlight>
    </Fragment>
  );

  render() {
    return (
      <View style={styles.container}>
        {// eslint-disable-next-line no-nested-ternary
        !this.props.profile.isLoadingFollowing ? (
          this.props.profile.following.length > 0 ? (
            <FlatList
              data={this.props.profile.following}
              keyExtractor={(item, index) => index.toString()}
              renderItem={this.renderItem}
            />
          ) : (
            <Fragment>
              <View style={{ paddingHorizontal: 20, marginTop: 15 }}>
                <TouchableHighlight
                  style={{
                    alignItems: 'center',
                    paddingVertical: 10,
                    paddingHorizontal: 20,
                    backgroundColor: color.buttonEffect,
                    flexDirection: 'row'
                  }}
                  underlayColor={color.backgroundContent}
                  onPress={() => this.props.navigation.navigate('Search', { title: 'Search' })}
                >
                  <Fragment>
                    <Image
                      style={{ width: 16, height: 16, marginRight: 10 }}
                      source={require('../../public/assets/icon/search.png')}
                    />
                    <Text style={{ color: color.whitelv2 }}>Search users...</Text>
                  </Fragment>
                </TouchableHighlight>
              </View>
              <View
                style={{
                  paddingHorizontal: 20,
                  marginTop: 20,
                  marginBottom: 5
                }}
              >
                <Text style={{ fontWeight: 'bold', color: color.whitelv2, fontSize: 16 }}>
                  Suggested
                </Text>
              </View>
              <View
                style={{
                  paddingHorizontal: 20,
                  marginTop: 100,
                  marginBottom: 5,
                  alignItems: 'center'
                }}
              >
                <Text style={{ color: color.whitelv2 }}>Empty</Text>
              </View>
            </Fragment>
          )
        ) : (
          <Fragment>
            <View style={{ paddingHorizontal: 20, marginTop: 15 }}>
              <TouchableHighlight
                style={{
                  alignItems: 'center',
                  paddingVertical: 10,
                  paddingHorizontal: 20,
                  backgroundColor: color.buttonEffect,
                  flexDirection: 'row'
                }}
                underlayColor={color.backgroundContent}
                onPress={() => this.props.navigation.navigate('Search', { title: 'Search' })}
              >
                <Fragment>
                  <Image
                    style={{ width: 16, height: 16, marginRight: 10 }}
                    source={require('../../public/assets/icon/search.png')}
                  />
                  <Text style={{ color: color.whitelv2 }}>Search users...</Text>
                </Fragment>
              </TouchableHighlight>
            </View>
            <View
              style={{
                paddingHorizontal: 20,
                marginTop: 20,
                marginBottom: 5
              }}
            >
              <Text style={{ fontWeight: 'bold', color: color.whitelv2, fontSize: 16 }}>
                Suggested
              </Text>
            </View>
            <Shimmer />
          </Fragment>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.backgroundContent,
    flex: 1
  },
  wrapPhotoProfile: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
    borderColor: color.backgroundContainer,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoProfile: {
    width: '100%',
    height: '100%',
    borderRadius: 45 / 2,
    position: 'absolute'
  }
});

const mapStateToProps = state => ({
  profile: state.profile
});

const mapDispatchToProps = {
  getFollowing
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(People);
