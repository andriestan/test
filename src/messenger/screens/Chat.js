import React, { Component } from 'react';

import { Loading } from '../../public/components';
import ChatPage from '../components/Chat';
import config from '../../public/config/config.json';

const color = config.color;

class Chat extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true
    };
  }

  componentDidMount() {
    setTimeout(() => this.setState({ isLoading: false }), 1);
  }

  render() {
    return !this.state.isLoading ? (
      <ChatPage navigation={this.props.navigation} />
    ) : (
      <Loading
        style={{ flex: 1, backgroundColor: color.backgroundContainer }}
        color={color.white}
      />
    );
  }
}

export default Chat;
