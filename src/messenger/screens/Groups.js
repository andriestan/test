import React, { Component } from 'react';
import { View, Text } from 'react-native';

import config from '../../public/config/config.json';

const color = config.color;

class Groups extends Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: color.backgroundContent, alignItems: 'center' }}>
        <Text style={{ color: color.whitelv2, marginTop: 30 }}>
          Your group chats will appear here.
        </Text>
      </View>
    );
  }
}

export default Groups;
