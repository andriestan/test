import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  Keyboard,
  ScrollView,
  FlatList,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { getMessages, sendMessage } from '../action';
import Shimmer from './ShimmerChat';

const color = config.color;

class Chat extends Component {
  constructor() {
    super();
    this.state = {
      keyboard: false,
      message: '',
      refreshing: false
    };
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.handleKeyboardOn.bind(this)
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.handleKeyboardOff.bind(this)
    );
  }

  componentDidMount() {
    this.props.getMessages(this.props.navigation.getParam('recipientId'));
  }

  handleKeyboardOn() {
    this.setState({ keyboard: true });
  }

  handleKeyboardOff() {
    this.setState({ keyboard: false });
  }

  async handleSend() {
    await this.props.sendMessage(this.props.navigation.getParam('recipientId'), this.state.message);
    this.setState({ message: '' });
  }

  scrollToEnd = () => {
    this.flatList.scrollToEnd();
  };

  renderItem = ({ item, index }) => (
    <Fragment>
      {index === 0 && this.props.messanger.isLoadingSend ? (
        <View style={{ paddingHorizontal: 15, marginVertical: 5 }}>
          <View style={{ alignItems: 'flex-end' }}>
            <View
              style={{
                width: '60%',
                alignItems: 'flex-end'
              }}
            >
              <View
                style={{
                  backgroundColor: color.backgroundContent,
                  paddingHorizontal: 15,
                  height: 30,
                  borderRadius: 15,
                  width: '90%'
                }}
              />
            </View>
          </View>
        </View>
      ) : null}
      <View style={{ paddingHorizontal: 15, marginVertical: 5 }}>
        <View style={{ alignItems: item.position === 'left' ? 'flex-start' : 'flex-end' }}>
          <View
            style={{
              width: '60%',
              alignItems: item.position === 'left' ? 'flex-start' : 'flex-end'
            }}
          >
            <View
              style={{
                backgroundColor: color.backgroundContent,
                paddingHorizontal: 15,
                paddingVertical: 10,
                borderRadius: 15
              }}
            >
              <Text style={{ color: color.white }}>{item.text}</Text>
              <Text style={{ color: color.whitelv2, fontSize: 10, textAlign: 'right' }}>
                {item.time_text}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </Fragment>
  );

  render() {
    return (
      <View style={styles.container}>
        {// eslint-disable-next-line no-nested-ternary
        !this.props.messanger.isLoading ? (
          this.props.messanger.messages.length > 0 ? (
            <FlatList
              ref={list => (this.flatList = list)}
              data={this.props.messanger.messages}
              extraData={this.props}
              keyExtractor={(item, index) => index.toString()}
              inverted
              renderItem={this.renderItem}
            />
          ) : (
            <ScrollView style={{ paddingTop: 200 }}>
              <Text style={{ color: color.whitelv2, textAlign: 'center' }}>Chat</Text>
            </ScrollView>
          )
        ) : (
          <Shimmer />
        )}
        <View
          style={{
            paddingHorizontal: 10,
            borderTopColor: 'rgba(0,0,0,0.1)',
            borderTopWidth: 1
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              marginBottom: this.state.keyboard ? 10 : 5,
              marginTop: 5
            }}
          >
            <View
              style={{
                flex: 1,
                backgroundColor: color.backgroundContent,
                paddingHorizontal: 10,
                height: 50,
                justifyContent: 'center',
                borderRadius: 20
              }}
            >
              <TextInput
                placeholder="Write message"
                placeholderTextColor={color.whitelv2}
                style={{ paddingVertical: 10, color: color.white }}
                value={this.state.message}
                onChangeText={text => this.setState({ message: text })}
                onSubmitEditing={this.state.message === '' ? null : () => this.handleSend()}
                returnKeyType="send"
                blurOnSubmit={false}
              />
            </View>
            <TouchableHighlight
              style={{
                height: 50,
                width: 50,
                borderRadius: 50 / 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 5,
                backgroundColor: color.backgroundContent
              }}
              underlayColor={color.buttonEffect}
              disabled={this.state.message === ''}
              onPress={() => this.handleSend()}
            >
              {!this.props.messanger.isLoadingSend ? (
                <Text
                  style={{
                    color: this.state.message === '' ? color.whitelv2 : color.white,
                    fontSize: 10
                  }}
                >
                  Send
                </Text>
              ) : (
                <ActivityIndicator color={color.white} />
              )}
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContainer
  }
});

const mapStateToProps = state => ({
  messanger: state.messanger
});

const mapDispatchToProps = {
  getMessages,
  sendMessage
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
