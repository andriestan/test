import React, { Component } from 'react';
import { View, FlatList } from 'react-native';

import config from '../../public/config/config.json';

const color = config.color;

class ShimmerChat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          position: 'left',
          width: '70%'
        },
        {
          position: 'right',
          width: '50%'
        },
        {
          position: 'left',
          width: '100%'
        },
        {
          position: 'left',
          width: '70%'
        },
        {
          position: 'left',
          width: '50%'
        },
        {
          position: 'right',
          width: '50%'
        },
        {
          position: 'right',
          width: '70%'
        },
        {
          position: 'left',
          width: '70%'
        },
        {
          position: 'left',
          width: '50%'
        },
        {
          position: 'left',
          width: '70%'
        },
        {
          position: 'right',
          width: '100%'
        },
        {
          position: 'right',
          width: '70%'
        },
        {
          position: 'right',
          width: '30%'
        }
      ]
    };
  }
  render() {
    return (
      <FlatList
        data={this.state.data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <View style={{ paddingHorizontal: 15, marginVertical: 5 }}>
            <View style={{ alignItems: item.position === 'left' ? 'flex-start' : 'flex-end' }}>
              <View
                style={{
                  width: '60%',
                  alignItems: item.position === 'left' ? 'flex-start' : 'flex-end'
                }}
              >
                <View
                  style={{
                    backgroundColor: color.backgroundContent,
                    paddingHorizontal: 15,
                    height: 30,
                    borderRadius: 15,
                    width: item.width
                  }}
                />
              </View>
            </View>
          </View>
        )}
      />
    );
  }
}

export default ShimmerChat;
