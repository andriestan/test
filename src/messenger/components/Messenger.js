import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableHighlight
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { getMessenger } from '../action';
import Shimmer from './ShimmerMessenger';

const color = config.color;

class Messenger extends Component {
  componentDidMount() {
    this.props.getMessenger();
  }

  renderItem = ({ item, index }) => (
    <Fragment>
      {index === 0 ? (
        <Fragment>
          <View style={{ paddingHorizontal: 20, marginTop: 15 }}>
            <TouchableHighlight
              style={{
                alignItems: 'center',
                paddingVertical: 10,
                backgroundColor: color.buttonEffect
              }}
              underlayColor={color.buttonEffect}
            >
              <Text style={{ color: color.white, fontWeight: 'bold' }}>
                + CREATE CHAT
              </Text>
            </TouchableHighlight>
          </View>
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 20,
              marginTop: 15,
              marginBottom: 5
            }}
          >
            <Text
              style={{
                fontWeight: 'bold',
                color: color.whitelv2,
                fontSize: 16
              }}
            >
              Recent Contacts
            </Text>
          </View>
        </Fragment>
      ) : null}
      <TouchableHighlight
        style={{
          flexDirection: 'row',
          paddingHorizontal: 20,
          paddingVertical: 10
        }}
        onPress={() =>
          this.props.navigation.navigate('Chat', {
            title: item.messageUser.username,
            recipientId:
              item.from_id === this.props.profile.userData.user_id
                ? item.to_id
                : item.from_id
          })
        }
        underlayColor={color.buttonEffect}
      >
        <Fragment>
          <View style={styles.wrapPhotoProfile}>
            <Text style={{ fontSize: 10 }}>Image</Text>
            <Image
              source={{ uri: item.messageUser.avatar }}
              style={styles.photoProfile}
            />
          </View>
          <View
            style={{
              flex: 1,
              paddingLeft: 15,
              paddingTop: 3,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
          >
            <View>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  marginBottom: 3,
                  color: color.white
                }}
              >
                {item.messageUser.username}
              </Text>
              <Text style={{ fontSize: 14, color: color.white }}>
                {item.text}
              </Text>
            </View>
            <Text style={{ fontSize: 12, color: color.whitelv2 }}>
              {item.time_text}
            </Text>
          </View>
        </Fragment>
      </TouchableHighlight>
    </Fragment>
  );

  render() {
    return (
      <View style={styles.container}>
        {// eslint-disable-next-line no-nested-ternary
        !this.props.messanger.isLoadingMessanger ? (
          this.props.messanger.messanger.length > 0 ? (
            <FlatList
              data={this.props.messanger.messanger}
              keyExtractor={(item, index) => index.toString()}
              renderItem={this.renderItem}
            />
          ) : (
            <Fragment>
              <View style={{ paddingHorizontal: 20, marginTop: 15 }}>
                <TouchableHighlight
                  style={{
                    alignItems: 'center',
                    paddingVertical: 10,
                    backgroundColor: color.buttonEffect
                  }}
                  underlayColor={color.buttonEffect}
                  onPress={() => this.props.navigation.navigate('UsersSearch')}
                >
                  <Text style={{ color: color.white, fontWeight: 'bold' }}>
                    + CREATE CHAT
                  </Text>
                </TouchableHighlight>
              </View>
              <View
                style={{
                  paddingHorizontal: 20,
                  marginTop: 15,
                  marginBottom: 5
                }}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: color.whitelv2,
                    fontSize: 16
                  }}
                >
                  Recent Contacts
                </Text>
              </View>
              <View
                style={{
                  paddingHorizontal: 20,
                  marginTop: 100,
                  marginBottom: 5,
                  alignItems: 'center'
                }}
              >
                <Text style={{ color: color.whitelv2 }}>Empty</Text>
              </View>
            </Fragment>
          )
        ) : (
          <Fragment>
            <View style={{ paddingHorizontal: 20, marginTop: 15 }}>
              <TouchableHighlight
                style={{
                  alignItems: 'center',
                  paddingVertical: 10,
                  backgroundColor: color.buttonEffect
                }}
                underlayColor={color.buttonEffect}
              >
                <Text style={{ color: color.white, fontWeight: 'bold' }}>
                  + CREATE CHAT
                </Text>
              </TouchableHighlight>
            </View>
            <View
              style={{
                paddingHorizontal: 20,
                marginTop: 15,
                marginBottom: 5
              }}
            >
              <Text
                style={{
                  fontWeight: 'bold',
                  color: color.whitelv2,
                  fontSize: 16
                }}
              >
                Recent Contacts
              </Text>
            </View>
            <Shimmer />
          </Fragment>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.backgroundContent,
    flex: 1
  },
  wrapPhotoProfile: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    borderColor: color.backgroundContainer,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoProfile: {
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2,
    position: 'absolute'
  }
});

const mapStateToProps = state => ({
  messanger: state.messanger,
  profile: state.profile
});

const mapDispatchToProps = {
  getMessenger
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Messenger);
