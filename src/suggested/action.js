import { Fetch, getUser } from '../public/services';

export const getFriend = () => async dispatch => {
  dispatch({ type: 'GET_FRIEND_PENDING' });
  const user = await getUser();
  const data = {
    type: 'followers,following',
    user_id: user.user_id
  };

  const response = await Fetch(
    `/api/get-friends?access_token=${user.access_token}`,
    data
  );

  if (response.api_status === 200) {
    dispatch({ type: 'GET_FRIEND_SUCCESS', payload: response.data });
  }
};
