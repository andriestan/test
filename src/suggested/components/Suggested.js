import React, { Component, Fragment } from 'react';
import {
  Text,
  StyleSheet,
  View,
  SectionList,
  TouchableHighlight,
  Image,
  RefreshControl,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { getFriend } from '../action';
import Shimmer from '../components/Shimmer';

const color = config.color;

class Suggested extends Component {
  constructor() {
    super();
    this.state = {
      refreshing: false
    };
  }

  async componentDidMount() {
    this.props.getFriend();
  }

  onRefresh = async () => {
    await this.setState({ refreshing: true });
    this.props.getFriend();
    this.setState({ refreshing: false });
  };

  renderItem = ({ item }) => (
    <TouchableHighlight
      style={{
        flexDirection: 'row',
        paddingHorizontal: 20,
        paddingVertical: 10
      }}
      onPress={() =>
        this.props.navigation.navigate('Profile', {
          title: item.username,
          userData: item
        })
      }
      underlayColor={color.buttonEffect}
    >
      <Fragment>
        <View style={styles.wrapPhotoProfile}>
          <Text style={{ fontSize: 10 }}>Image</Text>
          <Image source={{ uri: item.avatar }} style={styles.photoProfile} />
        </View>
        <View
          style={{
            flex: 1,
            paddingLeft: 15,
            paddingTop: 3,
            justifyContent: 'center'
          }}
        >
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              marginBottom: 3,
              color: color.white
            }}
          >
            {item.username}
          </Text>
          <Text style={{ fontSize: 12, color: color.white }}>{item.name}</Text>
        </View>
      </Fragment>
    </TouchableHighlight>
  );

  render() {
    return !this.props.suggested.isLoading ? (
      <View style={styles.container}>
        <SectionList
          renderItem={this.renderItem}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }
          renderSectionHeader={({ section: { title } }) => (
            <View style={{ paddingHorizontal: 20, marginTop: 15 }}>
              <Text style={{ color: color.white, fontSize: 18 }}>{title}</Text>
            </View>
          )}
          renderSectionFooter={({ section }) => {
            return section.data.length === 0 ? (
              <View
                style={{
                  width: '100%',
                  height: 150,
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <Text style={{ color: color.whitelv2 }}>Empty</Text>
              </View>
            ) : null;
          }}
          sections={[
            { title: 'Friends', data: this.props.suggested.friends },
            { title: 'Following', data: this.props.suggested.following }
          ]}
          keyExtractor={(item, index) => item + index}
        />
      </View>
    ) : (
      <ScrollView
        scrollEnabled={false}
        style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
          />
        }
      >
        <Shimmer />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContent
  },
  wrapPhotoProfile: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    borderColor: color.backgroundContainer,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoProfile: {
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2,
    position: 'absolute'
  }
});

const mapStateToProps = state => ({
  suggested: state.suggested
});

const mapDispatchToProps = {
  getFriend
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Suggested);
