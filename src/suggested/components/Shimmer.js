import React, { Component } from 'react';
import { View, SectionList, StyleSheet, Text } from 'react-native';

import config from '../../public/config/config.json';

const color = config.color;

class Shimmer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          position: 'left',
          width: '50%'
        },
        {
          position: 'right',
          width: '50%'
        },
        {
          position: 'left',
          width: '50%'
        }
      ],
      data2: [
        {
          position: 'left',
          width: '50%'
        },
        {
          position: 'right',
          width: '50%'
        },
        {
          position: 'left',
          width: '50%'
        },
        {
          position: 'left',
          width: '50%'
        },
        {
          position: 'left',
          width: '50%'
        }
      ]
    };
  }

  renderItem = ({ item }) => (
    <View
      style={{
        flexDirection: 'row',
        paddingHorizontal: 20,
        paddingVertical: 10
      }}
    >
      <View style={styles.wrapPhotoProfile} />
      <View
        style={{
          flex: 1,
          paddingLeft: 15,
          paddingTop: 3,
          justifyContent: 'center'
        }}
      >
        <View
          style={{
            height: 16,
            marginBottom: 3,
            backgroundColor: color.backgroundContainer,
            width: item.width
          }}
        />
        <View
          style={{
            height: 12,
            backgroundColor: color.backgroundContainer,
            width: '10%'
          }}
        />
      </View>
    </View>
  );

  render() {
    return (
      <SectionList
        scrollEnabled={false}
        renderItem={this.renderItem}
        renderSectionHeader={({ section: { title } }) => (
          <View style={{ paddingHorizontal: 20, marginTop: 15 }}>
            <Text style={{ color: color.white, fontSize: 18 }}>{title}</Text>
          </View>
        )}
        sections={[
          { title: 'Friends', data: this.state.data },
          { title: 'Following', data: this.state.data2 }
        ]}
        keyExtractor={(item, index) => item + index}
      />
    );
  }
}

const styles = StyleSheet.create({
  wrapPhotoProfile: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    backgroundColor: color.backgroundContainer
  }
});

export default Shimmer;
