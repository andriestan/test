import React, { Component } from 'react';

import { Loading } from '../../public/components';
import SuggestedPage from '../components/Suggested';
import config from '../../public/config/config.json';

const color = config.color;

class Suggested extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true
    };
  }

  componentDidMount() {
    setTimeout(() => this.setState({ isLoading: false }), 1);
  }

  render() {
    return !this.state.isLoading ? (
      <SuggestedPage navigation={this.props.navigation} />
    ) : (
      <Loading
        style={{ flex: 1, backgroundColor: color.backgroundContainer }}
        color={color.white}
      />
    );
  }
}

export default Suggested;
