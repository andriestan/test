const initialState = {
  isLoading: true,
  friends: [],
  following: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_FRIEND_PENDING':
      return { ...state, isLoading: true };
    // eslint-disable-next-line no-case-declarations
    case 'GET_FRIEND_SUCCESS':
      const friends = action.payload.followers.filter(
        data => data.is_following === 1
      );
      return {
        ...state,
        isLoading: false,
        friends,
        following: action.payload.following
      };
    default:
      return state;
  }
};
