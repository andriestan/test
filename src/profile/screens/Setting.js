import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableHighlight,
  Modal,
  TextInput,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import {
  updateUserData,
  updateNameUserData,
  updatePasswordUserData
} from '../action';
import { Image as ImageComponent } from '../../public/components';

const color = config.color;

class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      name: {
        first_name: '',
        last_name: ''
      },
      password: {
        current_password: '',
        new_password: ''
      },
      new_password_confirm: '',
      hidePassword: {
        current_password: true,
        new_password: true,
        new_password_confirm: true
      },
      text: '',
      title: '',
      field: '',
      data: [
        { name: 'Username', title: 'username', field: 'username' },
        { name: 'Name', title: 'name', field: 'name' },
        { name: 'Email', title: 'email', field: 'email' },
        { name: 'Phone', title: 'phone', field: 'phone_number' },
        { name: 'About', title: 'about', field: 'about' },
        { name: 'Website', title: 'website', field: 'website' },
        { name: 'Password', title: 'password', field: 'password' }
      ]
    };
  }

  async handleSave() {
    if (this.state.title === 'name') {
      await this.props.updateNameUserData(
        this.state.name,
        this.props.navigation
      );
      this.setState({ visible: false });
    } else if (this.state.title === 'password') {
      const result = await this.props.updatePasswordUserData(
        this.state.password
      );
      if (result) {
        this.setState({
          visible: false,
          password: {
            current_password: '',
            new_password: ''
          },
          new_password_confirm: '',
          hidePassword: {
            current_password: true,
            new_password: true,
            new_password_confirm: true
          }
        });
      }
    } else {
      await this.props.updateUserData(
        this.state.field,
        this.state.text,
        this.props.navigation
      );
      this.setState({ visible: false });
    }
  }

  renderText(title) {
    if (title === 'username') {
      // eslint-disable-next-line no-unused-expressions
      return this.props.profile.userData.username;
    } else if (title === 'email') {
      // eslint-disable-next-line no-unused-expressions
      return this.props.profile.userData.email;
    } else if (title === 'phone') {
      // eslint-disable-next-line no-unused-expressions
      return this.props.profile.userData.phone_number;
    } else if (title === 'password') {
      // eslint-disable-next-line no-unused-expressions
      return '********';
    } else if (title === 'about') {
      // eslint-disable-next-line no-unused-expressions
      return this.props.profile.userData.about;
    } else if (title === 'website') {
      // eslint-disable-next-line no-unused-expressions
      return this.props.profile.userData.website;
    } else if (title === 'name') {
      // eslint-disable-next-line no-unused-expressions
      return this.props.profile.userData.name;
    }
  }

  renderItem = ({ item }) => (
    <TouchableHighlight
      style={{ paddingHorizontal: 20 }}
      underlayColor={color.buttonEffect}
      // eslint-disable-next-line no-confusing-arrow
      onPress={() =>
        // eslint-disable-next-line no-nested-ternary
        item.title === 'name'
          ? this.setState({
              visible: true,
              title: item.title,
              field: item.field,
              name: {
                first_name: this.props.profile.userData.first_name,
                last_name: this.props.profile.userData.last_name
              }
            })
          : item.title === 'password'
          ? this.setState({
              visible: true,
              title: item.title,
              field: item.field
            })
          : this.setState({
              visible: true,
              title: item.title,
              field: item.field,
              text: this.renderText(item.title)
            })
      }
      disabled={item.title === 'username'}
    >
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingVertical: 15,
          borderBottomColor: color.whitelv2,
          borderBottomWidth: 1
        }}
      >
        <View>
          <Text style={{ color: color.whitelv2 }}>{item.name}</Text>
          {!this.props.profile.isLoading ? (
            <Text style={{ color: color.white, fontSize: 20 }}>
              {this.renderText(item.title) === '' ||
              this.renderText(item.title) === null
                ? 'empty'
                : this.renderText(item.title)}
            </Text>
          ) : (
            <Text style={{ color: color.white, fontSize: 20 }}>Loading</Text>
          )}
        </View>
        {item.title !== 'username' ? (
          <View>
            <Text style={{ color: color.whitelv2 }}>Edit</Text>
          </View>
        ) : null}
      </View>
    </TouchableHighlight>
  );

  render() {
    return (
      <ScrollView style={styles.container} keyboardShouldPersistTaps="handled">
        <Modal
          transparent
          animationType="slide"
          visible={this.state.visible}
          onRequestClose={() => this.setState({ visible: false })}
        >
          <View style={{ flex: 1, justifyContent: 'flex-end' }}>
            <View
              style={{
                padding: 20,
                borderRadius: 10,
                backgroundColor: color.white
              }}
            >
              {this.state.title !== 'name' &&
              this.state.title !== 'password' ? (
                <Fragment>
                  <View>
                    <Text style={{ fontWeight: 'bold' }}>
                      Enter your {this.state.title}
                    </Text>
                  </View>
                  <View
                    style={{
                      borderBottomColor: color.blue,
                      borderBottomWidth: 1,
                      paddingVertical: 5,
                      marginTop: 10
                    }}
                  >
                    <TextInput
                      value={this.state.text}
                      style={{ padding: 0 }}
                      onChangeText={text => this.setState({ text })}
                      autoFocus
                      autoCapitalize={
                        this.state.title === 'website' ? 'none' : 'sentences'
                      }
                      multiline={this.state.title === 'about'}
                      keyboardType={
                        // eslint-disable-next-line no-nested-ternary
                        this.state.title === 'email'
                          ? 'email-address'
                          : this.state.title === 'phone'
                          ? 'number-pad'
                          : 'default'
                      }
                    />
                  </View>
                </Fragment>
              ) : null}

              {this.state.title === 'name' ? (
                <Fragment>
                  <View style={{ marginBottom: 30 }}>
                    <View>
                      <Text style={{ fontWeight: 'bold' }}>
                        Enter your first name
                      </Text>
                    </View>
                    <View
                      style={{
                        borderBottomColor: color.blue,
                        borderBottomWidth: 1,
                        paddingVertical: 5,
                        marginTop: 10
                      }}
                    >
                      <TextInput
                        value={this.state.name.first_name}
                        style={{ padding: 0 }}
                        autoFocus
                        onChangeText={text =>
                          this.setState({
                            name: {
                              first_name: text,
                              last_name: this.state.name.last_name
                            }
                          })
                        }
                      />
                    </View>
                  </View>
                  <View>
                    <View>
                      <Text style={{ fontWeight: 'bold' }}>
                        Enter your last name
                      </Text>
                    </View>
                    <View
                      style={{
                        borderBottomColor: color.blue,
                        borderBottomWidth: 1,
                        paddingVertical: 5,
                        marginTop: 10
                      }}
                    >
                      <TextInput
                        value={this.state.name.last_name}
                        style={{ padding: 0 }}
                        onChangeText={text =>
                          this.setState({
                            name: {
                              first_name: this.state.name.first_name,
                              last_name: text
                            }
                          })
                        }
                      />
                    </View>
                  </View>
                </Fragment>
              ) : null}

              {this.state.title === 'password' ? (
                <Fragment>
                  <View style={{ marginBottom: 20 }}>
                    <View>
                      <Text style={{ fontWeight: 'bold' }}>
                        Enter your current password
                      </Text>
                    </View>
                    <View
                      style={{ flexDirection: 'row', alignItems: 'center' }}
                    >
                      <View
                        style={{
                          borderBottomColor: color.blue,
                          borderBottomWidth: 1,
                          paddingVertical: 5,
                          marginTop: 10,
                          flex: 1,
                          paddingRight: 10
                        }}
                      >
                        <TextInput
                          style={{ padding: 0 }}
                          autoFocus
                          keyboardType={
                            this.state.hidePassword.current_password
                              ? 'default'
                              : 'visible-password'
                          }
                          placeholder="Current Password"
                          secureTextEntry
                          value={this.state.password.current_password}
                          onChangeText={text =>
                            this.setState({
                              password: {
                                current_password: text,
                                new_password: this.state.password.new_password
                              }
                            })
                          }
                        />
                      </View>
                      <TouchableOpacity
                        style={{
                          marginTop: 15
                        }}
                        onPress={() =>
                          this.setState({
                            hidePassword: {
                              current_password: !this.state.hidePassword
                                .current_password,
                              new_password: this.state.hidePassword
                                .new_password,
                              new_password_confirm: this.state.hidePassword
                                .new_password_confirm
                            }
                          })
                        }
                      >
                        <Text>
                          {!this.state.hidePassword.current_password
                            ? 'Hide'
                            : 'Show'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{ marginBottom: 20 }}>
                    <View>
                      <Text style={{ fontWeight: 'bold' }}>
                        Enter your new password
                      </Text>
                    </View>
                    <View
                      style={{ flexDirection: 'row', alignItems: 'center' }}
                    >
                      <View
                        style={{
                          borderBottomColor: color.blue,
                          borderBottomWidth: 1,
                          paddingVertical: 5,
                          marginTop: 10,
                          flex: 1,
                          paddingRight: 10
                        }}
                      >
                        <TextInput
                          style={{ padding: 0 }}
                          keyboardType={
                            this.state.hidePassword.new_password
                              ? 'default'
                              : 'visible-password'
                          }
                          placeholder="New Password"
                          secureTextEntry
                          value={this.state.password.new_password}
                          onChangeText={text =>
                            this.setState({
                              password: {
                                current_password: this.state.password
                                  .current_password,
                                new_password: text
                              }
                            })
                          }
                        />
                      </View>
                      <TouchableOpacity
                        style={{
                          marginTop: 15
                        }}
                        onPress={() =>
                          this.setState({
                            hidePassword: {
                              current_password: this.state.hidePassword
                                .current_password,
                              new_password: !this.state.hidePassword
                                .new_password,
                              new_password_confirm: this.state.hidePassword
                                .new_password_confirm
                            }
                          })
                        }
                      >
                        <Text>
                          {!this.state.hidePassword.new_password
                            ? 'Hide'
                            : 'Show'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View>
                    <View>
                      <Text style={{ fontWeight: 'bold' }}>
                        Enter your new password confirm
                      </Text>
                    </View>
                    <View
                      style={{ flexDirection: 'row', alignItems: 'center' }}
                    >
                      <View
                        style={{
                          borderBottomColor: color.blue,
                          borderBottomWidth: 1,
                          paddingVertical: 5,
                          marginTop: 10,
                          flex: 1,
                          paddingRight: 10
                        }}
                      >
                        <TextInput
                          style={{ padding: 0 }}
                          keyboardType={
                            this.state.hidePassword.new_password_confirm
                              ? 'default'
                              : 'visible-password'
                          }
                          placeholder="New Password Confirm"
                          secureTextEntry
                          value={this.state.new_password_confirm}
                          onChangeText={text =>
                            this.setState({
                              new_password_confirm: text
                            })
                          }
                        />
                      </View>
                      <TouchableOpacity
                        style={{
                          marginTop: 15
                        }}
                        onPress={() =>
                          this.setState({
                            hidePassword: {
                              current_password: this.state.hidePassword
                                .current_password,
                              new_password: this.state.hidePassword
                                .new_password,
                              new_password_confirm: !this.state.hidePassword
                                .new_password_confirm
                            }
                          })
                        }
                      >
                        <Text>
                          {!this.state.hidePassword.new_password_confirm
                            ? 'Hide'
                            : 'Show'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </Fragment>
              ) : null}

              <View style={{ alignItems: 'flex-end', marginTop: 20 }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: '30%'
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.setState({ visible: false })}
                    disabled={this.props.profile.isLoadingUpdateUserData}
                  >
                    <Text style={{ color: color.blue, fontWeight: 'bold' }}>
                      Cancel
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.handleSave()}
                    disabled={
                      // eslint-disable-next-line no-nested-ternary
                      this.props.profile.isLoadingUpdateUserData ||
                      this.state.title === 'name'
                        ? this.state.name.first_name ===
                            this.props.profile.userData.first_name &&
                          this.state.name.last_name ===
                            this.props.profile.userData.last_name
                        : this.state.text ===
                            this.renderText(this.state.title) ||
                          this.state.title === 'password'
                        ? this.state.password.new_password === '' ||
                          this.state.password.current_password === '' ||
                          this.state.new_password_confirm === '' ||
                          this.state.password.new_password !==
                            this.state.new_password_confirm
                        : this.state.text === this.renderText(this.state.title)
                    }
                  >
                    {!this.props.profile.isLoadingUpdateUserData ? (
                      <Text
                        style={{
                          color: color.blue,
                          fontWeight: 'bold',
                          opacity:
                            // eslint-disable-next-line no-nested-ternary
                            this.state.title === 'name'
                              ? this.state.name.first_name ===
                                  this.props.profile.userData.first_name &&
                                this.state.name.last_name ===
                                  this.props.profile.userData.last_name
                                ? 0.5
                                : 1
                              : // eslint-disable-next-line no-nested-ternary
                              this.state.title === 'password'
                              ? // eslint-disable-next-line no-nested-ternary
                                this.state.password.new_password === '' ||
                                this.state.password.current_password === '' ||
                                this.state.new_password_confirm === ''
                                ? 0.5
                                : this.state.password.new_password ===
                                  this.state.new_password_confirm
                                ? 1
                                : 0.5
                              : this.state.text ===
                                this.renderText(this.state.title)
                              ? 0.5
                              : 1
                        }}
                      >
                        Save
                      </Text>
                    ) : (
                      <ActivityIndicator color={color.blue} />
                    )}
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </Modal>
        <View style={{ alignItems: 'center' }}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('EditCover', {
                title: 'Cover'
              })
            }
          >
            <ImageComponent
              uri={this.props.profile.userData.cover}
              style={{
                borderTopColor: color.backgroundContainer,
                borderTopWidth: 1,
                borderBottomColor: color.backgroundContainer,
                borderBottomWidth: 1
              }}
            />
            <View
              style={{
                backgroundColor: color.blue,
                width: 40,
                height: 40,
                borderRadius: 40 / 2,
                position: 'absolute',
                bottom: -20,
                right: 20,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Text style={{ color: color.white, fontSize: 12 }}>Edit</Text>
            </View>
          </TouchableOpacity>
          <View style={{ position: 'absolute', bottom: -125 / 2 }}>
            <TouchableOpacity
              style={{ width: 125, height: 125 }}
              onPress={() =>
                this.props.navigation.navigate('EditAvatar', {
                  title: 'Avatar'
                })
              }
            >
              <Image
                source={{ uri: this.props.profile.userData.avatar }}
                style={{
                  width: '100%',
                  height: '100%',
                  borderRadius: 125 / 2
                }}
              />
              <View
                style={{
                  backgroundColor: color.blue,
                  width: 40,
                  height: 40,
                  borderRadius: 40 / 2,
                  position: 'absolute',
                  bottom: 0,
                  right: 0,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text style={{ color: color.white, fontSize: 12 }}>Edit</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ marginVertical: 30, marginTop: 80 }}>
          <FlatList
            data={this.state.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderItem}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContent
  }
});

const mapStateToProps = state => ({
  profile: state.profile
});

const mapDispatchToProps = {
  updateUserData,
  updateNameUserData,
  updatePasswordUserData
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Setting);
