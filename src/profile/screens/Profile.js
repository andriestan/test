import React, { Component } from 'react';

import { Loading } from '../../public/components';
import ProfilePage from '../components/Profile';
import config from '../../public/config/config.json';

const color = config.color;

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true
    };
  }

  componentDidMount() {
    setTimeout(() => this.setState({ isLoading: false }), 1);
  }

  render() {
    return !this.state.isLoading ? (
      <ProfilePage navigation={this.props.navigation} />
    ) : (
      <Loading
        style={{ flex: 1, backgroundColor: color.backgroundContainer }}
        color={color.white}
      />
    );
  }
}

export default Profile;
