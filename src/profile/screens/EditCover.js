import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { Image } from '../../public/components';

const color = config.color;

class EditCover extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          uri={this.props.profile.userData.cover}
          style={{
            borderTopColor: color.backgroundContainer,
            borderTopWidth: 1,
            borderBottomColor: color.backgroundContainer,
            borderBottomWidth: 1
          }}
        />
        {this.props.profile.isLoadingUpdateUserData ? (
          <View
            style={{
              alignItems: 'center',
              position: 'absolute',
              width: '100%'
            }}
          >
            <ActivityIndicator color={color.blue} size="large" />
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContent,
    justifyContent: 'center'
  }
});

const mapStateToProps = state => ({
  profile: state.profile
});

export default connect(mapStateToProps)(EditCover);
