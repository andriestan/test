import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableHighlight,
  Text,
  TouchableOpacity,
  Modal,
  Platform
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { updateUserData } from '../action';

const color = config.color;

class HeaderEditCover extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  openCamera() {
    this.setState({ visible: false });
    ImagePicker.openCamera({
      width: 918,
      height: 300,
      cropping: true
    }).then(image => {
      const data = {
        name: `${image.modificationDate}.jpg`,
        type: image.mime,
        uri:
          Platform.OS === 'android'
            ? image.path
            : image.path.replace('file://', '')
      };
      this.props.updateUserData('cover', data);
    });
  }

  openPicker() {
    this.setState({ visible: false });
    ImagePicker.openPicker({
      width: 918,
      height: 300,
      cropping: true
    }).then(image => {
      const data = {
        name: `${image.modificationDate}.jpg`,
        type: image.mime,
        uri:
          Platform.OS === 'android'
            ? image.path
            : image.path.replace('file://', '')
      };
      this.props.updateUserData('cover', data);
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Modal
          transparent
          animationType="slide"
          visible={this.state.visible}
          onRequestClose={() => this.setState({ visible: false })}
        >
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              backgroundColor: 'rgba(0,0,0,0.2)'
            }}
          >
            <View
              style={{
                borderRadius: 10,
                backgroundColor: color.white
              }}
            >
              <TouchableHighlight
                style={{
                  paddingVertical: 20,
                  alignItems: 'center',
                  paddingHorizontal: 20
                }}
                underlayColor={color.whitelv2}
                onPress={() => this.openCamera()}
              >
                <Text style={{ fontSize: 20 }}>Camera</Text>
              </TouchableHighlight>
              <TouchableHighlight
                style={{
                  paddingVertical: 20,
                  alignItems: 'center',
                  paddingHorizontal: 20
                }}
                underlayColor={color.whitelv2}
                onPress={() => this.openPicker()}
              >
                <Text style={{ fontSize: 20 }}>Galery</Text>
              </TouchableHighlight>
            </View>
            <TouchableHighlight
              onPress={() => this.setState({ visible: false })}
              underlayColor={color.whitelv2}
              style={{
                padding: 20,
                borderRadius: 10,
                backgroundColor: color.white,
                marginVertical: 20,
                alignItems: 'center'
              }}
            >
              <Text style={{ fontSize: 20 }}>Cancel</Text>
            </TouchableHighlight>
          </View>
        </Modal>
        <TouchableHighlight
          style={{
            width: '15%',
            alignItems: 'center',
            height: '100%',
            justifyContent: 'center'
          }}
          underlayColor={color.backgroundlv2}
          onPress={() => this.props.navigation.goBack()}
        >
          <Image
            source={require('../../public/assets/icon/ic-back.png')}
            style={{ width: '35%', height: '35%' }}
          />
        </TouchableHighlight>
        <View
          style={{ width: '70%', justifyContent: 'center', height: '100%' }}
        >
          <Text style={{ color: '#fff', fontSize: 20 }}>
            {this.props.navigation.getParam('title')}
          </Text>
        </View>
        <TouchableOpacity
          style={{
            width: '15%',
            alignItems: 'center',
            height: '100%',
            justifyContent: 'center'
          }}
          onPress={() => this.setState({ visible: true })}
          disabled={this.props.profile.isLoadingUpdateUserData}
        >
          <Text
            style={{
              color: color.white
            }}
          >
            edit
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.background,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center'
  }
});

const mapStateToProps = state => ({
  profile: state.profile
});

const mapDispatchToProps = {
  updateUserData
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderEditCover);
