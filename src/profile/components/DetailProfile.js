import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  Modal
} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';

import config from '../../public/config/config.json';
import { Image as ImageComponent, Spinner } from '../../public/components';

const color = config.color;

class DetailProfile extends Component {
  constructor() {
    super();
    this.state = {
      visibleImage: false,
      image: []
    };
  }

  render() {
    const {
      data,
      isLogin,
      isLoading,
      handleFollow,
      handleChat,
      isLoadingFollow,
      navigation
    } = this.props;

    return (
      <View style={styles.wrapProfile}>
        <Spinner
          style={{ backgroundColor: 'rgba(0,0,0,0.5)' }}
          color={color.backgroundContent}
          visible={isLoadingFollow}
        />
        <View style={{ alignItems: 'center', paddingBottom: 100 / 2 }}>
          <TouchableHighlight
            underlayColor="rgba(0,0,0,0)"
            onPress={() =>
              this.setState({
                visibleImage: true,
                image: [{ url: data.cover }]
              })
            }
          >
            <ImageComponent uri={data.cover} />
          </TouchableHighlight>
          <TouchableHighlight
            underlayColor="rgba(0,0,0,0)"
            onPress={() =>
              this.setState({
                visibleImage: true,
                image: [{ url: data.avatar }]
              })
            }
            style={{
              width: 100,
              height: 100,
              borderRadius: 100 / 2,
              position: 'absolute',
              bottom: 0
            }}
          >
            <Image
              source={{ uri: data.avatar }}
              style={{
                width: '100%',
                height: '100%',
                borderRadius: 100 / 2
              }}
            />
          </TouchableHighlight>
        </View>
        <View style={{ alignItems: 'center', marginVertical: 10 }}>
          <Text
            style={{ fontWeight: 'bold', fontSize: 16, color: color.white }}
          >
            {data.name}
          </Text>
          {data.about !== null ? (
            <Text style={{ color: color.whitelv2 }}>{data.about}</Text>
          ) : null}
          {data.website !== '' ? (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('Browser', {
                  title: 'Oxa',
                  url: data.website
                })
              }
            >
              <Text
                style={{ color: color.blue, textDecorationLine: 'underline' }}
              >
                {data.website}
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
        <View style={{ alignItems: 'center' }}>
          <View style={{ flexDirection: 'row', width: '90%' }}>
            <View style={{ alignItems: 'center', flex: 1 }}>
              <Text
                style={{ fontWeight: 'bold', fontSize: 16, color: color.white }}
              >
                {data.details.post_count}
              </Text>
              <Text style={{ color: color.whitelv2 }}>Posts</Text>
            </View>
            <View style={{ alignItems: 'center', flex: 1 }}>
              <Text
                style={{ fontWeight: 'bold', fontSize: 16, color: color.white }}
              >
                {data.details.followers_count}
              </Text>
              <Text style={{ color: color.whitelv2 }}>Followers</Text>
            </View>
            <View style={{ alignItems: 'center', flex: 1 }}>
              <Text
                style={{ fontWeight: 'bold', fontSize: 16, color: color.white }}
              >
                {data.details.following_count}
              </Text>
              <Text style={{ color: color.whitelv2 }}>Following</Text>
            </View>
          </View>
          {// eslint-disable-next-line no-nested-ternary
          isLogin ? (
            data.is_following ? (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: '80%',
                  marginVertical: 10
                }}
              >
                <TouchableHighlight
                  style={{
                    backgroundColor: color.blue,
                    alignItems: 'center',
                    width: '80%',
                    paddingVertical: 10,
                    borderRadius: 20
                  }}
                  underlayColor={color.buttonEffect}
                  // eslint-disable-next-line no-confusing-arrow
                  onPress={handleFollow}
                  disabled={isLogin ? isLoading : false}
                >
                  <Text style={{ color: '#fff' }}>Following</Text>
                </TouchableHighlight>
                <TouchableHighlight
                  style={{
                    width: '17%',
                    backgroundColor: color.blue,
                    alignItems: 'center',
                    paddingVertical: 10,
                    borderRadius: 20
                  }}
                  onPress={handleChat}
                  underlayColor={color.buttonEffect}
                >
                  <Image
                    source={require('../../public/assets/icon/messenger-white.png')}
                    style={{ width: 20, height: 20 }}
                  />
                </TouchableHighlight>
              </View>
            ) : (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: '80%',
                  marginVertical: 10
                }}
              >
                <TouchableHighlight
                  style={{
                    backgroundColor: color.blue,
                    alignItems: 'center',
                    width: '80%',
                    paddingVertical: 10,
                    borderRadius: 20
                  }}
                  underlayColor={color.buttonEffect}
                  // eslint-disable-next-line no-confusing-arrow
                  onPress={handleFollow}
                  disabled={isLogin ? isLoading : false}
                >
                  <Text style={{ color: '#fff' }}>
                    {// eslint-disable-next-line no-nested-ternary
                    isLoading
                      ? 'Loading'
                      : // eslint-disable-next-line no-nested-ternary
                      data.is_following === 1
                      ? 'Following'
                      : data.is_following_me
                      ? 'Follow back'
                      : 'Follow'}
                  </Text>
                </TouchableHighlight>
                <TouchableHighlight
                  style={{
                    width: '17%',
                    backgroundColor: color.blue,
                    alignItems: 'center',
                    paddingVertical: 10,
                    borderRadius: 20
                  }}
                  onPress={handleChat}
                  underlayColor={color.buttonEffect}
                  disabled={isLogin ? isLoading : false}
                >
                  <Image
                    source={require('../../public/assets/icon/messenger-white.png')}
                    style={{ width: 20, height: 20 }}
                  />
                </TouchableHighlight>
              </View>
            )
          ) : (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '80%',
                marginVertical: 10
              }}
            >
              <TouchableHighlight
                style={{
                  backgroundColor: color.blue,
                  alignItems: 'center',
                  width: '80%',
                  paddingVertical: 10,
                  borderRadius: 20
                }}
                underlayColor={color.buttonEffect}
                // eslint-disable-next-line no-confusing-arrow
                onPress={handleFollow}
              >
                <Text style={{ color: '#fff' }}>Follow</Text>
              </TouchableHighlight>
              <TouchableHighlight
                style={{
                  width: '17%',
                  backgroundColor: color.blue,
                  alignItems: 'center',
                  paddingVertical: 10,
                  borderRadius: 20
                }}
                onPress={handleFollow}
                underlayColor={color.buttonEffect}
              >
                <Image
                  source={require('../../public/assets/icon/messenger-white.png')}
                  style={{ width: 20, height: 20 }}
                />
              </TouchableHighlight>
            </View>
          )}
        </View>
        <Modal
          transparent
          animationType="fade"
          visible={this.state.visibleImage}
          onRequestClose={() => this.setState({ visibleImage: false })}
          transparent
        >
          <ImageViewer
            imageUrls={this.state.image}
            onSwipeDown={() => this.setState({ visibleImage: false })}
            enableSwipeDown
            swipeDownThreshold={1}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapProfile: {
    backgroundColor: color.backgroundContent
  }
});

export default DetailProfile;
