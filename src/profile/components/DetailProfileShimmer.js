import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import config from '../../public/config/config.json';

const color = config.color;

class DetailProfileShimmer extends Component {
  render() {
    return (
      <View style={styles.wrapProfile}>
        <View style={{ alignItems: 'center', paddingBottom: 100 / 2 }}>
          <View
            style={{ backgroundColor: color.backgroundContainer, width: '100%', height: 100 }}
          />
          <View
            style={{
              width: 100,
              height: 100,
              borderRadius: 100 / 2,
              position: 'absolute',
              bottom: 0,
              backgroundColor: color.backgroundContainer,
              borderColor: 'rgba(0,0,0,0.1)',
              borderWidth: 1
            }}
          />
        </View>
        <View style={{ alignItems: 'center', marginVertical: 10 }}>
          <View style={{ backgroundColor: color.backgroundContainer, height: 16, width: '40%' }} />
          <View
            style={{
              backgroundColor: color.backgroundContainer,
              height: 16,
              width: '40%',
              marginTop: 5
            }}
          />
          <View
            style={{
              backgroundColor: color.backgroundContainer,
              height: 16,
              width: '40%',
              marginTop: 5
            }}
          />
        </View>
        <View style={{ alignItems: 'center' }}>
          <View style={{ flexDirection: 'row', width: '90%' }}>
            <View style={{ alignItems: 'center', flex: 1 }}>
              <View
                style={{ backgroundColor: color.backgroundContainer, height: 16, width: '70%' }}
              />
              <View
                style={{
                  backgroundColor: color.backgroundContainer,
                  height: 16,
                  width: '80%',
                  marginTop: 5
                }}
              />
            </View>
            <View style={{ alignItems: 'center', flex: 1 }}>
              <View
                style={{ backgroundColor: color.backgroundContainer, height: 16, width: '70%' }}
              />
              <View
                style={{
                  backgroundColor: color.backgroundContainer,
                  height: 16,
                  width: '80%',
                  marginTop: 5
                }}
              />
            </View>
            <View style={{ alignItems: 'center', flex: 1 }}>
              <View
                style={{ backgroundColor: color.backgroundContainer, height: 16, width: '70%' }}
              />
              <View
                style={{
                  backgroundColor: color.backgroundContainer,
                  height: 16,
                  width: '80%',
                  marginTop: 5
                }}
              />
            </View>
          </View>
          <View
            style={{
              backgroundColor: color.backgroundContainer,
              alignItems: 'center',
              width: '80%',
              paddingVertical: 18,
              borderRadius: 20,
              marginVertical: 10
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapProfile: {
    backgroundColor: color.backgroundContent
  }
});

export default DetailProfileShimmer;
