import React, { Component, Fragment } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  ScrollView,
  RefreshControl,
  Text
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import {
  storeUserProfile,
  getUserProfile,
  handleFollow,
  getPostsUser,
  updatePostsUser,
  handleLikeProfile,
  searchIdUser
} from '../action';
import DetailProfile from '../components/DetailProfile';
import { Post, Loading, PostShare } from '../../public/components';
import PostShimmer from '../components/PostShimmer';
import DetailShimmer from './DetailProfileShimmer';

const color = config.color;

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      refreshing: false,
      getUser: false
    };
  }

  async componentDidMount() {
    if (!this.props.navigation.getParam('mention')) {
      await this.props.storeUserProfile(
        this.props.navigation.getParam('userData')
      );
    } else {
      await this.props.searchIdUser(this.props.navigation.getParam('username'));
      await this.setState({ getUser: true });
    }
    this.props.getPostsUser(this.props.profile.userProfileData.user_id);
    if (this.props.auth.isLogin) {
      if (!this.state.getUser) {
        this.props.getUserProfile(this.props.profile.userProfileData.user_id);
      }
    }
  }

  componentWillUnmount() {
    this.props.storeUserProfile({});
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    this.props.getPostsUser(this.props.profile.userProfileData.user_id);
    if (this.props.auth.isLogin) {
      this.props.getUserProfile(this.props.profile.userProfileData.user_id);
    }
    this.setState({ refreshing: false });
  };

  onEndReached = () => {
    if (!this.props.profile.isLoadingPostUserUpdate) {
      this.props.updatePostsUser(
        this.props.profile.userProfileData.user_id,
        this.props.profile.postsUser[this.props.profile.postsUser.length - 1]
          .post_id
      );
    }
  };

  handleFollow() {
    this.props.handleFollow(this.props.profile.userProfileData.user_id);
  }

  render() {
    const { profile, auth, navigation } = this.props;

    return profile.userProfileData.avatar ? (
      <View style={styles.container}>
        {// eslint-disable-next-line no-nested-ternary
        profile.isLoadingPostUser ? (
          <ScrollView
            scrollEnabled={false}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
          >
            <DetailProfile
              navigation={navigation}
              data={profile.userProfileData}
              isLogin={auth.isLogin}
              isLoading={profile.isLoading}
              isLoadingFollow={profile.isLoadingFollow}
              // eslint-disable-next-line no-confusing-arrow
              handleFollow={() =>
                auth.isLogin
                  ? this.handleFollow()
                  : this.props.navigation.navigate('Login', { title: 'Login' })
              }
              handleChat={() =>
                this.props.navigation.navigate('Chat', {
                  title: profile.userProfileData.username,
                  recipientId: profile.userProfileData.user_id
                })
              }
            />
            <PostShimmer />
          </ScrollView>
        ) : profile.postsUser.length > 0 ? (
          <FlatList
            data={profile.postsUser}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={
              profile.isPaginationPostUser ? this.onEndReached : false
            }
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
            ListFooterComponent={
              profile.isPaginationPostUser
                ? () => (
                    <Loading
                      style={{ height: 75, marginBottom: 5 }}
                      color={color.white}
                    />
                  )
                : null
            }
            renderItem={({ item, index }) => (
              <Fragment>
                {index === 0 ? (
                  <DetailProfile
                    navigation={navigation}
                    data={profile.userProfileData}
                    isLogin={auth.isLogin}
                    isLoading={profile.isLoading}
                    isLoadingFollow={profile.isLoadingFollow}
                    // eslint-disable-next-line no-confusing-arrow
                    handleFollow={() =>
                      auth.isLogin
                        ? this.handleFollow()
                        : this.props.navigation.navigate('Login', {
                            title: 'Login'
                          })
                    }
                    handleChat={() =>
                      this.props.navigation.navigate('Chat', {
                        title: profile.userProfileData.username,
                        recipientId: profile.userProfileData.user_id
                      })
                    }
                  />
                ) : null}
                {item.shared_info === null ? (
                  <Post
                    userData={this.props.profile.userData}
                    item={item}
                    navigation={navigation}
                    profile
                    isLogin={this.props.auth.isLogin}
                    handleLike={() =>
                      this.props.handleLikeProfile(item.post_id)
                    }
                  />
                ) : (
                  <PostShare
                    data={item}
                    userData={this.props.profile.userData}
                    item={item.shared_info}
                    navigation={this.props.navigation}
                    isLogin={this.props.auth.isLogin}
                    isMe={
                      this.props.profile.userData.user_id ===
                      item.publisher.user_id
                    }
                    isMeShare={
                      this.props.profile.userData.user_id ===
                      item.shared_info.publisher.user_id
                    }
                    handleLike={() => this.props.handleLike(item.post_id)}
                    handleShareTimeline={this.props.shareToTimeline}
                  />
                )}
              </Fragment>
            )}
          />
        ) : (
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
          >
            <DetailProfile
              navigation={navigation}
              data={profile.userProfileData}
              isLogin={auth.isLogin}
              isLoading={profile.isLoading}
              isLoadingFollow={profile.isLoadingFollow}
              // eslint-disable-next-line no-confusing-arrow
              handleFollow={() =>
                auth.isLogin
                  ? this.handleFollow()
                  : this.props.navigation.navigate('Login', { title: 'Login' })
              }
              handleChat={() =>
                this.props.navigation.navigate('Chat', {
                  title: profile.userProfileData.username,
                  recipientId: profile.userProfileData.user_id
                })
              }
            />
            <View
              style={{
                backgroundColor: color.backgroundContent,
                height: 300,
                justifyContent: 'center',
                alignItems: 'center',
                marginVertical: 5
              }}
            >
              <Text style={{ color: color.whitelv2 }}>Empty Post</Text>
            </View>
          </ScrollView>
        )}
      </View>
    ) : (
      <View style={styles.container}>
        <DetailShimmer />
        <PostShimmer />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.backgroundContainer,
    flex: 1
  },
  wrapProfile: {
    backgroundColor: color.backgroundContent
  }
});

const mapStateToProps = state => ({
  profile: state.profile,
  auth: state.auth
});

const mapDispatchToProps = {
  storeUserProfile,
  getUserProfile,
  handleFollow,
  getPostsUser,
  updatePostsUser,
  handleLikeProfile,
  searchIdUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
