import React, { Component, Fragment } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  ScrollView,
  RefreshControl,
  Text
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import {
  getPostsUser,
  updatePostsUser,
  getProfile,
  getNewsFeed,
  handleLikeProfile
} from '../action';
import DetailMyProfile from '../components/DetailMyProfile';
import { Post, Loading, PostShare } from '../../public/components';
import PostShimmer from '../components/PostShimmer';

const color = config.color;

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      refreshing: false
    };
  }

  async componentDidMount() {
    this.props.getPostsUser(this.props.profile.userData.user_id);
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    this.props.getProfile(
      this.props.handleLogout,
      this.props.navigation,
      this.props.getNewsFeed
    );
    this.props.getPostsUser(this.props.profile.userData.user_id);
    this.setState({ refreshing: false });
  };

  onEndReached = () => {
    if (!this.props.profile.isLoadingPostUserUpdate) {
      this.props.updatePostsUser(
        this.props.profile.userData.user_id,
        this.props.profile.postsUser[this.props.profile.postsUser.length - 1]
          .post_id
      );
    }
  };

  render() {
    const { profile, navigation } = this.props;

    return (
      <View style={styles.container}>
        {// eslint-disable-next-line no-nested-ternary
        profile.isLoadingPostUser ? (
          <ScrollView
            scrollEnabled={false}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
          >
            <DetailMyProfile
              navigation={navigation}
              data={profile.userData}
              isLoading={profile.isLoading}
            />
            <PostShimmer />
          </ScrollView>
        ) : profile.postsUser.length > 0 ? (
          <FlatList
            data={profile.postsUser}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={
              profile.isPaginationPostUser ? this.onEndReached : false
            }
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
            ListFooterComponent={
              profile.isPaginationPostUser
                ? () => (
                    <Loading
                      style={{ height: 75, marginBottom: 5 }}
                      color={color.white}
                    />
                  )
                : null
            }
            renderItem={({ item, index }) => (
              <Fragment>
                {index === 0 ? (
                  <DetailMyProfile
                    navigation={navigation}
                    data={profile.userData}
                    isLoading={profile.isLoading}
                  />
                ) : null}
                {item.shared_info === null ? (
                  <Post
                    userData={this.props.profile.userData}
                    item={item}
                    navigation={navigation}
                    profile
                    isMe
                    isLogin={this.props.auth.isLogin}
                    handleLike={() =>
                      this.props.handleLikeProfile(item.post_id)
                    }
                  />
                ) : (
                  <PostShare
                    data={item}
                    userData={this.props.profile.userData}
                    item={item.shared_info}
                    navigation={this.props.navigation}
                    isLogin={this.props.auth.isLogin}
                    isMe={
                      this.props.profile.userData.user_id ===
                      item.publisher.user_id
                    }
                    isMeShare={
                      this.props.profile.userData.user_id ===
                      item.shared_info.publisher.user_id
                    }
                    handleLike={() => this.props.handleLike(item.post_id)}
                    handleShareTimeline={this.props.shareToTimeline}
                  />
                )}
              </Fragment>
            )}
          />
        ) : (
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
          >
            <DetailMyProfile
              navigation={navigation}
              data={profile.userData}
              isLoading={profile.isLoading}
            />
            <View
              style={{
                backgroundColor: color.backgroundContent,
                height: 300,
                justifyContent: 'center',
                alignItems: 'center',
                marginVertical: 5
              }}
            >
              <Text style={{ color: color.whitelv2 }}>Empty Post</Text>
            </View>
          </ScrollView>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.backgroundContainer,
    flex: 1
  },
  wrapProfile: {
    backgroundColor: color.backgroundContent
  }
});

const mapStateToProps = state => ({
  profile: state.profile,
  auth: state.auth
});

const mapDispatchToProps = {
  getPostsUser,
  updatePostsUser,
  getProfile,
  getNewsFeed,
  handleLikeProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
