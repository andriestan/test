import { Alert } from 'react-native';

import { getUser, Fetch } from '../public/services';
import config from '../public/config/config.json';
import { getNewsFeed } from '../home/action';

export const storeUserProfile = data => async dispatch => {
  dispatch({ type: 'STORE_USER_PROFILE', payload: data });
};

export const getProfile = (
  handleLogout,
  navigation,
  getNews
) => async dispatch => {
  dispatch({ type: 'GET_PROFILE_PENDING' });
  const user = await getUser();
  const data = {
    user_id: user.user_id,
    fetch: 'user_data'
  };
  const response = await Fetch(
    `/api/get-user-data?access_token=${user.access_token}`,
    data
  );
  if (response.api_status === 200) {
    dispatch({ type: 'GET_PROFILE_SUCCESS', payload: response.user_data });
  } else {
    handleLogout(navigation, getNews);
    dispatch({
      type: 'GET_PROFILE_ERROR',
      payload: response.errors.error_text
    });
  }
};

export const handleLikeProfile = postId => async dispatch => {
  dispatch({ type: 'HANDLE_LIKE_PROFILE_PENDING', payload: postId });
  dispatch({ type: 'HANDLE_LIKE_PENDING', payload: postId });
  const data = {
    post_id: postId,
    action: 'like'
  };
  const user = await getUser();
  await Fetch(`/api/post-actions?access_token=${user.access_token}`, data);
};

export const getUserProfile = userId => async dispatch => {
  dispatch({ type: 'GET_USER_PROFILE_PENDING' });
  const user = await getUser();
  const data = {
    user_id: userId,
    fetch: 'user_data'
  };

  let response;
  if (user) {
    response = await Fetch(
      `/api/get-user-data?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(
      `/api/get-user-data?access_token=${config.access_token}`,
      data
    );
  }

  if (response.api_status === 200) {
    dispatch({ type: 'GET_USER_PROFILE_SUCCESS', payload: response.user_data });
  } else {
    dispatch({
      type: 'GET_USER_PROFILE_ERROR',
      payload: response.errors.error_text
    });
  }
};

export const searchIdUser = username => async dispatch => {
  dispatch({ type: 'SEARCH_ID_USER' });
  const data = {
    search_key: username
  };

  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/search?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(`/api/search?access_token=${config.guest}`, data);
  }
  dispatch(getUserProfile(response.users[0].user_id));
};

export const getPostsUser = userId => async dispatch => {
  dispatch({ type: 'GET_POSTS_USER_PENDING' });
  const data = {
    id: userId,
    fetch: 'user_data',
    type: 'get_user_posts',
    limit: 20
  };
  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/posts?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(`/api/posts?access_token=${config.guest}`, data);
  }

  if (response.api_status === 200) {
    dispatch({ type: 'GET_POSTS_USER_SUCCESS', payload: response.data });
  } else {
    dispatch({
      type: 'GET_POSTS_USER_ERROR',
      payload: response.errors.error_text
    });
  }
};

export const updatePostsUser = (userId, postId) => async dispatch => {
  dispatch({ type: 'UPDATE_POSTS_USER_PENDING' });
  const data = {
    id: userId,
    fetch: 'user_data',
    type: 'get_user_posts',
    limit: 20,
    after_post_id: postId
  };

  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/posts?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(`/api/posts?access_token=${config.guest}`, data);
  }

  if (response.api_status === 200) {
    dispatch({ type: 'UPDATE_POSTS_USER_SUCCESS', payload: response.data });
  } else {
    dispatch({
      type: 'UPDATE_POSTS_USER_ERROR',
      payload: response.errors.error_text
    });
  }
};

export const getFollowing = () => async dispatch => {
  dispatch({ type: 'GET_FOLLOWING_PENDING' });
  const user = await getUser();
  const data = {
    user_id: user.user_id,
    fetch: 'following'
  };
  const response = await Fetch(
    `/api/get-user-data?access_token=${user.access_token}`,
    data
  );
  if (response.api_status === 200) {
    dispatch({ type: 'GET_FOLLOWING_SUCCESS', payload: response.following });
  }
};

export const handleFollow = userId => async dispatch => {
  dispatch({ type: 'HANDLE_FOLLOW_PENDING' });
  const user = await getUser();
  const data = {
    user_id: userId
  };
  const response = await Fetch(
    `/api/follow-user?access_token=${user.access_token}`,
    data
  );
  if (response.api_status === 200) {
    dispatch({
      type: 'HANDLE_FOLLOW_SUCCESS',
      payload: response.follow_status
    });
  } else {
    dispatch({
      type: 'HANDLE_FOLLOW_ERROR',
      payload: response.errors.error_text
    });
  }
};

export const updateUserData = (field, value, navigation) => async dispatch => {
  dispatch({ type: 'UPDATE_USER_DATA_PENDING' });
  const user = await getUser();
  const data = {
    [field]: value
  };
  const response = await Fetch(
    `/api/update-user-data?access_token=${user.access_token}`,
    data
  );
  if (response.api_status === 200) {
    dispatch({
      type: 'UPDATE_USER_DATA_SUCCESS'
    });
    dispatch(getProfile());
    if (field === 'avatar' || field === 'cover') {
      dispatch(getPostsUser(user.user_id));
      dispatch(getNewsFeed());
    }
    Alert.alert('Your profile was updated');
    await navigation.goBack();
    navigation.navigate('ProfileSetting', {
      title: 'My Profile Setting'
    });
  } else {
    dispatch({
      type: 'UPDATE_USER_DATA_ERROR'
    });
    Alert.alert(response.errors.error_text);
  }
};

export const updateNameUserData = (data, navigation) => async dispatch => {
  dispatch({ type: 'UPDATE_USER_DATA_PENDING' });
  const user = await getUser();
  const response = await Fetch(
    `/api/update-user-data?access_token=${user.access_token}`,
    data
  );
  if (response.api_status === 200) {
    dispatch({
      type: 'UPDATE_USER_DATA_SUCCESS'
    });
    dispatch(getProfile());
    Alert.alert('Your profile was updated');
    await navigation.goBack();
    navigation.navigate('ProfileSetting', {
      title: 'My Profile Setting'
    });
  } else {
    dispatch({
      type: 'UPDATE_USER_DATA_ERROR'
    });
    Alert.alert(response.errors.error_text);
  }
};

export const updatePasswordUserData = data => async dispatch => {
  dispatch({ type: 'UPDATE_USER_DATA_PENDING' });
  const user = await getUser();
  const response = await Fetch(
    `/api/update-user-data?access_token=${user.access_token}`,
    data
  );
  if (response.api_status === 200) {
    dispatch({
      type: 'UPDATE_USER_DATA_SUCCESS'
    });
    Alert.alert('Your password was updated');
    return true;
  }
  dispatch({
    type: 'UPDATE_USER_DATA_ERROR'
  });
  Alert.alert(response.errors.error_text);
  return false;
};
