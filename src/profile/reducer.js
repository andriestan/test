const initialState = {
  userData: {},
  userProfileData: {},
  isLoading: true,
  isLoadingFollow: false,
  isLoadingPostUser: true,
  isLoadingPostUserUpdate: false,
  postsUser: [],
  isPaginationPostUser: true,
  isLoadingFollowing: false,
  following: [],
  isLoadingUpdateUserData: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_PROFILE_PENDING':
      return { ...state, isLoading: true };
    case 'GET_PROFILE_SUCCESS':
      return { ...state, isLoading: false, userData: action.payload };
    case 'GET_PROFILE_ERROR':
      return {
        ...state,
        isLoading: false
      };
    case 'SEARCH_ID_USER':
      return { ...state, isLoading: true };
    case 'STORE_USER_PROFILE':
      return { ...state, userProfileData: action.payload };
    case 'GET_USER_PROFILE_PENDING':
      return { ...state, isLoading: true };
    case 'GET_USER_PROFILE_SUCCESS':
      return { ...state, isLoading: false, userProfileData: action.payload };
    case 'GET_USER_PROFILE_ERROR':
      return {
        ...state,
        isLoading: false
      };
    case 'GET_POSTS_USER_PENDING':
      return { ...state, isLoadingPostUser: true, postsUser: [] };
    case 'GET_POSTS_USER_SUCCESS':
      return {
        ...state,
        isLoadingPostUser: false,
        postsUser: action.payload,
        isPaginationPostUser: !(action.payload.length < 20)
      };
    case 'GET_POSTS_USER_ERROR':
      return {
        ...state,
        isLoadingPostUser: false
      };
    case 'UPDATE_POSTS_USER_PENDING':
      return { ...state, isLoadingPostUserUpdate: true };
    case 'UPDATE_POSTS_USER_SUCCESS':
      return {
        ...state,
        isLoadingPostUserUpdate: false,
        postsUser: state.postsUser.concat(action.payload),
        isPaginationPostUser: !(action.payload.length < 20)
      };
    case 'UPDATE_POSTS_USER_ERROR':
      return {
        ...state,
        isLoadingPostUserUpdate: false
      };
    case 'HANDLE_FOLLOW_PENDING':
      return { ...state, isLoadingFollow: true };
    // eslint-disable-next-line no-case-declarations
    case 'HANDLE_FOLLOW_SUCCESS':
      const userProfileData = state.userProfileData;
      userProfileData.is_following = action.payload === 'followed' ? 1 : 0;
      return { ...state, isLoadingFollow: false, userProfileData };
    case 'HANDLE_FOLLOW_ERROR':
      return { ...state, isLoadingFollow: false };
    case 'GET_FOLLOWING_PENDING':
      return { ...state, isLoadingFollowing: true };
    case 'GET_FOLLOWING_SUCCESS':
      return { ...state, isLoadingFollowing: false, following: action.payload };
    // eslint-disable-next-line no-case-declarations
    case 'HANDLE_LIKE_PROFILE_PENDING':
      const index = state.postsUser.findIndex(
        field => field.post_id === action.payload
      );
      const postsUser = state.postsUser;
      postsUser[index].is_liked = !postsUser[index].is_liked;
      if (postsUser[index].is_liked) {
        postsUser[index].post_likes = Number(postsUser[index].post_likes) + 1;
      } else {
        postsUser[index].post_likes = Number(postsUser[index].post_likes) - 1;
      }
      return { ...state, postsUser };
    case 'UPDATE_USER_DATA_PENDING':
      return { ...state, isLoadingUpdateUserData: true };
    case 'UPDATE_USER_DATA_SUCCESS':
      return { ...state, isLoadingUpdateUserData: false };
    case 'UPDATE_USER_DATA_ERROR':
      return { ...state, isLoadingUpdateUserData: false };
    case 'DELETE_POST_SUCCESS':
      return {
        ...state,
        postsUser: state.postsUser.filter(
          data => data.post_id !== action.payload
        )
      };
    default:
      return state;
  }
};
