import { combineReducers } from 'redux';

import home from '../../home/reducer';
import auth from '../../auth/reducer';
import profile from '../../profile/reducer';
import search from '../../search/reducer';
import comment from '../../comment/reducer';
import post from '../../post/reducer';
import messanger from '../../messenger/reducer';
import notification from '../../notification/reducer';
import browser from '../../browser/reducer';
import suggested from '../../suggested/reducer';

export default combineReducers({
  auth,
  home,
  profile,
  search,
  comment,
  post,
  messanger,
  notification,
  browser,
  suggested
});
