import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableHighlight
} from 'react-native';
import { connect } from 'react-redux';

import config from '../config/config.json';

const color = config.color;

const Header = ({ navigation, auth, notification }) => (
  <View style={styles.container}>
    <TouchableHighlight
      style={{
        width: '15%',
        alignItems: 'center',
        height: '100%',
        justifyContent: 'center'
      }}
      onPress={() => navigation.openDrawer()}
    >
      <Image
        source={require('../assets/icon/menu.png')}
        style={{ width: '40%', height: 20 }}
      />
    </TouchableHighlight>
    <TouchableHighlight
      style={{ flex: 2, justifyContent: 'flex-end', height: '100%' }}
      // eslint-disable-next-line no-confusing-arrow
      onPress={() => navigation.navigate('Search')}
      underlayColor={color.backgroundlv2}
    >
      <View style={styles.wrapInput}>
        <View style={{ width: 20, height: 15 }}>
          <Image
            source={require('../assets/icon/search.png')}
            style={{ width: '80%', height: '100%' }}
          />
        </View>
        <View>
          <Text style={{ color: '#9496a3' }}>Search and discover</Text>
        </View>
      </View>
    </TouchableHighlight>
    <View style={{ flex: 1.5, alignItems: 'flex-end' }}>
      <View style={styles.groupIcon}>
        <View style={styles.icon}>
          <TouchableHighlight
            style={[styles.icon, { width: '100%' }]}
            // eslint-disable-next-line no-confusing-arrow
            onPress={() =>
              auth.isLogin
                ? navigation.navigate('Notification', {
                    title: 'Notifications'
                  })
                : navigation.navigate('Login', { title: 'Login' })
            }
            underlayColor={color.backgroundlv2}
          >
            {notification.count > 0 ? (
              <Image
                source={require('../assets/icon/ic-notif-active.png')}
                style={{ width: 20, height: 20 }}
              />
            ) : (
              <Image
                source={require('../assets/icon/notification.png')}
                style={{ width: 20, height: 20 }}
              />
            )}
          </TouchableHighlight>
          {notification.count > 0 ? (
            <View
              style={{
                backgroundColor: color.red,
                width: 10,
                height: 10,
                borderRadius: 10 / 2,
                position: 'absolute',
                top: 12,
                right: 9
              }}
            />
          ) : null}
        </View>
        <TouchableHighlight
          style={styles.icon}
          underlayColor={color.backgroundlv2}
          // eslint-disable-next-line no-confusing-arrow
          onPress={() =>
            auth.isLogin
              ? navigation.navigate('Messenger', { title: 'Chat' })
              : navigation.navigate('Login', { title: 'Login' })
          }
        >
          <Image
            source={require('../assets/icon/messenger-white.png')}
            style={{ width: 20, height: 20 }}
          />
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.icon}
          underlayColor={color.backgroundlv2}
          // eslint-disable-next-line no-confusing-arrow
          onPress={() =>
            auth.isLogin
              ? navigation.navigate('Suggested', { title: 'Friend List' })
              : navigation.navigate('Login', { title: 'Login' })
          }
        >
          <Image
            source={require('../assets/icon/ic-friends-idle.png')}
            style={{ width: 23, height: 18 }}
          />
        </TouchableHighlight>
      </View>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.background,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  wrapInput: {
    flexDirection: 'row',
    borderBottomColor: '#9496a3',
    borderBottomWidth: 1,
    paddingHorizontal: 10,
    marginBottom: 11,
    alignItems: 'center',
    paddingBottom: 7
  },
  groupIcon: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    height: '100%'
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    height: '100%',
    justifyContent: 'center'
  }
});

const mapStateToProps = state => ({
  auth: state.auth,
  notification: state.notification
});

export default connect(mapStateToProps)(Header);
