import React, { Component } from 'react';
import { Image, Dimensions, View, Text } from 'react-native';

import config from '../config/config.json';

const color = config.color;
const { width } = Dimensions.get('window');

class ImageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 0,
      height: 0
    };
    this.getSizeImage(this.props.uri);
  }

  getSizeImage(uri) {
    Image.getSize(uri, (w, h) => {
      this.setState({
        width: w,
        height: h
      });
    });
  }

  render() {
    return this.state.width > 0 ? (
      <View
        style={[
          {
            width,
            height: this.state.height / (this.state.width / width)
          },
          this.props.style
        ]}
      >
        <Image source={{ uri: this.props.uri }} style={{ width: '100%', height: '100%' }} />
      </View>
    ) : (
      <View
        style={[{ justifyContent: 'center', alignItems: 'center', height: 200 }, this.props.style]}
      >
        <Text style={{ color: color.whitelv2 }}>Loading</Text>
      </View>
    );
  }
}

export default ImageComponent;
