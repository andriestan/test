import React, { Fragment } from 'react';
import {
  SafeAreaView,
  TouchableHighlight,
  Text,
  View,
  Image,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';

import { handleLogout } from '../../auth/action';
import { getNewsFeed } from '../../home/action';
import config from '../config/config.json';

const color = config.color;

// eslint-disable-next-line no-shadow
const Drawer = ({ handleLogout, navigation, getNewsFeed, auth, profile }) => (
  <SafeAreaView style={{ flex: 1 }}>
    <View style={{ backgroundColor: color.backgroundContent, flex: 1 }}>
      <View style={{ width: '100%', height: 200 }}>
        <Image
          source={
            auth.isLogin
              ? { uri: profile.userData.avatar }
              : require('../assets/icon/ic-whatsup.png')
          }
          style={{ width: '100%', height: '100%', opacity: 0.3 }}
          blurRadius={1}
        />
        <View style={{ width: '50%', position: 'absolute', bottom: 20 }}>
          <TouchableHighlight
            // eslint-disable-next-line no-confusing-arrow
            onPress={() =>
              auth.isLogin
                ? navigation.navigate('MyProfile', { title: 'My Profile' })
                : navigation.navigate('Login', { title: 'Login' })
            }
            underlayColor={color.buttonEffect}
            style={{ alignItems: 'center', width: '100%' }}
          >
            <Fragment>
              <Image
                source={
                  auth.isLogin
                    ? { uri: profile.userData.avatar }
                    : require('../assets/icon/ic-whatsup.png')
                }
                style={{
                  width: 80,
                  height: 80,
                  borderRadius: 80 / 2,
                  marginBottom: 5
                }}
              />
              <Text style={{ color: color.white, fontWeight: 'bold' }}>
                {auth.isLogin ? profile.userData.name : 'Player'}
              </Text>
            </Fragment>
          </TouchableHighlight>
        </View>
      </View>
      <TouchableHighlight
        // eslint-disable-next-line no-confusing-arrow
        onPress={() => navigation.navigate('Blog', { title: 'Blog' })}
        underlayColor={color.buttonEffect}
        style={{ paddingHorizontal: 20, paddingVertical: 10, marginTop: 10 }}
      >
        <Text style={{ fontSize: 16, color: color.white }}>Blog</Text>
      </TouchableHighlight>
      <TouchableHighlight
        // eslint-disable-next-line no-confusing-arrow
        onPress={() => navigation.navigate('Faq', { title: 'FAQ' })}
        underlayColor={color.buttonEffect}
        style={{ paddingHorizontal: 20, paddingVertical: 10 }}
      >
        <Text style={{ fontSize: 16, color: color.white }}>FAQ</Text>
      </TouchableHighlight>
      <TouchableHighlight
        // eslint-disable-next-line no-confusing-arrow
        onPress={() => navigation.navigate('Feedback', { title: 'Feedback' })}
        underlayColor={color.buttonEffect}
        style={{ paddingHorizontal: 20, paddingVertical: 10 }}
      >
        <Text style={{ fontSize: 16, color: color.white }}>Feedback</Text>
      </TouchableHighlight>
      <TouchableHighlight
        // eslint-disable-next-line no-confusing-arrow
        onPress={() =>
          navigation.navigate('CommunityGuidelines', {
            title: 'Community Guidelines'
          })
        }
        underlayColor={color.buttonEffect}
        style={{ paddingHorizontal: 20, paddingVertical: 10 }}
      >
        <Text style={{ fontSize: 16, color: color.white }}>
          Community Guidelines
        </Text>
      </TouchableHighlight>
      <TouchableHighlight
        // eslint-disable-next-line no-confusing-arrow
        onPress={() =>
          navigation.navigate('TermOfService', { title: 'Term of Service' })
        }
        underlayColor={color.buttonEffect}
        style={{ paddingHorizontal: 20, paddingVertical: 10 }}
      >
        <Text style={{ fontSize: 16, color: color.white }}>
          Terms of Service
        </Text>
      </TouchableHighlight>
      <TouchableHighlight
        // eslint-disable-next-line no-confusing-arrow
        onPress={() =>
          navigation.navigate('PrivacyPolicy', { title: 'Privacy Policy' })
        }
        underlayColor={color.buttonEffect}
        style={{ paddingHorizontal: 20, paddingVertical: 10 }}
      >
        <Text style={{ fontSize: 16, color: color.white }}>Privacy Policy</Text>
      </TouchableHighlight>
      <TouchableHighlight
        // eslint-disable-next-line no-confusing-arrow
        onPress={() =>
          auth.isLogin
            ? handleLogout(navigation, getNewsFeed)
            : navigation.navigate('Login', { title: 'Login' })
        }
        underlayColor={color.buttonEffect}
        style={[
          { paddingHorizontal: 20, paddingVertical: 10 },
          auth.isLoading ? { alignItems: 'flex-start' } : null
        ]}
        disabled={auth.isLoading}
      >
        {!auth.isLoading ? (
          <Text style={{ fontSize: 16, color: color.blue }}>
            {auth.isLogin ? 'Logout' : 'Login'}
          </Text>
        ) : (
          <ActivityIndicator color={color.white} style={{ marginLeft: 20 }} />
        )}
      </TouchableHighlight>
    </View>
  </SafeAreaView>
);

const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile
});

const mapDispatchToProps = {
  handleLogout,
  getNewsFeed
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Drawer);
