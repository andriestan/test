import React from 'react';
import { View, ActivityIndicator, Modal } from 'react-native';

const Spinner = ({ style, color, visible }) => (
  <Modal visible={visible} animationType="fade" transparent>
    <View style={[style, { justifyContent: 'center', alignItems: 'center', flex: 1 }]}>
      <ActivityIndicator color={color} size="large" />
    </View>
  </Modal>
);

export default Spinner;
