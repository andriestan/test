import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Loading = ({ style, color }) => (
  <View style={[style, { justifyContent: 'center', alignItems: 'center' }]}>
    <ActivityIndicator color={color} size="large" />
  </View>
);

export default Loading;
