import React from 'react';
import { View, StyleSheet, Image, TouchableHighlight, Text } from 'react-native';

import config from '../../public/config/config.json';

const color = config.color;

const Header = ({ navigation }) => (
  <View style={styles.container}>
    <TouchableHighlight
      style={{ width: '15%', alignItems: 'center', height: '100%', justifyContent: 'center' }}
      underlayColor={color.backgroundlv2}
      onPress={() => navigation.goBack()}
    >
      <Image
        source={require('../assets/icon/ic-back.png')}
        style={{ width: '35%', height: '35%' }}
      />
    </TouchableHighlight>
    <View style={{ width: '80%', justifyContent: 'center', height: '100%' }}>
      <Text style={{ color: '#fff', fontSize: 20 }}>{navigation.getParam('title')}</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.background,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center'
  }
});

export default Header;
