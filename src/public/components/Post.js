import React, { Component } from 'react';
import {
  TouchableHighlight,
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Modal,
  TouchableWithoutFeedback,
  ActivityIndicator
} from 'react-native';
import Hyperlink from 'react-native-hyperlink';
import ImageViewer from 'react-native-image-zoom-viewer';
import { connect } from 'react-redux';

import { MultiImages, Image as ImageComponent, Video } from './index';
import { deletePost } from '../../home/action';
import config from '../config/config.json';
// eslint-disable-next-line import/no-extraneous-dependencies
const linkify = require('linkify-it')();

const color = config.color;

linkify.add('@', {
  validate(text, pos, self) {
    const tail = text.slice(pos);
    if (!self.re.twitter) {
      // eslint-disable-next-line no-param-reassign
      self.re.twitter = new RegExp(
        `^([a-zA-Z0-9_]){1,15}(?!_)(?=$|${self.re.src_ZPCc})`
      );
    }
    if (self.re.twitter.test(tail)) {
      // Linkifier allows punctuation chars before prefix,
      // but we additionally disable `@` ("@@mention" is invalid)
      if (pos >= 2 && tail[pos - 2] === '@') {
        return false;
      }
      return tail.match(self.re.twitter)[0].length;
    }
    return 0;
  },
  normalize(match) {
    // eslint-disable-next-line no-param-reassign
    match.url = match.url.replace(/^@/, '');
  }
});

linkify.add('#', {
  validate(text, pos, self) {
    const tail = text.slice(pos);
    if (!self.re.twitter) {
      // eslint-disable-next-line no-param-reassign
      self.re.twitter = new RegExp(
        `^([a-zA-Z0-9_]){1,15}(?!_)(?=$|${self.re.src_ZPCc})`
      );
    }
    if (self.re.twitter.test(tail)) {
      // Linkifier allows punctuation chars before prefix,
      // but we additionally disable `@` ("@@mention" is invalid)
      if (pos >= 2 && tail[pos - 2] === '#') {
        return false;
      }
      return tail.match(self.re.twitter)[0].length;
    }
    return 0;
  },
  normalize(match) {
    // eslint-disable-next-line no-param-reassign
    match.url = match.url.replace(/^#/, '');
  }
});

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      isLoading: false,
      visibleVideo: false,
      visibleDelete: false,
      visibleImage: false,
      image: []
    };
  }

  async shareTimeline() {
    await this.setState({ isLoading: true });
    await this.props.handleShareTimeline(this.props.item.id);
    this.setState({ visible: false, isLoading: false });
  }

  async handleDelete() {
    await this.props.deletePost(this.props.item.id);
    this.setState({ visibleDelete: false });
  }

  async handleVideo(item) {
    await this.setState({ visibleVideo: true });
    this.props.navigation.navigate('Comment', {
      title: 'Comments',
      postId: item.post_id,
      data: item
    });
  }

  render() {
    const {
      item,
      navigation,
      profile,
      isLogin,
      isMe,
      handleLike,
      userData
    } = this.props;
    return (
      <View style={styles.content}>
        <View style={{ paddingHorizontal: 20, paddingTop: 10 }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}
          >
            <TouchableOpacity
              style={{ flexDirection: 'row', flex: 1 }}
              // eslint-disable-next-line no-confusing-arrow
              onPress={() =>
                // eslint-disable-next-line no-nested-ternary
                isLogin
                  ? isMe
                    ? navigation.navigate('MyProfile', {
                        title: 'My Profile'
                      })
                    : navigation.navigate('Profile', {
                        title: item.publisher.username,
                        userData: item.publisher
                      })
                  : navigation.navigate('Profile', {
                      title: item.publisher.username,
                      userData: item.publisher
                    })
              }
              disabled={profile}
            >
              <View style={styles.wrapImage}>
                <Text style={{ fontSize: 12, color: color.whitelv2 }}>
                  Image
                </Text>
                <Image
                  style={styles.image}
                  source={{ uri: item.publisher.avatar }}
                />
              </View>
              <View style={{ justifyContent: 'center', marginLeft: 10 }}>
                <View
                  style={{
                    flexDirection:
                      item.publisher.name.length < 18 && !isMe
                        ? 'row'
                        : 'column'
                  }}
                >
                  <Text style={{ fontWeight: 'bold', color: color.white }}>
                    {item.publisher.name}
                  </Text>
                  {item.event_id !== '0' ? (
                    <TouchableWithoutFeedback>
                      <View style={{ flexDirection: 'row' }}>
                        <Text
                          style={{
                            color: color.white,
                            fontWeight: 'bold',
                            marginHorizontal: 10
                          }}
                        >
                          >
                        </Text>
                        <Text
                          style={{ color: color.white, fontWeight: 'bold' }}
                        >
                          {item.event.name}
                        </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  ) : null}
                  {item.postFile_type === 'post_cover' ||
                  item.postFile_type === 'post_avatar' ? (
                    <Text style={{ color: color.whitelv2 }}>
                      {item.postFile_type === 'post_cover'
                        ? `${
                            item.publisher.name.length < 18 && !isMe ? ' ' : ''
                          }Changed his profile cover`
                        : `${
                            item.publisher.name.length < 18 && !isMe ? ' ' : ''
                          }Changed his profile picture`}
                    </Text>
                  ) : null}
                </View>
                <Text style={{ fontSize: 12, color: color.whitelv2 }}>
                  {item.post_time}
                </Text>
              </View>
            </TouchableOpacity>
            {isLogin && isMe ? (
              <TouchableOpacity
                style={{ marginTop: 10 }}
                onPress={() => this.setState({ visibleDelete: true })}
              >
                <Image
                  source={require('../assets/icon/ic-trash-idle.png')}
                  style={{ width: 16, height: 18 }}
                />
              </TouchableOpacity>
            ) : null}
          </View>
          {item.postText !== '' ? (
            <View style={{ marginBottom: 5, marginTop: 15 }}>
              <Hyperlink
                linkify={linkify}
                // eslint-disable-next-line no-confusing-arrow
                onPress={(url, text) =>
                  // eslint-disable-next-line no-nested-ternary
                  url.slice(0, 4) === 'http'
                    ? navigation.navigate('Browser', { title: 'Oxa', url })
                    : // eslint-disable-next-line no-nested-ternary
                    text[0] === '@'
                    ? userData.username === url
                      ? navigation.navigate('My Profile', { title: url })
                      : navigation.navigate('Profile', {
                          title: url,
                          mention: true,
                          username: url
                        })
                    : navigation.navigate('HashtagPage', { hashtag: text })
                }
                linkStyle={{
                  textDecorationLine: 'underline',
                  color: color.blue
                }}
              >
                <Text style={{ color: color.white }}>{item.Orginaltext}</Text>
              </Hyperlink>
            </View>
          ) : null}
          {// eslint-disable-next-line no-nested-ternary
          item.postFile_full !== '' ? (
            // eslint-disable-next-line no-nested-ternary
            item.postFile_type === 'post_image' ||
            item.postFile_type === 'post_cover' ||
            item.postFile_type === 'post_avatar' ? (
              <TouchableHighlight
                onPress={() =>
                  this.setState({
                    visibleImage: true,
                    image: [{ url: item.postFile_full }]
                  })
                }
                underlayColor="rgba(0,0,0,0)"
              >
                <ImageComponent
                  uri={item.postFile_full}
                  style={{
                    marginVertical: 10,
                    marginHorizontal: -20,
                    borderTopColor: color.backgroundContainer,
                    borderTopWidth: 1,
                    borderBottomColor: color.backgroundContainer,
                    borderBottomWidth: 1
                  }}
                />
              </TouchableHighlight>
            ) : item.postFile_type === 'post_video' ? (
              <TouchableHighlight
                onPress={() => this.handleVideo(item)}
                underlayColor="rgba(0,0,0,0)"
              >
                <Video
                  styles={{ marginHorizontal: -20 }}
                  uri={item.postFile_full}
                  muted={
                    this.props.home.viewable.filter(data => data.item === item)
                      .length > 0
                      ? !this.props.home.viewable.filter(
                          data => data.item === item
                        )[0].isViewable
                      : true
                  }
                  repeat
                  paused={
                    // eslint-disable-next-line no-nested-ternary
                    this.state.visibleVideo
                      ? true
                      : this.props.home.viewable.filter(
                          data => data.item === item
                        ).length > 0
                      ? !this.props.home.viewable.filter(
                          data => data.item === item
                        )[0].isViewable
                      : true
                  }
                />
              </TouchableHighlight>
            ) : null
          ) : null}
          {item.photo_multi ? (
            <MultiImages
              data={item.photo_multi}
              style={{ marginVertical: 10, marginHorizontal: -20 }}
            />
          ) : null}
          {item.postSticker ? (
            <ImageComponent
              uri={item.postSticker}
              style={{
                marginVertical: 10,
                marginHorizontal: -20,
                borderTopColor: color.backgroundContainer,
                borderTopWidth: 1,
                borderBottomColor: color.backgroundContainer,
                borderBottomWidth: 1
              }}
            />
          ) : null}
          <TouchableHighlight
            underlayColor={color.buttonEffect}
            onPress={() =>
              navigation.navigate('Comment', {
                title: 'Comments',
                postId: item.post_id,
                data: item
              })
            }
          >
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: 10
              }}
            >
              <Text
                style={{ fontWeight: 'bold', fontSize: 12, color: color.white }}
              >
                {item.post_likes} Likes
              </Text>
              <Text
                style={{ fontWeight: 'bold', fontSize: 12, color: color.white }}
              >
                {item.post_comments} Comments
              </Text>
            </View>
          </TouchableHighlight>
          <View style={styles.wrapLikeCommentShare}>
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                height: 20
              }}
              onPress={
                isLogin
                  ? handleLike
                  : () => navigation.navigate('Login', { title: 'Login' })
              }
            >
              {// eslint-disable-next-line no-nested-ternary
              isLogin ? (
                item.is_liked ? (
                  <Image
                    source={require('../assets/icon/ic-like-active.png')}
                    style={{ width: 18, height: 16 }}
                  />
                ) : (
                  <Image
                    source={require('../assets/icon/ic-like-idle.png')}
                    style={{ width: 18, height: 16 }}
                  />
                )
              ) : (
                <Image
                  source={require('../assets/icon/ic-like-idle.png')}
                  style={{ width: 18, height: 16 }}
                />
              )}
            </TouchableOpacity>
            <TouchableHighlight
              underlayColor={color.buttonEffect}
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                height: 20
              }}
              onPress={() =>
                navigation.navigate('Comment', {
                  title: 'Comments',
                  postId: item.post_id,
                  data: item,
                  autoFocus: true
                })
              }
            >
              <Image
                source={require('../assets/icon/ic-comment-idle.png')}
                style={{ width: 18, height: 16 }}
              />
            </TouchableHighlight>
            <TouchableOpacity
              underlayColor={color.buttonEffect}
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                height: 20
              }}
              // eslint-disable-next-line no-confusing-arrow
              onPress={() =>
                isLogin
                  ? this.setState({ visible: true })
                  : navigation.navigate('Login', { title: 'Login' })
              }
            >
              <Image
                source={require('../assets/icon/ic-share-idle.png')}
                style={{ width: 14, height: 18 }}
              />
            </TouchableOpacity>
          </View>
        </View>

        <Modal
          transparent
          visible={this.state.visible}
          animationType="slide"
          onRequestClose={() => this.setState({ visible: false })}
        >
          <TouchableWithoutFeedback
            onPress={() => this.setState({ visible: false })}
          >
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-end',
                alignItems: 'center'
              }}
            >
              <TouchableWithoutFeedback>
                <View
                  style={{
                    backgroundColor: color.white,
                    width: '90%',
                    borderRadius: 20,
                    marginBottom: 60,
                    padding: 10
                  }}
                >
                  <TouchableHighlight
                    style={{
                      alignItems: 'center',
                      paddingVertical: 10
                    }}
                    underlayColor={color.buttonEffect}
                    onPress={() => this.shareTimeline()}
                  >
                    {!this.state.isLoading ? (
                      <Text>Timeline</Text>
                    ) : (
                      <ActivityIndicator color={color.backgroundContent} />
                    )}
                  </TouchableHighlight>
                  {/* <TouchableHighlight
                    style={{ alignItems: 'center', paddingVertical: 10 }}
                    underlayColor={color.buttonEffect}
                  >
                    <Text>Other</Text>
                  </TouchableHighlight> */}
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>

        <Modal
          transparent
          animationType="slide"
          visible={this.state.visibleDelete}
          onRequestClose={() => this.setState({ visibleDelete: false })}
        >
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              backgroundColor: 'rgba(0,0,0,0.2)'
            }}
          >
            <View
              style={{
                borderRadius: 10,
                backgroundColor: color.white
              }}
            >
              <TouchableHighlight
                style={{
                  paddingVertical: 20,
                  alignItems: 'center',
                  paddingHorizontal: 20
                }}
                underlayColor={color.whitelv2}
                onPress={() => this.handleDelete()}
              >
                {!this.props.home.isLoadingDelete ? (
                  <Text style={{ fontSize: 20 }}>Delete</Text>
                ) : (
                  <ActivityIndicator color={color.backgroundContent} />
                )}
              </TouchableHighlight>
            </View>
            <TouchableHighlight
              onPress={() => this.setState({ visibleDelete: false })}
              underlayColor={color.whitelv2}
              style={{
                padding: 20,
                borderRadius: 10,
                backgroundColor: color.white,
                marginVertical: 20,
                alignItems: 'center'
              }}
            >
              <Text style={{ fontSize: 20 }}>Cancel</Text>
            </TouchableHighlight>
          </View>
        </Modal>

        <Modal
          transparent
          animationType="fade"
          visible={this.state.visibleImage}
          onRequestClose={() => this.setState({ visibleImage: false })}
          transparent
        >
          <ImageViewer
            imageUrls={this.state.image}
            onSwipeDown={() => this.setState({ visibleImage: false })}
            enableSwipeDown
            swipeDownThreshold={1}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: color.backgroundContent,
    marginVertical: 5
  },
  wrapImage: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: color.backgroundContainer,
    borderWidth: 1
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2
  },
  wrapLikeCommentShare: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderTopColor: color.backgroundContainer,
    borderTopWidth: 1,
    paddingVertical: 10,
    marginTop: 5
  }
});

const mapStateToProps = state => ({
  home: state.home
});

const mapDispatchToProps = {
  deletePost
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Post);
