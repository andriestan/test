import React, { Component } from 'react';
import { ScrollView, Dimensions, View, Text } from 'react-native';

import ImageComponent from './Image';
import config from '../config/config.json';

const { width } = Dimensions.get('window');
const color = config.color;

class MultiImages extends Component {
  constructor() {
    super();
    this.state = {
      active: 1
    };
  }

  render() {
    return (
      <View
        style={[
          {
            borderTopColor: color.backgroundContainer,
            borderTopWidth: 1,
            borderBottomColor: color.backgroundContainer,
            borderBottomWidth: 1
          },
          this.props.style
        ]}
      >
        <ScrollView
          horizontal
          style={{ width }}
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={10}
          onMomentumScrollEnd={event =>
            this.setState({
              // eslint-disable-next-line no-mixed-operators
              active: Math.ceil(event.nativeEvent.contentOffset.x / width + 1)
            })
          }
        >
          {this.props.data.map((data, key) => (
            <View
              style={{
                width,
                justifyContent: 'center',
                backgroundColor: '#fff'
              }}
              key={key}
            >
              <ImageComponent uri={data.image} />
            </View>
          ))}
        </ScrollView>
        <View
          style={{
            paddingVertical: 5,
            paddingHorizontal: 10,
            backgroundColor: 'rgba(0,0,0,0.3)',
            borderRadius: 20,
            position: 'absolute',
            top: 10,
            right: 10
          }}
        >
          <Text style={{ color: '#fff', fontSize: 12 }}>{`${
            this.state.active
          }/${this.props.data.length}`}</Text>
        </View>
      </View>
    );
  }
}

export default MultiImages;
