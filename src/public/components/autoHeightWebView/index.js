import React, { PureComponent } from 'react';

import { StyleSheet, Dimensions } from 'react-native';

import { WebView } from 'react-native-webview';

import {
  isEqual,
  setState,
  getWidth,
  isSizeChanged,
  handleSizeUpdated,
  getStateFromProps,
  getBaseScript
} from './utils';

import momoize from './momoize';

export default class AutoHeightWebView extends PureComponent {
  // static propTypes = {
  //   onSizeUpdated: PropTypes.func,
  //   // 'web/' by default on iOS
  //   // 'file:///android_asset/web/' by default on Android
  //   baseUrl: PropTypes.string,
  //   // add baseUrl/files... to android/app/src/assets/ on android
  //   // add baseUrl/files... to project root on iOS
  //   files: PropTypes.arrayOf(
  //     PropTypes.shape({
  //       href: PropTypes.string,
  //       type: PropTypes.string,
  //       rel: PropTypes.string
  //     })
  //   ),
  //   style: ViewPropTypes.style,
  //   customScript: PropTypes.string,
  //   customStyle: PropTypes.string,
  //   // webview props
  //   originWhitelist: PropTypes.arrayOf(PropTypes.string),
  //   onMessage: PropTypes.func
  // };

  // static defaultProps = {
  //   baseUrl: Platform.OS === 'ios' ? 'web/' : 'file:///android_asset/web/'
  // };

  static getDerivedStateFromProps(props, state) {
    return getStateFromProps(props, state);
  }

  constructor(props) {
    super(props);
    const { style } = props;
    this.webView = React.createRef();
    const width = getWidth(style);
    const height =
      style && style.height ? style.height : Dimensions.get('window').height;
    this.size = {
      oldWidth: width,
      oldHeight: height
    };
    this.state = {
      width,
      height
    };
  }

  onMessage = event => {
    if (!event.nativeEvent) {
      return;
    }
    let data = {};
    // Sometimes the message is invalid JSON, so we ignore that case
    try {
      data = JSON.parse(event.nativeEvent.data);
    } catch (error) {
      console.error(error);
      return;
    }
    const { height, width } = data;
    const { oldHeight, oldWidth } = this.size;
    if (isSizeChanged(height, oldHeight, width, oldWidth)) {
      this.size = {
        oldHeight: height,
        oldWidth: width
      };
      this.setState(
        {
          height,
          width
        },
        () => handleSizeUpdated(height, width, this.props.onSizeUpdated)
      );
    }
    const { onMessage } = this.props;
    onMessage && onMessage(event);
  };

  getUpdatedState = momoize(setState, isEqual);

  stopLoading() {
    this.webView.current.stopLoading();
  }

  render() {
    const { height, width } = this.state;
    const { style, originWhitelist } = this.props;
    const { source, script } = this.getUpdatedState(this.props, getBaseScript);
    const heightPage = this.props.height
      ? this.props.height
      : Dimensions.get('window').height - 125;
    return (
      <WebView
        {...this.props}
        originWhitelist={originWhitelist || ['*']}
        ref={this.webView}
        onMessage={this.onMessage}
        style={[
          styles.webView,
          {
            width,
            height: height < heightPage ? heightPage : height
          },
          style
        ]}
        injectedJavaScript={script}
        source={source}
      />
    );
  }
}

const styles = StyleSheet.create({
  webView: {
    backgroundColor: 'transparent'
  }
});
