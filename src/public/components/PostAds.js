import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';

import config from '../config/config.json';
import { WebViewNormal } from './index';

const color = config.color;

class Post extends Component {
  render() {
    const { item } = this.props;
    return (
      <View style={styles.content}>
        <View style={{ paddingHorizontal: 20, paddingVertical: 10 }}>
          <View style={{ flexDirection: 'row', flex: 1 }}>
            <View style={styles.wrapImage}>
              <Text style={{ fontSize: 12, color: color.whitelv2 }}>Image</Text>
              <Image style={styles.image} source={{ uri: item.pic }} />
            </View>
            <View style={{ justifyContent: 'center', marginLeft: 10 }}>
              <View>
                <Text style={{ fontWeight: 'bold', color: color.white }}>
                  {item.name}
                </Text>
              </View>
              <Text style={{ fontSize: 12, color: color.whitelv2 }}>
                {item.timestamp}
              </Text>
            </View>
          </View>
          <View style={{ marginTop: 15, marginHorizontal: -20 }}>
            <WebViewNormal
              source={{
                html: item.ads
              }}
              style={{ width: Dimensions.get('window').width }}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: color.backgroundContent,
    marginVertical: 5
  },
  wrapImage: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: color.backgroundContainer,
    borderWidth: 1
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2
  },
  wrapLikeCommentShare: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderTopColor: color.backgroundContainer,
    borderTopWidth: 1,
    paddingVertical: 10,
    marginTop: 5
  }
});

export default Post;
