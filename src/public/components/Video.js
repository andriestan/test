import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import VideoPlayer from 'react-native-video';

const { width } = Dimensions.get('window').width;

class Video extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenWidth: 0,
      heightScaled: 0
    };
  }

  render() {
    const { uri, styles, controls, paused, muted, repeat } = this.props;
    return (
      <VideoPlayer
        style={[
          {
            width,
            height: 200
          },
          styles
        ]}
        source={{ uri }}
        resizeMode="cover"
        paused={paused}
        muted={muted}
        repeat={repeat}
        controls={controls}
        onLoad={response => {
          // eslint-disable-next-line no-shadow
          const { width, height } = response.naturalSize;

          this.setState({
            heightScaled: Number(height),
            videoPaused: false,
            screenWidth: Number(width)
          });
        }}
      />
    );
  }
}

export default Video;
