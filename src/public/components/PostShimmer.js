import React from 'react';
import { View, StyleSheet } from 'react-native';

import config from '../config/config.json';

const color = config.color;

const PostShimmer = () => (
  <View style={styles.content}>
    <View style={{ paddingHorizontal: 20, paddingTop: 10 }}>
      <View style={{ flexDirection: 'row' }}>
        <View style={styles.wrapImage} />
        <View style={{ justifyContent: 'center', marginLeft: 10 }}>
          <View
            style={{
              backgroundColor: color.backgroundContainer,
              width: 100,
              height: 15,
              marginBottom: 5
            }}
          />
          <View style={{ backgroundColor: color.backgroundContainer, width: 100, height: 15 }} />
        </View>
      </View>
      <View style={{ marginVertical: 5 }}>
        <View
          style={{
            backgroundColor: color.backgroundContainer,
            width: '100%',
            height: 15,
            marginBottom: 5
          }}
        />
        <View
          style={{
            backgroundColor: color.backgroundContainer,
            width: '90%',
            height: 15,
            marginBottom: 5
          }}
        />
        <View style={{ backgroundColor: color.backgroundContainer, width: '80%', height: 15 }} />
      </View>
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingVertical: 10
          }}
        >
          <View style={{ backgroundColor: color.backgroundContainer, width: 100, height: 15 }} />
          <View style={{ backgroundColor: color.backgroundContainer, width: 100, height: 15 }} />
        </View>
      </View>
      <View style={styles.wrapLikeCommentShare}>
        <View style={{ backgroundColor: color.backgroundContainer, width: '20%', height: 15 }} />
        <View style={{ backgroundColor: color.backgroundContainer, width: '20%', height: 15 }} />
        <View style={{ backgroundColor: color.backgroundContainer, width: '20%', height: 15 }} />
      </View>
    </View>
  </View>
);

const styles = StyleSheet.create({
  content: {
    backgroundColor: color.backgroundContent,
    marginVertical: 5
  },
  wrapImage: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.backgroundContainer
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2
  },
  wrapLikeCommentShare: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderTopColor: color.backgroundContainer,
    borderTopWidth: 1,
    paddingVertical: 10,
    marginTop: 5
  }
});

export default PostShimmer;
