import { createMaterialTopTabNavigator } from 'react-navigation';

import Users from '../../search/screens/Users';
import Groups from '../../search/screens/Groups';
import Pages from '../../search/screens/Pages';
import Hashtag from '../../search/screens/Hashtag';
import config from '../config/config.json';

const color = config.color;

const Search = createMaterialTopTabNavigator(
  {
    UsersSearch: {
      screen: Users,
      navigationOptions: {
        tabBarLabel: 'Users'
      }
    },
    GroupsSearch: {
      screen: Groups,
      navigationOptions: {
        tabBarLabel: 'Groups'
      }
    },
    PagesSearch: {
      screen: Pages,
      navigationOptions: {
        tabBarLabel: 'Pages'
      }
    },
    HashtagPage: {
      screen: Hashtag,
      navigationOptions: {
        tabBarLabel: 'Hashtag'
      }
    }
  },
  {
    tabBarOptions: {
      style: {
        backgroundColor: color.background
      },
      tabStyle: {
        height: 40
      },
      labelStyle: {
        fontSize: 16
      },
      upperCaseLabel: false,
      indicatorStyle: {
        backgroundColor: color.white,
        height: 1.5
      },
      activeTintColor: color.white,
      inactiveTintColor: color.whitelv2
    }
  }
);

export default Search;
