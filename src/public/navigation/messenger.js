import { createMaterialTopTabNavigator } from 'react-navigation';

import Messages from '../../messenger/screens/Messages';
import People from '../../messenger/screens/People';
import Groups from '../../messenger/screens/Groups';
import config from '../config/config.json';

const color = config.color;

const Messenger = createMaterialTopTabNavigator(
  {
    MessagesMessenger: {
      screen: Messages,
      navigationOptions: {
        tabBarLabel: 'Messages'
      }
    },
    PeopleMessenger: {
      screen: People,
      navigationOptions: {
        tabBarLabel: 'People'
      }
    },
    GroupsMessenger: {
      screen: Groups,
      navigationOptions: {
        tabBarLabel: 'Groups'
      }
    }
  },
  {
    tabBarOptions: {
      style: {
        backgroundColor: color.background
      },
      tabStyle: {
        height: 40
      },
      labelStyle: {
        fontSize: 16
      },
      upperCaseLabel: false,
      indicatorStyle: {
        backgroundColor: color.white,
        height: 1.5
      },
      activeTintColor: color.white,
      inactiveTintColor: color.whitelv2
    }
  }
);

export default Messenger;
