import React from 'react';
import { Image } from 'react-native';
import {
  createBottomTabNavigator,
  createStackNavigator
} from 'react-navigation';
import { fromRight, zoomIn, fromBottom } from 'react-navigation-transitions';

// bottomTabs
import Home from '../../home/screens/Home';
import Live from '../../live/screens/Live';
import Tournament from '../../tournament/screens/Tournament';
import Community from '../../community/screens/Community';

import Search from './search';
import Messenger from './messenger';
import Suggested from '../../suggested/screens/Suggested';
import Chat from '../../messenger/screens/Chat';
import Comment from '../../comment/screens/Comment';
import CommentReply from '../../comment/screens/CommentReply';
import Video from '../../comment/screens/Video';
import Like from '../../comment/screens/Like';
import Post from '../../post/screens/Post';
import Browser from '../../browser/screens/Browser';

// profile
import MyProfile from '../../profile/screens/MyProfile';
import Profile from '../../profile/screens/Profile';
import ProfileSetting from '../../profile/screens/Setting';
import EditAvatar from '../../profile/screens/EditAvatar';
import EditCover from '../../profile/screens/EditCover';

// auth
import Login from '../../auth/screens/Login';
import Register from '../../auth/screens/Register';
import ForgotPassword from '../../auth/screens/ForgotPassword';

// other
import Faq from '../../other/screens/Faq';
import Feedback from '../../other/screens/Feedback';
import CommunityGuidelines from '../../other/screens/CommunityGuidelines';
import TermOfService from '../../other/screens/TermOfService';
import PrivacyPolicy from '../../other/screens/PrivacyPolicy';
import Blog from '../../other/screens/Blog';

// header
import { Header, HeaderTitle } from '../components';
import HeaderSearch from '../../search/components/Header';
import HeaderPost from '../../post/components/Header';
import HeaderBrowser from '../../browser/components/Header';
import HeaderEditAvatar from '../../profile/components/HeaderEditAvatar';
import HeaderEditCover from '../../profile/components/HeaderEditCover';

import Notification from '../../notification/screens/Notification';
import config from '../config/config.json';

const color = config.color;

const bottomTabs = createBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: ({ navigation }) => ({
        tabBarOnPress: () => {
          if (
            !!navigation &&
            navigation.isFocused() &&
            !!navigation.state.params &&
            !!navigation.state.params.scrollToTop
          ) {
            navigation.state.params.scrollToTop();
          } else {
            navigation.navigate(navigation.state.key);
          }
        }
      })
    },
    Live: {
      screen: Live,
      navigationOptions: {
        title: 'Live'
      }
    },
    Tournament,
    Community
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let icon;
        let style = { height: 20, width: 20 };
        if (routeName === 'Home') {
          if (focused) {
            icon = require('../assets/icon/ic-home-active.png');
          } else {
            icon = require('../assets/icon/ic-home-idle.png');
          }
        } else if (routeName === 'Live') {
          style = { height: 18, width: 25 };
          if (focused) {
            icon = require('../assets/icon/ic-live-active.png');
          } else {
            icon = require('../assets/icon/ic-live-idle.png');
          }
        } else if (routeName === 'Tournament') {
          if (focused) {
            icon = require('../assets/icon/ic-turnamen-active.png');
          } else {
            icon = require('../assets/icon/ic-turnamen-idle.png');
          }
        } else if (routeName === 'Community') {
          if (focused) {
            icon = require('../assets/icon/ic-komunitas-active.png');
          } else {
            icon = require('../assets/icon/ic-komunitas-idle.png');
          }
        }

        return <Image source={icon} style={style} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: color.blue,
      style: {
        backgroundColor: color.backgroundContainer,
        height: 50
      }
    }
  }
);

const handleCustomTransition = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];

  // Custom transitions go there
  if (
    prevScene &&
    prevScene.route.routeName === 'ProfileSetting' &&
    nextScene.route.routeName === 'EditAvatar'
  ) {
    return zoomIn(500);
  } else if (
    prevScene &&
    prevScene.route.routeName === 'ProfileSetting' &&
    nextScene.route.routeName === 'EditCover'
  ) {
    return zoomIn(500);
  } else if (
    prevScene &&
    prevScene.route.routeName === 'Comment' &&
    nextScene.route.routeName === 'Video'
  ) {
    return fromBottom(500);
  }
  return fromRight(500);
};

export default createStackNavigator(
  {
    bottomTabs: {
      screen: bottomTabs,
      navigationOptions: props => ({
        header: <Header {...props} />
      })
    },
    Login: {
      screen: Login,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    Register: {
      screen: Register,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    Search: {
      screen: Search,
      navigationOptions: props => ({
        header: <HeaderSearch {...props} />
      })
    },
    Messenger: {
      screen: Messenger,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    Suggested: {
      screen: Suggested,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    Chat: {
      screen: Chat,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    Notification: {
      screen: Notification,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    Profile: {
      screen: Profile,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    MyProfile: {
      screen: MyProfile,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    ProfileSetting: {
      screen: ProfileSetting,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    EditAvatar: {
      screen: EditAvatar,
      navigationOptions: props => ({
        header: <HeaderEditAvatar {...props} />
      })
    },
    EditCover: {
      screen: EditCover,
      navigationOptions: props => ({
        header: <HeaderEditCover {...props} />
      })
    },
    Comment: {
      screen: Comment,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    CommentReply: {
      screen: CommentReply,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    Video: {
      screen: Video,
      navigationOptions: () => ({
        header: null
      })
    },
    Post: {
      screen: Post,
      navigationOptions: props => ({
        header: <HeaderPost {...props} />
      })
    },
    Like: {
      screen: Like,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    Blog: {
      screen: Blog,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    Faq: {
      screen: Faq,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    Feedback: {
      screen: Feedback,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    CommunityGuidelines: {
      screen: CommunityGuidelines,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    TermOfService: {
      screen: TermOfService,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    PrivacyPolicy: {
      screen: PrivacyPolicy,
      navigationOptions: props => ({
        header: <HeaderTitle {...props} />
      })
    },
    Browser: {
      screen: Browser,
      navigationOptions: props => ({
        header: <HeaderBrowser {...props} />
      })
    }
  },
  {
    transitionConfig: nav => handleCustomTransition(nav)
  }
);
