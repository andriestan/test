import { createAppContainer, createDrawerNavigator } from 'react-navigation';

import { Drawer } from '../components';

import bottomTab from './bottomTab';

const drawer = createDrawerNavigator(
  {
    bottomTab
  },
  {
    contentComponent: Drawer
  }
);

export default createAppContainer(drawer);
