import config from '../config/config.json';

const Fetch = async (url, item = false) => {
  // eslint-disable-next-line no-undef
  const bodyFormData = new FormData({ server_key: config.server_key });
  bodyFormData.append('server_key', config.server_key);

  if (item) {
    const data = Object.entries(item);
    // eslint-disable-next-line array-callback-return
    data.map(value => {
      bodyFormData.append(value[0], value[1]);
    });
  }

  // eslint-disable-next-line no-undef
  let response = await fetch(config.host + url, {
    method: 'post',
    body: bodyFormData,
    headers: { 'Content-Type': 'multipart/form-data' }
  });
  response = await response.json();
  return response;
};

const FetchWebView = async (url, item = false) => {
  // eslint-disable-next-line no-undef
  const bodyFormData = new FormData({ server_key: config.server_key });
  bodyFormData.append('server_key', config.server_key);

  if (item) {
    const data = Object.entries(item);
    // eslint-disable-next-line array-callback-return
    data.map(value => {
      bodyFormData.append(value[0], value[1]);
    });
  }

  // eslint-disable-next-line no-undef
  const response = await fetch(config.host + url, {
    method: 'post',
    body: bodyFormData,
    headers: { 'Content-Type': 'multipart/form-data' }
  });
  return response;
};

export { Fetch, FetchWebView };
