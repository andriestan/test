import AsyncStorage from '@react-native-community/async-storage';

const getUser = async () => {
  const user = await AsyncStorage.getItem('user');
  return JSON.parse(user);
};

export { getUser };
