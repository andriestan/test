import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  StyleSheet,
  Dimensions,
  View,
  TouchableHighlight,
  RefreshControl
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { WebView } from '../../public/components';
import { getUser } from '../../public/services';

const color = config.color;

class Faq extends Component {
  constructor() {
    super();
    this.state = {
      access_token: '',
      refreshing: false
    };
  }

  async componentDidMount() {
    const user = await getUser();
    this.setState({ access_token: user.access_token });
  }

  onRefresh = async () => {
    await this.setState({ refreshing: true });
    this.setState({ refreshing: false });
  };

  render() {
    return this.props.auth.isLogin ? (
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
        }
      >
        {!this.state.refreshing ? (
          <WebView
            source={{
              uri: `${config.host}/menu_faq.php?id=${this.props.profile.userData.user_id}&s=${
                this.state.access_token
              }`
            }}
            style={{ width: Dimensions.get('window').width }}
          />
        ) : null}
      </ScrollView>
    ) : (
      <View style={[styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
        <TouchableHighlight
          style={{
            paddingVertical: 10,
            width: '70%',
            alignItems: 'center',
            backgroundColor: color.blue,
            borderRadius: 10
          }}
          underlayColor={color.buttonEffect}
          onPress={() => this.props.navigation.navigate('Login', { title: 'Login' })}
        >
          <Text style={{ color: color.white }}>Login / Register</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContent
  }
});

const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile
});

export default connect(mapStateToProps)(Faq);
