const initialState = {
  isLoading: true,
  count: 0,
  data: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_NOTIFICATION_PENDING':
      return { ...state, isLoading: true };
    case 'GET_NOTIFICATION_SUCCESS':
      return {
        ...state,
        isLoading: false,
        data: action.payload.data,
        count: action.payload.count
      };
    case 'SEEN_NOTIFICATION_GLOBAL_SUCCESS':
      return {
        ...state,
        isLoading: false,
        count: 0
      };
    default:
      return state;
  }
};
