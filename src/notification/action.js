import { getUser, Fetch } from '../public/services';

export const getNotification = () => async dispatch => {
  dispatch({ type: 'GET_NOTIFICATION_PENDING' });
  const user = await getUser();
  const data = {
    fetch: 'notifications'
  };
  const response = await Fetch(
    `/api/get-general-data?access_token=${user.access_token}`,
    data
  );
  if (response.api_status === 200) {
    dispatch({
      type: 'GET_NOTIFICATION_SUCCESS',
      payload: {
        data: response.notifications,
        count: response.new_notifications_count
      }
    });
  }
};

export const seenGlobalNotification = () => async dispatch => {
  const user = await getUser();
  const data = {
    fetch: 'notifications'
  };
  const response = await Fetch(
    `/api/get-general-data?access_token=${user.access_token}&seen=true`,
    data
  );
  if (response.api_status === 200) {
    dispatch({
      type: 'SEEN_NOTIFICATION_GLOBAL_SUCCESS'
    });
  }
};

export const seenNotification = id => async dispatch => {
  const user = await getUser();
  const data = {
    fetch: 'notifications'
  };
  const response = await Fetch(
    `/api/get-general-data?access_token=${
      user.access_token
    }&seen_id=true&id=${id}`,
    data
  );
  if (response.api_status === 200) {
    dispatch(getNotification());
  }
};
