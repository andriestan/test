import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableHighlight
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { seenGlobalNotification, seenNotification } from '../action';

const color = config.color;

class Notification extends Component {
  componentDidMount() {
    if (this.props.notification.count > 0) {
      this.props.seenGlobalNotification();
    }
  }

  handleClick(item) {
    this.props.seenNotification(item.id);
    if (item.type === 'following') {
      this.props.navigation.navigate('Profile', {
        title: item.notifier.username,
        userData: item.notifier
      });
    } else {
      this.props.navigation.navigate('Comment', {
        title: 'Comments',
        postId: item.post_id
      });
    }
  }

  renderItem = ({ item }) => (
    <TouchableHighlight
      style={[
        {
          flexDirection: 'row',
          paddingHorizontal: 20,
          paddingVertical: 10
        },
        item.seen_id === '0'
          ? {
              backgroundColor: color.buttonEffect,
              borderBottomColor: 'rgba(0,0,0,0.2)',
              borderBottomWidth: 1
            }
          : null
      ]}
      underlayColor={color.buttonEffect}
      // eslint-disable-next-line no-confusing-arrow
      onPress={() => this.handleClick(item)}
    >
      <Fragment>
        <View style={styles.wrapPhotoProfile}>
          <Text style={{ fontSize: 8, color: color.whitelv2 }}>Image</Text>
          <Image
            source={{ uri: item.notifier.avatar }}
            style={styles.photoProfile}
          />
        </View>
        <View
          style={{
            flex: 1,
            paddingLeft: 10,
            flexDirection: 'row',
            justifyContent: 'space-between'
          }}
        >
          <View style={{ justifyContent: 'center' }}>
            <View style={{ flexDirection: 'row', marginBottom: 3 }}>
              <Text
                style={{
                  fontSize: 14,
                  color: color.blue
                }}
              >
                {item.notifier.username}
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  color: color.white
                }}
              >
                {` ${item.type_text}`}
              </Text>
            </View>
            <Text style={{ fontSize: 12, color: color.whitelv2 }}>
              {item.time_text_string}
            </Text>
          </View>
        </View>
      </Fragment>
    </TouchableHighlight>
  );

  render() {
    return (
      <View style={styles.container}>
        {this.props.notification.data.length > 0 ? (
          <FlatList
            data={this.props.notification.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderItem}
          />
        ) : (
          <Text
            style={{
              color: color.whitelv2,
              textAlign: 'center',
              marginTop: 100
            }}
          >
            Empty
          </Text>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.backgroundContent,
    flex: 1
  },
  wrapPhotoProfile: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
    borderColor: color.backgroundContainer,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoProfile: {
    width: '100%',
    height: '100%',
    borderRadius: 45 / 2,
    position: 'absolute'
  }
});

const mapStateToProps = state => ({
  notification: state.notification
});

const mapDispatchToProps = {
  seenGlobalNotification,
  seenNotification
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Notification);
