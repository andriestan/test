import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import LikePage from '../components/Like';
import config from '../../public/config/config.json';
import Shimmer from '../components/LikeShimmer';

const color = config.color;

class Like extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true
    };
  }

  componentDidMount() {
    setTimeout(() => this.setState({ isLoading: false }), 1);
  }

  render() {
    return !this.state.isLoading ? (
      <LikePage navigation={this.props.navigation} />
    ) : (
      <View style={styles.container}>
        <Shimmer />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.backgroundContent,
    flex: 1
  }
});

export default Like;
