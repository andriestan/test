import React, { Component } from 'react';
import { View, StatusBar, StyleSheet } from 'react-native';
import VideoPlayer from 'react-native-video-controls';
import Orientation from 'react-native-orientation';

class Video extends Component {
  enterFullscreen() {
    Orientation.lockToLandscape();
  }

  exitFullscreen() {
    Orientation.lockToPortrait();
  }

  async goBack() {
    await Orientation.lockToPortrait();
    this.props.navigation.goBack();
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <VideoPlayer
          source={{ uri: this.props.navigation.getParam('uri') }}
          navigator={this.props.navigator}
          onBack={() => this.goBack()}
          onEnterFullscreen={() => this.enterFullscreen()}
          onExitFullscreen={() => this.exitFullscreen()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default Video;
