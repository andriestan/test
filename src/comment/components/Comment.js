import React, { Component, Fragment } from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  TextInput,
  Keyboard,
  ScrollView,
  TouchableHighlight,
  ActivityIndicator,
  RefreshControl
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import WrapComment from '../components/WrapComment';
import {
  getComment,
  createComment,
  updateComment,
  handleLikeComment,
  storePost,
  getPost
} from '../action';
import Shimmer from '../components/Shimmer';
import { Loading } from '../../public/components';
import WrapCommentShimmer from '../components/WrapCommentShimmer';
import Post from './Post';

const color = config.color;

class Comment extends Component {
  constructor() {
    super();
    this.state = {
      keyboard: false,
      comment: '',
      refreshing: false
    };
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.handleKeyboardOn.bind(this)
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.handleKeyboardOff.bind(this)
    );
  }

  componentDidMount() {
    if (this.props.navigation.getParam('data')) {
      this.props.storePost(this.props.navigation.getParam('data'));
    } else {
      this.props.getPost(this.props.navigation.getParam('postId'));
    }

    this.props.getComment(this.props.navigation.getParam('postId'));
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    this.props.getComment(this.props.navigation.getParam('postId'));
    this.setState({ refreshing: false });
  };

  onEndReached = () => {
    if (!this.props.comment.isLoadingUpdate) {
      this.props.updateComment(
        this.props.navigation.getParam('postId'),
        this.props.comment.comments[this.props.comment.comments.length - 1].id
      );
    }
  };

  handleKeyboardOn() {
    this.setState({ keyboard: true });
  }

  handleKeyboardOff() {
    this.setState({ keyboard: false });
  }

  async createComment() {
    Keyboard.dismiss();
    await this.props.createComment(
      this.props.navigation.getParam('postId'),
      this.state.comment
    );
    this.setState({ comment: '' });
  }

  render() {
    return !this.props.comment.isLoading ? (
      <View style={styles.container}>
        {this.props.comment.comments.length > 0 ? (
          <FlatList
            ref={list => {
              this.flatList = list;
            }}
            data={this.props.comment.comments}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={
              this.props.comment.isPagination ? this.onEndReached : false
            }
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
            ListFooterComponent={
              this.props.comment.isPagination
                ? () => (
                    <Loading
                      style={{ height: 50, marginBottom: 5 }}
                      color={color.white}
                    />
                  )
                : null
            }
            // eslint-disable-next-line no-confusing-arrow
            renderItem={({ item, index }) =>
              !this.props.comment.isLoadingComment ? (
                <Fragment>
                  {// eslint-disable-next-line no-nested-ternary
                  index === 0 ? (
                    !this.props.comment.isLoadingPost ? (
                      <Post
                        item={this.props.comment.post}
                        navigation={this.props.navigation}
                      />
                    ) : null
                  ) : null}
                  <WrapComment
                    item={item}
                    navigation={this.props.navigation}
                    // eslint-disable-next-line no-confusing-arrow
                    handleLike={() =>
                      this.props.auth.isLogin
                        ? this.props.handleLikeComment(item.id)
                        : this.props.navigation.navigate('Login', {
                            title: 'Login'
                          })
                    }
                    handleReply={() =>
                      this.props.navigation.navigate('CommentReply', {
                        title: 'Reply',
                        id: item.id,
                        data: item
                      })
                    }
                  />
                </Fragment>
              ) : (
                <Fragment>
                  {index === 0 ? (
                    <Fragment>
                      {!this.props.comment.isLoadingPost ? (
                        <Post
                          item={this.props.comment.post}
                          navigation={this.props.navigation}
                        />
                      ) : null}
                      <WrapCommentShimmer />
                    </Fragment>
                  ) : null}
                  <WrapComment
                    item={item}
                    navigation={this.props.navigation}
                    // eslint-disable-next-line no-confusing-arrow
                    handleLike={() =>
                      this.props.auth.isLogin
                        ? this.props.handleLikeComment(item.id)
                        : this.props.navigation.navigate('Login', {
                            title: 'Login'
                          })
                    }
                    handleReply={() =>
                      this.props.navigation.navigate('CommentReply', {
                        title: 'Reply',
                        id: item.id,
                        data: item
                      })
                    }
                  />
                </Fragment>
              )
            }
          />
        ) : (
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
          >
            {!this.props.comment.isLoadingPost ? (
              <Post
                item={this.props.comment.post}
                navigation={this.props.navigation}
              />
            ) : null}
            <Text
              style={{
                color: color.white,
                textAlign: 'center',
                marginVertical: 100
              }}
            >
              Empty
            </Text>
          </ScrollView>
        )}
        <View
          style={{
            paddingHorizontal: 10,
            borderTopColor: 'rgba(0,0,0,0.1)',
            borderTopWidth: 1
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              marginBottom: this.state.keyboard ? 10 : 5,
              marginTop: 5
            }}
          >
            <View
              style={{
                flex: 1,
                backgroundColor: color.backgroundContent,
                paddingHorizontal: 10,
                height: 50,
                justifyContent: 'center',
                borderRadius: 20
              }}
            >
              <TextInput
                placeholder="Write comment"
                placeholderTextColor={color.whitelv2}
                style={{ paddingVertical: 10, color: color.white }}
                multiline
                value={this.state.comment}
                onChangeText={text => this.setState({ comment: text })}
                autoFocus={this.props.navigation.getParam('autoFocus')}
              />
            </View>
            <TouchableHighlight
              style={{
                height: 50,
                width: 50,
                borderRadius: 50 / 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 5,
                backgroundColor: color.backgroundContent
              }}
              underlayColor={color.buttonEffect}
              // eslint-disable-next-line no-confusing-arrow
              onPress={() =>
                this.props.auth.isLogin
                  ? this.createComment()
                  : this.props.navigation.navigate('Login', { title: 'Login' })
              }
              disabled={this.state.comment === ''}
            >
              {!this.props.comment.isLoadingComment ? (
                <Text
                  style={{
                    color:
                      this.state.comment === '' ? color.whitelv2 : color.white,
                    fontSize: 10
                  }}
                >
                  Send
                </Text>
              ) : (
                <ActivityIndicator color={color.whitelv2} />
              )}
            </TouchableHighlight>
          </View>
        </View>
      </View>
    ) : (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
          />
        }
        style={styles.container}
      >
        {!this.props.comment.isLoadingPost ? (
          <Post
            item={this.props.comment.post}
            navigation={this.props.navigation}
          />
        ) : null}
        <Shimmer />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContainer
  }
});

const mapStateToProps = state => ({
  comment: state.comment,
  auth: state.auth
});

const mapDispatchToProps = {
  getComment,
  createComment,
  updateComment,
  handleLikeComment,
  storePost,
  getPost
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Comment);
