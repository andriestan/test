import React from 'react';
import { View, StyleSheet } from 'react-native';

import config from '../../public/config/config.json';

const color = config.color;

const WrapCommentShimmer = () => (
  <View style={styles.content}>
    <View style={styles.wrapImage} />
    <View style={{ flex: 1, marginLeft: 10 }}>
      <View
        style={{
          backgroundColor: color.backgroundContent,
          borderRadius: 10,
          height: 70
        }}
      />
    </View>
  </View>
);

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 5
  },
  wrapImage: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.backgroundContent
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: 45 / 2
  }
});

export default WrapCommentShimmer;
