import React, { Component, Fragment } from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  TextInput,
  Keyboard,
  ScrollView,
  TouchableHighlight,
  ActivityIndicator,
  RefreshControl
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import WrapComment from '../components/WrapComment';
import WrapCommentReply from './WrapCommentReply';
import {
  getCommentReply,
  createCommentReply,
  updateCommentReply,
  handleLikeComment
} from '../action';
import Shimmer from '../components/Shimmer';
import { Loading } from '../../public/components';
import WrapCommentShimmer from '../components/WrapCommentShimmer';

const color = config.color;

class CommentReply extends Component {
  constructor() {
    super();
    this.state = {
      keyboard: false,
      comment: '',
      refreshing: false
    };
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.handleKeyboardOn.bind(this)
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.handleKeyboardOff.bind(this)
    );
  }

  componentDidMount() {
    this.props.getCommentReply(this.props.navigation.getParam('id'));
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    this.props.getCommentReply(this.props.navigation.getParam('id'));
    this.setState({ refreshing: false });
  };

  onEndReached = () => {
    if (!this.props.comment.isLoadingUpdateReply) {
      this.props.updateCommentReply(
        this.props.navigation.getParam('id'),
        this.props.comment.commentsReply[
          this.props.comment.commentsReply.length - 1
        ].id
      );
    }
  };

  handleKeyboardOn() {
    this.setState({ keyboard: true });
  }

  handleKeyboardOff() {
    this.setState({ keyboard: false });
  }

  async createCommentReply() {
    Keyboard.dismiss();
    await this.props.createCommentReply(
      this.props.navigation.getParam('id'),
      this.state.comment
    );
    this.setState({ comment: '' });
  }

  render() {
    return !this.props.comment.isLoadingReply ? (
      <View style={styles.container}>
        {this.props.comment.commentsReply.length > 0 ? (
          <FlatList
            ref={list => {
              this.flatList = list;
            }}
            data={this.props.comment.commentsReply}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={
              this.props.comment.isPaginationReply ? this.onEndReached : false
            }
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
            ListFooterComponent={
              this.props.comment.isPaginationReply
                ? () => (
                    <Loading
                      style={{ height: 50, marginBottom: 5 }}
                      color={color.white}
                    />
                  )
                : null
            }
            // eslint-disable-next-line no-confusing-arrow
            renderItem={({ item, index }) =>
              !this.props.comment.isLoadingCommentReply ? (
                <Fragment>
                  {index === 0 ? (
                    <WrapComment
                      reply
                      item={this.props.navigation.getParam('data')}
                      navigation={this.props.navigation}
                      // eslint-disable-next-line no-confusing-arrow
                      handleLike={() =>
                        this.props.auth.isLogin
                          ? this.props.handleLikeComment(
                              this.props.navigation.getParam('data').id
                            )
                          : this.props.navigation.navigate('Login', {
                              title: 'Login'
                            })
                      }
                      handleReply={() =>
                        this.props.navigation.navigate('CommentReply', {
                          title: 'Reply'
                        })
                      }
                    />
                  ) : null}
                  <WrapCommentReply
                    item={item}
                    navigation={this.props.navigation}
                  />
                </Fragment>
              ) : (
                <Fragment>
                  {index === 0 ? (
                    <Fragment>
                      <WrapComment
                        reply
                        item={this.props.navigation.getParam('data')}
                        navigation={this.props.navigation}
                        // eslint-disable-next-line no-confusing-arrow
                        handleLike={() =>
                          this.props.auth.isLogin
                            ? this.props.handleLikeComment(
                                this.props.navigation.getParam('data').id
                              )
                            : this.props.navigation.navigate('Login', {
                                title: 'Login'
                              })
                        }
                        handleReply={() =>
                          this.props.navigation.navigate('CommentReply', {
                            title: 'Reply'
                          })
                        }
                      />
                      <View style={{ paddingLeft: 20 }}>
                        <WrapCommentShimmer />
                      </View>
                    </Fragment>
                  ) : null}
                  <View style={{ paddingLeft: 20 }}>
                    <WrapComment
                      item={item}
                      navigation={this.props.navigation}
                      // eslint-disable-next-line no-confusing-arrow
                      handleLike={() =>
                        this.props.auth.isLogin
                          ? this.props.handleLikeComment(item.id)
                          : this.props.navigation.navigate('Login', {
                              title: 'Login'
                            })
                      }
                      handleReply={() =>
                        this.props.navigation.navigate('CommentReply', {
                          title: 'Reply'
                        })
                      }
                    />
                  </View>
                </Fragment>
              )
            }
          />
        ) : (
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
          >
            <WrapComment
              item={this.props.navigation.getParam('data')}
              navigation={this.props.navigation}
              // eslint-disable-next-line no-confusing-arrow
              handleLike={() =>
                this.props.auth.isLogin
                  ? this.props.handleLikeComment(
                      this.props.navigation.getParam('data').id
                    )
                  : this.props.navigation.navigate('Login', {
                      title: 'Login'
                    })
              }
              handleReply={() =>
                this.props.navigation.navigate('CommentReply', {
                  title: 'Reply',
                  id: this.props.navigation.getParam('data').id
                })
              }
            />
            <Text
              style={{
                color: color.white,
                textAlign: 'center',
                marginVertical: 100
              }}
            >
              Empty
            </Text>
          </ScrollView>
        )}
        <View
          style={{
            paddingHorizontal: 10,
            borderTopColor: 'rgba(0,0,0,0.1)',
            borderTopWidth: 1
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              marginBottom: this.state.keyboard ? 10 : 5,
              marginTop: 5
            }}
          >
            <View
              style={{
                flex: 1,
                backgroundColor: color.backgroundContent,
                paddingHorizontal: 10,
                height: 50,
                justifyContent: 'center',
                borderRadius: 20
              }}
            >
              <TextInput
                placeholder="Write comment"
                placeholderTextColor={color.whitelv2}
                style={{ paddingVertical: 10, color: color.white }}
                multiline
                value={this.state.comment}
                onChangeText={text => this.setState({ comment: text })}
                autoFocus={this.props.navigation.getParam('autoFocus')}
              />
            </View>
            <TouchableHighlight
              style={{
                height: 50,
                width: 50,
                borderRadius: 50 / 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 5,
                backgroundColor: color.backgroundContent
              }}
              underlayColor={color.buttonEffect}
              // eslint-disable-next-line no-confusing-arrow
              onPress={() =>
                this.props.auth.isLogin
                  ? this.createCommentReply()
                  : this.props.navigation.navigate('Login', { title: 'Login' })
              }
              disabled={this.state.comment === ''}
            >
              {!this.props.comment.isLoadingCommentReply ? (
                <Text
                  style={{
                    color:
                      this.state.comment === '' ? color.whitelv2 : color.white,
                    fontSize: 10
                  }}
                >
                  Send
                </Text>
              ) : (
                <ActivityIndicator color={color.whitelv2} />
              )}
            </TouchableHighlight>
          </View>
        </View>
      </View>
    ) : (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
          />
        }
        style={styles.container}
        scrollEnabled={false}
      >
        <WrapComment
          reply
          item={this.props.navigation.getParam('data')}
          navigation={this.props.navigation}
          // eslint-disable-next-line no-confusing-arrow
          handleLike={() =>
            this.props.auth.isLogin
              ? this.props.handleLikeComment(
                  this.props.navigation.getParam('data').id
                )
              : this.props.navigation.navigate('Login', {
                  title: 'Login'
                })
          }
          handleReply={() =>
            this.props.navigation.navigate('CommentReply', {
              title: 'Reply',
              id: this.props.navigation.getParam('data').id
            })
          }
        />
        <View style={{ paddingLeft: 20 }}>
          <Shimmer />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.backgroundContainer
  }
});

const mapStateToProps = state => ({
  comment: state.comment,
  auth: state.auth
});

const mapDispatchToProps = {
  getCommentReply,
  createCommentReply,
  updateCommentReply,
  handleLikeComment
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommentReply);
