import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableHighlight,
  Image
} from 'react-native';
import { connect } from 'react-redux';

import { getPostLike } from '../action';
import config from '../../public/config/config.json';
import Shimmer from '../components/LikeShimmer';

const color = config.color;

class Like extends Component {
  componentDidMount() {
    this.props.getPostLike(this.props.navigation.getParam('postId'));
  }

  renderItem = ({ item }) => (
    <TouchableHighlight
      style={{
        flexDirection: 'row',
        paddingHorizontal: 20,
        paddingVertical: 10
      }}
      // eslint-disable-next-line no-confusing-arrow
      onPress={() =>
        this.props.profile.userData.user_id === item.user_id
          ? this.props.navigation.navigate('MyProfile', {
              title: 'My Profile'
            })
          : this.props.navigation.navigate('Profile', {
              title: item.username,
              userData: item
            })
      }
      underlayColor={color.buttonEffect}
    >
      <Fragment>
        <View style={styles.wrapPhotoProfile}>
          <Text style={{ fontSize: 10 }}>Image</Text>
          <Image source={{ uri: item.avatar }} style={styles.photoProfile} />
        </View>
        <View
          style={{
            flex: 1,
            paddingLeft: 15,
            paddingTop: 3,
            justifyContent: 'center'
          }}
        >
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              marginBottom: 3,
              color: color.white
            }}
          >
            {item.username}
          </Text>
          <Text style={{ fontSize: 12, color: color.white }}>{item.name}</Text>
        </View>
      </Fragment>
    </TouchableHighlight>
  );

  render() {
    return !this.props.comment.isLoadingLikes ? (
      <View
        style={[
          styles.container,
          this.props.comment.likes.length === 0
            ? { justifyContent: 'center', alignItems: 'center' }
            : null
        ]}
      >
        {this.props.comment.likes.length > 0 ? (
          <FlatList
            data={this.props.comment.likes}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderItem}
          />
        ) : (
          <Text style={{ color: color.whitelv2 }}>Empty</Text>
        )}
      </View>
    ) : (
      <View style={styles.container}>
        <Shimmer />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.backgroundContent,
    flex: 1
  },
  wrapPhotoProfile: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    borderColor: color.backgroundContainer,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoProfile: {
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2,
    position: 'absolute'
  }
});

const mapStateToProps = state => ({
  comment: state.comment,
  profile: state.profile
});

const mapDispatchToProps = {
  getPostLike
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Like);
