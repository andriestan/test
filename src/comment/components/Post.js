import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import Hyperlink from 'react-native-hyperlink';

import {
  MultiImages,
  Image as ImageComponent,
  Video
} from '../../public/components';
import config from '../../public/config/config.json';
// eslint-disable-next-line import/no-extraneous-dependencies
const linkify = require('linkify-it')();

const color = config.color;

linkify.add('@', {
  validate(text, pos, self) {
    const tail = text.slice(pos);
    if (!self.re.twitter) {
      // eslint-disable-next-line no-param-reassign
      self.re.twitter = new RegExp(
        `^([a-zA-Z0-9_]){1,15}(?!_)(?=$|${self.re.src_ZPCc})`
      );
    }
    if (self.re.twitter.test(tail)) {
      // Linkifier allows punctuation chars before prefix,
      // but we additionally disable `@` ("@@mention" is invalid)
      if (pos >= 2 && tail[pos - 2] === '@') {
        return false;
      }
      return tail.match(self.re.twitter)[0].length;
    }
    return 0;
  },
  normalize(match) {
    // eslint-disable-next-line no-param-reassign
    match.url = match.url.replace(/^@/, '');
  }
});

linkify.add('#', {
  validate(text, pos, self) {
    const tail = text.slice(pos);
    if (!self.re.twitter) {
      // eslint-disable-next-line no-param-reassign
      self.re.twitter = new RegExp(
        `^([a-zA-Z0-9_]){1,15}(?!_)(?=$|${self.re.src_ZPCc})`
      );
    }
    if (self.re.twitter.test(tail)) {
      // Linkifier allows punctuation chars before prefix,
      // but we additionally disable `@` ("@@mention" is invalid)
      if (pos >= 2 && tail[pos - 2] === '#') {
        return false;
      }
      return tail.match(self.re.twitter)[0].length;
    }
    return 0;
  },
  normalize(match) {
    // eslint-disable-next-line no-param-reassign
    match.url = match.url.replace(/^#/, '');
  }
});

class Post extends Component {
  constructor() {
    super();
    this.state = {
      visibleVideo: false
    };
  }

  handleVideo(item) {
    this.props.navigation.navigate('Video', {
      uri: item.postFile_full
    });
  }

  render() {
    const { item, navigation } = this.props;
    return (
      <View style={styles.content}>
        <View style={{ paddingHorizontal: 20, paddingTop: 10 }}>
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}
          >
            <View style={{ flexDirection: 'row', flex: 1 }}>
              <View style={styles.wrapImage}>
                <Text style={{ fontSize: 12, color: color.whitelv2 }}>
                  Image
                </Text>
                <Image
                  style={styles.image}
                  source={{ uri: item.publisher.avatar }}
                />
              </View>
              <View style={{ justifyContent: 'center', marginLeft: 10 }}>
                <View
                  style={{
                    flexDirection:
                      item.publisher.name.length < 18 ? 'row' : 'column'
                  }}
                >
                  <Text style={{ fontWeight: 'bold', color: color.white }}>
                    {item.publisher.name}
                  </Text>
                  {item.event_id !== '0' ? (
                    <View style={{ flexDirection: 'row' }}>
                      <Text
                        style={{
                          color: color.white,
                          fontWeight: 'bold',
                          marginHorizontal: 10
                        }}
                      >
                        >
                      </Text>
                      <Text style={{ color: color.white, fontWeight: 'bold' }}>
                        {item.event.name}
                      </Text>
                    </View>
                  ) : null}
                  {item.postFile_type === 'post_cover' ||
                  item.postFile_type === 'post_avatar' ? (
                    <Text style={{ color: color.whitelv2 }}>
                      {item.postFile_type === 'post_cover'
                        ? `${
                            item.publisher.name.length < 18 ? ' ' : ''
                          }Changed his profile cover`
                        : `${
                            item.publisher.name.length < 18 ? ' ' : ''
                          }Changed his profile picture`}
                    </Text>
                  ) : null}
                </View>
                <Text style={{ fontSize: 12, color: color.whitelv2 }}>
                  {item.post_time}
                </Text>
              </View>
            </View>
          </View>
          {item.postText !== '' ? (
            <View style={{ marginBottom: 5, marginTop: 15 }}>
              <Hyperlink
                linkify={linkify}
                // eslint-disable-next-line no-confusing-arrow
                onPress={(url, text) =>
                  // eslint-disable-next-line no-nested-ternary
                  url.slice(0, 4) === 'http'
                    ? navigation.navigate('Browser', { title: 'Oxa', url })
                    : // eslint-disable-next-line no-nested-ternary
                    text[0] === '@'
                    ? item.publisher.username === url
                      ? navigation.navigate('My Profile', { title: url })
                      : navigation.navigate('Profile', {
                          title: url,
                          mention: true,
                          username: url
                        })
                    : navigation.navigate('HashtagPage', { hashtag: text })
                }
                linkStyle={{
                  textDecorationLine: 'underline',
                  color: color.blue
                }}
              >
                <Text style={{ color: color.white }}>{item.Orginaltext}</Text>
              </Hyperlink>
            </View>
          ) : null}
          {// eslint-disable-next-line no-nested-ternary
          item.postFile_full !== '' ? (
            // eslint-disable-next-line no-nested-ternary
            item.postFile_type === 'post_image' ||
            item.postFile_type === 'post_cover' ||
            item.postFile_type === 'post_avatar' ? (
              <ImageComponent
                uri={item.postFile_full}
                style={{
                  marginVertical: 10,
                  marginHorizontal: -20,
                  borderTopColor: color.backgroundContainer,
                  borderTopWidth: 1,
                  borderBottomColor: color.backgroundContainer,
                  borderBottomWidth: 1
                }}
              />
            ) : item.postFile_type === 'post_video' ? (
              <TouchableHighlight
                onPress={() => this.handleVideo(item)}
                underlayColor="rgba(0,0,0,0)"
              >
                <Video
                  styles={{ marginHorizontal: -20 }}
                  uri={item.postFile_full}
                  muted
                  repeat
                  paused={this.state.visibleVideo}
                />
              </TouchableHighlight>
            ) : null
          ) : null}
          {item.photo_multi ? (
            <MultiImages
              data={item.photo_multi}
              style={{ marginVertical: 10, marginHorizontal: -20 }}
            />
          ) : null}
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingVertical: 10,
            paddingHorizontal: 20
          }}
        >
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('Like', {
                title:
                  item.post_likes <= '1'
                    ? `${item.post_likes} Like`
                    : `${item.post_likes} Likes`,
                postId: item.post_id
              })
            }
          >
            <Text
              style={{ fontWeight: 'bold', fontSize: 12, color: color.white }}
            >
              {item.post_likes} Likes
            </Text>
          </TouchableOpacity>
          <Text
            style={{ fontWeight: 'bold', fontSize: 12, color: color.white }}
          >
            {item.post_comments} Comments
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: color.backgroundContent,
    marginVertical: 5,
    paddingBottom: 10,
    marginBottom: 5
  },
  wrapImage: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: color.backgroundContainer,
    borderWidth: 1
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: 55 / 2
  },
  wrapLikeCommentShare: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderTopColor: color.backgroundContainer,
    borderTopWidth: 1,
    paddingVertical: 10,
    marginTop: 5
  }
});

export default Post;
