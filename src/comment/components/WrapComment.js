import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Modal,
  TouchableHighlight,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';

import config from '../../public/config/config.json';
import { deleteComment } from '../action';

const color = config.color;

class WrapComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleDelete: false
    };
  }

  async handleDelete() {
    await this.props.deleteComment(this.props.item.id, this.props.item.post_id);
    this.setState({ visibleDelete: false });
  }

  render() {
    const { item, navigation, handleLike, handleReply, reply } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.wrapImage}>
          <Text style={{ color: color.whitelv2, fontSize: 9 }}>Image</Text>
          <Image style={styles.image} source={{ uri: item.publisher.avatar }} />
        </View>
        <View style={{ flex: 1, marginLeft: 10 }}>
          <View
            style={{
              backgroundColor: color.backgroundContent,
              padding: 10,
              borderRadius: 10
            }}
          >
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <TouchableOpacity
                // eslint-disable-next-line no-confusing-arrow
                onPress={() =>
                  item.onwer
                    ? navigation.navigate('MyProfile', {
                        title: 'My Profile'
                      })
                    : navigation.navigate('Profile', {
                        title: item.publisher.username,
                        userData: item.publisher
                      })
                }
              >
                <Text style={{ color: color.white, fontWeight: 'bold' }}>
                  {item.publisher.name}
                </Text>
              </TouchableOpacity>
              {item.onwer ? (
                <TouchableOpacity
                  onPress={() => this.setState({ visibleDelete: true })}
                >
                  <Image
                    source={require('../../public/assets/icon/ic-trash-idle.png')}
                    style={{ width: 14, height: 16 }}
                  />
                </TouchableOpacity>
              ) : null}
            </View>
            <Text style={{ color: color.white }}>{item.Orginaltext}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: '98%',
              marginTop: 5,
              alignSelf: 'flex-end'
            }}
          >
            <TouchableOpacity style={{ width: '20%' }} onPress={handleLike}>
              <Text
                style={[
                  { color: color.whitelv2 },
                  item.is_comment_liked ? { fontWeight: 'bold' } : null
                ]}
              >
                {item.comment_likes} Like
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ width: '20%' }}
              onPress={handleReply}
              disabled={reply}
            >
              <Text style={{ color: color.whitelv2 }}>
                {item.replies} Reply
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <Modal
          transparent
          animationType="slide"
          visible={this.state.visibleDelete}
          onRequestClose={() => this.setState({ visibleDelete: false })}
        >
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              backgroundColor: 'rgba(0,0,0,0.2)'
            }}
          >
            <View
              style={{
                borderRadius: 10,
                backgroundColor: color.white
              }}
            >
              <TouchableHighlight
                style={{
                  paddingVertical: 20,
                  alignItems: 'center',
                  paddingHorizontal: 20
                }}
                underlayColor={color.whitelv2}
                onPress={() => this.handleDelete()}
              >
                {!this.props.comment.isLoadingDelete ? (
                  <Text style={{ fontSize: 20 }}>Delete</Text>
                ) : (
                  <ActivityIndicator color={color.backgroundContent} />
                )}
              </TouchableHighlight>
            </View>
            <TouchableHighlight
              onPress={() => this.setState({ visibleDelete: false })}
              underlayColor={color.whitelv2}
              style={{
                padding: 20,
                borderRadius: 10,
                backgroundColor: color.white,
                marginVertical: 20,
                alignItems: 'center'
              }}
            >
              <Text style={{ fontSize: 20 }}>Cancel</Text>
            </TouchableHighlight>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 5
  },
  wrapImage: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: color.whitelv2,
    borderWidth: 1
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: 45 / 2
  }
});

const mapStateToProps = state => ({
  profile: state.profile,
  comment: state.comment
});

const mapDispatchToProps = {
  deleteComment
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WrapComment);
