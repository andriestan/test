import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import config from '../../public/config/config.json';

const color = config.color;

class Shimmer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [1, 2, 3, 4, 5, 6, 7, 8, 9]
    };
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.data.map((item, index) => (
          <View style={styles.content} key={item}>
            <View style={styles.wrapImage} />
            <View style={{ flex: 1, marginLeft: 10 }}>
              <View
                style={{
                  backgroundColor: color.backgroundContent,
                  borderRadius: 10,
                  height: index % 2 === 0 ? 70 : 40
                }}
              />
            </View>
          </View>
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.backgroundContainer,
    flex: 1
  },
  content: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 5
  },
  wrapImage: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.backgroundContent
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: 45 / 2
  }
});

export default Shimmer;
