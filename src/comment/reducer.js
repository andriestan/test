const initialState = {
  comments: [],
  isLoading: true,
  isLoadingComment: false,
  isLoadingUpdate: false,
  isPagination: true,
  commentsReply: [],
  isLoadingReply: true,
  isPaginationReply: true,
  isLoadingCommentReply: false,
  isLoadingUpdateReply: false,
  post: '',
  isLoadingPost: true,
  isLoadingDelete: false,
  isLoadingDeleteReply: false,
  isLoadingLikes: false,
  likes: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_COMMENT_PENDING':
      return { ...state, isLoading: true };
    case 'GET_COMMENT_SUCCESS':
      return {
        ...state,
        comments: action.payload,
        isLoading: false,
        isPagination: !(action.payload.length < 20)
      };
    case 'CREATE_COMMENT_PENDING':
      return { ...state, isLoadingComment: true };
    // eslint-disable-next-line no-case-declarations
    case 'CREATE_COMMENT_SUCCESS':
      const comments = state.comments;
      comments.unshift(action.payload);
      return { ...state, comments, isLoadingComment: false };
    case 'UPDATE_COMMENT_PENDING':
      return { ...state, isLoadingUpdate: true };
    case 'UPDATE_COMMENT_SUCCESS':
      return {
        ...state,
        comments: state.comments.concat(action.payload),
        isLoadingUpdate: false,
        isPagination: !(action.payload.length < 20)
      };
    // eslint-disable-next-line no-case-declarations
    case 'HANDLE_LIKE_COMMENT_PENDING':
      const index = state.comments.findIndex(
        field => field.id === action.payload
      );
      // eslint-disable-next-line no-redeclare
      const commentsVariable = state.comments;
      commentsVariable[index].is_comment_liked = !commentsVariable[index]
        .is_comment_liked;
      if (commentsVariable[index].is_comment_liked) {
        commentsVariable[index].comment_likes =
          Number(commentsVariable[index].comment_likes) + 1;
      } else {
        commentsVariable[index].comment_likes =
          Number(commentsVariable[index].comment_likes) - 1;
      }
      return { ...state, comments: commentsVariable };
    case 'GET_COMMENT_REPLY_PENDING':
      return { ...state, isLoadingReply: true };
    case 'GET_COMMENT_REPLY_SUCCESS':
      return {
        ...state,
        commentsReply: action.payload,
        isLoadingReply: false,
        isPaginationReply: !(action.payload.length < 30)
      };
    case 'CREATE_COMMENT_REPLY_PENDING':
      return { ...state, isLoadingCommentReply: true };
    // eslint-disable-next-line no-case-declarations
    case 'CREATE_COMMENT_REPLY_SUCCESS':
      const indexComment = state.comments.findIndex(
        field => field.id === action.payload.comment_id
      );
      const commentsVariableForReply = state.comments;
      commentsVariableForReply[indexComment].replies =
        Number(commentsVariableForReply[indexComment].replies) + 1;

      const commentsReply = state.commentsReply;
      commentsReply.unshift(action.payload);
      return {
        ...state,
        commentsReply,
        isLoadingCommentReply: false,
        comments: commentsVariableForReply
      };
    case 'UPDATE_COMMENT_REPLY_PENDING':
      return { ...state, isLoadingUpdateReply: true };
    case 'UPDATE_COMMENT_REPLY_SUCCESS':
      return {
        ...state,
        commentsReply: state.commentsReply.concat(action.payload),
        isLoadingUpdateReply: false,
        isPaginationReply: !(action.payload.length < 30)
      };
    case 'GET_POST_PENDING':
      return { ...state, isLoadingPost: true };
    case 'STORE_POST':
      return { ...state, post: action.payload, isLoadingPost: false };
    case 'DELETE_COMMENT_PENDING':
      return { ...state, isLoadingDelete: true };
    case 'DELETE_COMMENT_SUCCESS':
      return {
        ...state,
        isLoadingDelete: false,
        comments: state.comments.filter(
          data => data.id !== action.payload.commentId
        )
      };
    case 'DELETE_COMMENT_REPLY_PENDING':
      return { ...state, isLoadingDeleteReply: true };
    // eslint-disable-next-line no-case-declarations
    case 'DELETE_COMMENT_REPLY_SUCCESS':
      const indexCommentDeleteReply = state.comments.findIndex(
        field => field.id === action.payload.commentId
      );
      const commentsVariableForReplyDelete = state.comments;
      commentsVariableForReplyDelete[indexCommentDeleteReply].replies =
        Number(
          commentsVariableForReplyDelete[indexCommentDeleteReply].replies
        ) - 1;
      return {
        ...state,
        isLoadingDeleteReply: false,
        commentsReply: state.commentsReply.filter(
          data => data.id !== action.payload.replyId
        ),
        comments: commentsVariableForReplyDelete
      };
    case 'GET_POST_LIKE_PENDING':
      return { ...state, isLoadingLikes: true };
    case 'GET_POST_LIKE_SUCCESS':
      return { ...state, isLoadingLikes: false, likes: action.payload };
    default:
      return state;
  }
};
