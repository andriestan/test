import { Fetch, getUser } from '../public/services';
import config from '../public/config/config.json';

export const storePost = data => async dispatch => {
  dispatch({ type: 'STORE_POST', payload: data });
};

export const getPost = postId => async dispatch => {
  dispatch({ type: 'GET_POST_LIKE_PENDING' });
  const data = {
    fetch: 'post_data',
    post_id: postId
  };
  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/get-post-data?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(
      `/api/get-post-data?access_token=${config.guest}`,
      data
    );
  }

  if (response.api_status === 200) {
    dispatch(storePost(response.post_data));
  }
};

export const getPostLike = postId => async dispatch => {
  dispatch({ type: 'GET_POST_LIKE_PENDING' });
  const data = {
    fetch: 'post_liked_users',
    post_id: postId
  };
  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/get-post-data?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(
      `/api/get-post-data?access_token=${config.guest}`,
      data
    );
  }

  if (response.api_status === 200) {
    dispatch({
      type: 'GET_POST_LIKE_SUCCESS',
      payload: response.post_liked_users
    });
  }
};

export const getComment = postId => async dispatch => {
  dispatch({ type: 'GET_COMMENT_PENDING' });
  const data = {
    type: 'fetch_comments',
    post_id: postId,
    limit: 20
  };
  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/comments?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(`/api/comments?access_token=${config.guest}`, data);
  }

  if (response.api_status === 200) {
    dispatch({ type: 'GET_COMMENT_SUCCESS', payload: response.data });
  }
};

export const updateComment = (postId, offset) => async dispatch => {
  dispatch({ type: 'UPDATE_COMMENT_PENDING' });
  const data = {
    type: 'fetch_comments',
    post_id: postId,
    limit: 20,
    offset
  };
  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/comments?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(`/api/comments?access_token=${config.guest}`, data);
  }

  if (response.api_status === 200) {
    dispatch({ type: 'UPDATE_COMMENT_SUCCESS', payload: response.data });
  }
};

export const createComment = (postId, text) => async dispatch => {
  dispatch({ type: 'CREATE_COMMENT_PENDING' });
  const data = {
    type: 'create',
    post_id: postId,
    text
  };
  const user = await getUser();
  const response = await Fetch(
    `/api/comments?access_token=${user.access_token}`,
    data
  );

  if (response.api_status === 200) {
    await dispatch({ type: 'CREATE_COMMENT_SUCCESS', payload: response.data });
  }
};

export const handleLikeComment = (commentId, action) => async dispatch => {
  dispatch({ type: 'HANDLE_LIKE_COMMENT_PENDING', payload: commentId });
  const data = {
    type: action ? 'comment_dislike' : 'comment_like',
    comment_id: commentId
  };
  const user = await getUser();
  await Fetch(`/api/comments?access_token=${user.access_token}`, data);
};

export const getCommentReply = commentId => async dispatch => {
  dispatch({ type: 'GET_COMMENT_REPLY_PENDING' });
  const data = {
    type: 'fetch_comments_reply',
    comment_id: commentId,
    limit: 30
  };
  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/comments?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(`/api/comments?access_token=${config.guest}`, data);
  }

  if (response.api_status === 200) {
    dispatch({ type: 'GET_COMMENT_REPLY_SUCCESS', payload: response.data });
  }
};

export const createCommentReply = (commentId, text) => async dispatch => {
  dispatch({ type: 'CREATE_COMMENT_REPLY_PENDING' });
  const data = {
    type: 'create_reply',
    comment_id: commentId,
    text
  };
  const user = await getUser();
  const response = await Fetch(
    `/api/comments?access_token=${user.access_token}`,
    data
  );

  if (response.api_status === 200) {
    await dispatch({
      type: 'CREATE_COMMENT_REPLY_SUCCESS',
      payload: response.data
    });
  }
};

export const updateCommentReply = (commentId, offset) => async dispatch => {
  dispatch({ type: 'UPDATE_COMMENT_REPLY_PENDING' });
  const data = {
    type: 'fetch_comments_reply',
    comment_id: commentId,
    limit: 30,
    offset
  };
  const user = await getUser();
  let response;
  if (user) {
    response = await Fetch(
      `/api/comments?access_token=${user.access_token}`,
      data
    );
  } else {
    response = await Fetch(`/api/comments?access_token=${config.guest}`, data);
  }

  if (response.api_status === 200) {
    dispatch({ type: 'UPDATE_COMMENT_REPLY_SUCCESS', payload: response.data });
  }
};

export const deleteComment = (commentId, postId) => async dispatch => {
  dispatch({ type: 'DELETE_COMMENT_PENDING' });

  const user = await getUser();
  const data = {
    type: 'delete',
    comment_id: commentId
  };

  const response = await Fetch(
    `/api/comments?access_token=${user.access_token}`,
    data
  );

  if (response.api_status === 200) {
    dispatch({
      type: 'DELETE_COMMENT_SUCCESS',
      payload: { commentId, postId }
    });
  }
};

export const deleteCommentReply = (replyId, commentId) => async dispatch => {
  dispatch({ type: 'DELETE_COMMENT_REPLY_PENDING' });

  const user = await getUser();
  const data = {
    type: 'delete_reply',
    reply_id: replyId
  };

  const response = await Fetch(
    `/api/comments?access_token=${user.access_token}`,
    data
  );

  if (response.api_status === 200) {
    dispatch({
      type: 'DELETE_COMMENT_REPLY_SUCCESS',
      payload: { replyId, commentId }
    });
  }
};
